package by.tanana.newsManagement.dao.test;

import static org.junit.Assert.assertEquals;

import java.sql.Timestamp;

import javax.sql.DataSource;

import org.junit.Test;
import org.unitils.UnitilsJUnit4;
import org.unitils.database.annotations.Transactional;
import org.unitils.database.util.TransactionMode;
import org.unitils.dbunit.annotation.DataSet;
import org.unitils.spring.annotation.SpringApplicationContext;
import org.unitils.spring.annotation.SpringBean;

import exception.DAOException;
import by.tanana.newsManagement.dao.AuthorDAO;
import by.tanana.newsManagement.entity.AuthorTO;

@SpringApplicationContext("test-applicationContext.xml")
@DataSet("AuthorDAOTest.xml")
@Transactional(TransactionMode.ROLLBACK)
public class AuthorDAOTest extends UnitilsJUnit4 {
	@SpringBean("dataSource")
	private DataSource dataSource;
	@SpringBean("authorDAO")
	private AuthorDAO authorDAO;

	@Test
	public void readTest() throws DAOException {
		AuthorTO actualAuthor = authorDAO.read(new Long(1));
		AuthorTO expectedAuthor = setExpectedAuthor(1L, "Mark", null);
		assertAuthorsEquals(expectedAuthor, actualAuthor);
	}

	@Test
	public void createTest() throws DAOException {
		AuthorTO expectedAuthor = setExpectedAuthor(null, "Anna", null);
		Long id = authorDAO.create(expectedAuthor);
		expectedAuthor.setAuthorId(id);
		AuthorTO actualAuthor = authorDAO.read(id);
		assertAuthorsEquals(expectedAuthor, actualAuthor);	
	}

	@Test 
	public void updateTest() throws DAOException {
		AuthorTO expectedAuthor = setExpectedAuthor(1L, "Mathew", null);
		authorDAO.update(expectedAuthor);
		AuthorTO actualAuthor = authorDAO.read(new Long(1));
		assertAuthorsEquals(expectedAuthor, actualAuthor);	
	}
	
	@Test
	public void deleteTest() throws DAOException {
		AuthorTO actualAuthor = new AuthorTO();
		authorDAO.delete(new Long(1), Timestamp.valueOf("2007-09-23 10:10:10.0"));
		actualAuthor = authorDAO.read(new Long(1));
		assertEquals(Timestamp.valueOf("2007-09-23 10:10:10.0"), actualAuthor.getExpired());
	}
	
	@Test
	public void readByNewsIdTest() throws DAOException {
		AuthorTO actualAuthor = authorDAO.readByNewsId(new Long(1));
		AuthorTO expectedAuthor = setExpectedAuthor(1L, "Mark", null);
		assertAuthorsEquals(expectedAuthor, actualAuthor);
	}
	
	@Test
	public void bindingNewsWithAuthorTest() throws DAOException {
		Long expectedAuthorId = new Long(1);
		Long newsId = new Long(1);
		authorDAO.bindNewsWithAuthor(expectedAuthorId, newsId);
		AuthorTO actualAuthor = authorDAO.readByNewsId(newsId);
		assertEquals(expectedAuthorId, actualAuthor.getAuthorId());
	}
	
	@Test
	public void deleteFromNewsAuthorByNewsIdTest() throws DAOException {
		AuthorTO expectedAuthor = null;
		Long newsId = new Long(1);
		authorDAO.deleteFromNewsAuthorByNewsId(newsId);
		AuthorTO actualAuthor = authorDAO.readByNewsId(newsId);
		assertEquals(expectedAuthor, actualAuthor);
	}
	
	private AuthorTO setExpectedAuthor(Long authorId, String name, Timestamp expired) {
		AuthorTO expectedAuthor = new AuthorTO();
		expectedAuthor.setAuthorId(authorId);
		expectedAuthor.setName(name);
		expectedAuthor.setExpired(expired);
		return expectedAuthor;
	}
	
	private void assertAuthorsEquals(AuthorTO expectedAuthor, AuthorTO actualAuthor) {
		assertEquals(expectedAuthor.getAuthorId(), actualAuthor.getAuthorId());
		assertEquals(expectedAuthor.getName(), actualAuthor.getName());
		assertEquals(expectedAuthor.getExpired(), actualAuthor.getExpired());
	}

}
