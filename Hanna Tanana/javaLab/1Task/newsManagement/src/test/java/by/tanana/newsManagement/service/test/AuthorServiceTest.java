package by.tanana.newsManagement.service.test;

import java.sql.Timestamp;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import exception.DAOException;
import exception.ServiceException;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import by.tanana.newsManagement.dao.AuthorDAO;
import by.tanana.newsManagement.entity.AuthorTO;
import by.tanana.newsManagement.service.AuthorService;


@ContextConfiguration(locations = {"classpath:/test-applicationContext.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class AuthorServiceTest {
	@InjectMocks
	@Autowired
	private AuthorService authorService;

	@Mock
	private AuthorDAO authorDAO;
	
	private AuthorTO author;

	@Before
	public void setUp() throws Exception
	{
		MockitoAnnotations.initMocks( this );
		author = new AuthorTO();
		author.setAuthorId(new Long(1));
		author.setName("Bill");
	}

	@Test
	public void createTest() throws ServiceException, DAOException {		
		when(authorDAO.create(author)).thenReturn(author.getAuthorId());
		Long actualId = authorService.create(author);
		verify(authorDAO, times(1)).create(author);	
		assertEquals(author.getAuthorId(), actualId);
	}

	@Test
	public void readTest() throws ServiceException, DAOException {
		Long authorId = new Long(1);	
		when(authorDAO.read(authorId)).thenReturn(author);
		AuthorTO actualAuthor = authorService.read(authorId);
		assertEquals(author, actualAuthor);
		verify(authorDAO, times(1)).read(authorId);
	}
	
	@Test
	public void readByNewsId() throws ServiceException, DAOException {
		Long newsId = new Long(1);
		when(authorDAO.readByNewsId(newsId)).thenReturn(author);
		AuthorTO actualAuthor = authorService.readByNewsId(newsId);
		assertEquals(author, actualAuthor);
		verify(authorDAO, times(1)).readByNewsId(newsId);
	}

	@Test
	public void updateTest() throws ServiceException, DAOException {
		authorService.update(author);
		verify(authorDAO, times(1)).update(author);
	}

	@Test
	public void deleteTest() throws ServiceException, DAOException {
		Long authorId = new Long(1);
		Timestamp expiredTime = Timestamp.valueOf("2015-06-16 22:30:00");
		authorService.delete(authorId, expiredTime);
		verify(authorDAO, times(1)).delete(authorId, expiredTime);
	}

	@Test 
	public void bindingNewsWithAuthorTest() throws ServiceException, DAOException {
		Long authorId = new Long(1);
		Long newsId = new Long(1);
		authorService.bindNewsWithAuthor(authorId, newsId);
		verify(authorDAO, times(1)).bindNewsWithAuthor(authorId, newsId);
	}

	@Test
	public void deleteAuthorsFromNewsAuthorByNewsIdTest() throws ServiceException, DAOException {
		Long newsId = new Long(1);
		authorService.deleteAuthorsFromNewsAuthorByNewsId(newsId);
		verify(authorDAO, times(1)).deleteFromNewsAuthorByNewsId(newsId);
	}

}
