package by.tanana.newsManagement.dao.test;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.junit.Test;
import org.unitils.UnitilsJUnit4;
import org.unitils.database.annotations.Transactional;
import org.unitils.database.util.TransactionMode;
import org.unitils.dbunit.annotation.DataSet;
import org.unitils.spring.annotation.SpringApplicationContext;
import org.unitils.spring.annotation.SpringBean;

import exception.DAOException;
import by.tanana.newsManagement.dao.TagDAO;
import by.tanana.newsManagement.entity.TagTO;

@SpringApplicationContext("test-applicationContext.xml")
@DataSet("TagDAOTest.xml")
@Transactional(TransactionMode.ROLLBACK)
public class TagDAOTest extends UnitilsJUnit4 {
	@SpringBean("dataSource")
	private DataSource dataSource;
	@SpringBean("tagDAO")
	private TagDAO tagDAO;

	@Test
	public void readTest() throws DAOException {
		TagTO actualTag = tagDAO.read(new Long(1));
		TagTO expectedTag = setExpectedTag(1L, "social");
		assertTagsEquals(expectedTag, actualTag);
	}

	@Test
	public void createTest() throws DAOException {
		TagTO expectedTag = setExpectedTag(3L, "crazy");
		Long id = tagDAO.create(expectedTag);
		expectedTag.setTagId(id);
		TagTO actualTag = tagDAO.read(id);
		assertTagsEquals(expectedTag, actualTag);
	}

	@Test 
	public void updateTest() throws DAOException {
		TagTO expectedTag = setExpectedTag(1L, "society");
		tagDAO.update(expectedTag);
		TagTO actualTag = tagDAO.read(new Long(1));
		assertTagsEquals(expectedTag, actualTag);
	}

	@Test
	public void deleteTest() throws DAOException {
		TagTO expectedTag = null;
		tagDAO.delete(new Long(2));
		TagTO actualTag = tagDAO.read(new Long(2));
		assertEquals(expectedTag, actualTag);
	}
	
	@Test
	public void readListOfTagsByNewsIdTest() throws DAOException {
		List<TagTO> tagList = tagDAO.readListOfTagsByNewsId(new Long(1));
		TagTO expectedTag = setExpectedTag(1L, "social");
		TagTO actualTag = tagList.get(0);
		assertEquals(1, tagList.size());
		assertTagsEquals(expectedTag, actualTag);
	}

	@Test
	public void bindingMultipleTagsWithNewsTest() throws DAOException {
		TagTO expectedTag1 = setExpectedTag(1L, "social");
		TagTO expectedTag2 = setExpectedTag(2L, "media");
		List<Long> expectedListOfTags = new ArrayList<Long>();
		expectedListOfTags.add(expectedTag1.getTagId());
		expectedListOfTags.add(expectedTag2.getTagId());
		Long newsId = new Long(2);
		tagDAO.bindMultipleTagsWithNews(expectedListOfTags, newsId);
		List<TagTO> actualListOfTags = tagDAO.readListOfTagsByNewsId(newsId);
		assertEquals(expectedListOfTags.size(), actualListOfTags.size());
		assertTagsEquals(expectedTag1, actualListOfTags.get(0));
		assertTagsEquals(expectedTag2, actualListOfTags.get(1));
	}
	
	@Test
	public void deleteFromNewsTagByNewsIdTest() throws DAOException {
		List<TagTO> expectedListOfTags = null;
		Long newsId = new Long(1);
		tagDAO.deleteFromNewsTagByNewsId(newsId);
		List<TagTO> actualListOfTags = tagDAO.readListOfTagsByNewsId(newsId);
		assertEquals(expectedListOfTags, actualListOfTags);
	}
	
	private TagTO setExpectedTag(Long tagId, String name) {
		TagTO expectedTag = new TagTO();
		expectedTag.setTagId(tagId);
		expectedTag.setName(name);
		return expectedTag;
	}
	
	private void assertTagsEquals(TagTO expectedTag, TagTO actualTag) {
		assertEquals(expectedTag.getTagId(), actualTag.getTagId());
		assertEquals(expectedTag.getName(), actualTag.getName());
	}


}
