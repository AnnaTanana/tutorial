package by.tanana.newsManagement.dao.test;

import static org.junit.Assert.assertEquals;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.junit.Test;
import org.unitils.UnitilsJUnit4;
import org.unitils.database.annotations.Transactional;
import org.unitils.database.util.TransactionMode;
import org.unitils.dbunit.annotation.DataSet;
import org.unitils.spring.annotation.SpringApplicationContext;
import org.unitils.spring.annotation.SpringBean;

import exception.DAOException;
import by.tanana.newsManagement.dao.NewsDAO;
import by.tanana.newsManagement.entity.NewsTO;
import by.tanana.newsManagement.entity.SearchCriteria;

@SpringApplicationContext("test-applicationContext.xml")
@DataSet("NewsDAOTest.xml")
@Transactional(TransactionMode.ROLLBACK)
public class NewsDAOTest extends UnitilsJUnit4 {
	@SpringBean("dataSource")
	private DataSource dataSource;
	@SpringBean("newsDAO")
	private NewsDAO newsDAO;

	@Test
	public void readTest() throws DAOException {
		NewsTO actualNews = newsDAO.read(new Long(1));
		NewsTO expectedNews = setExpectedNews(1L, "��� � ������", "��� � ������: ��� ���������� ����� IT-���������� �� 20 ���",
				"��� ��� ����� 15 ��� �� ����� � ����� �����������, �� ��� ����� � �� �������� ������ ������. ...",
				"2015-06-16 22:30:00", "2015-06-17");	
		assertNewsEquals(expectedNews, actualNews);
	}

	@Test
	public void createTest() throws DAOException {
		NewsTO expectedNews = setExpectedNews(null, "European Summit", "European Summit Strikes Greece Bailout Deal",
				"Greece reached an agreement with its creditors over the reforms needed to start talks for a third bailout in five years and remain in the euro.",
				"2015-07-13 10:00:00", "2015-07-13");	
		Long id = newsDAO.create(expectedNews);
		expectedNews.setNewsId(id);
		NewsTO actualNews = newsDAO.read(id);
		assertNewsEquals(expectedNews, actualNews);
	}

	@Test 
	public void updateTest() throws DAOException {
		NewsTO expectedNews = setExpectedNews(1L, "European Meeting", "European Meeting Strikes Greece Bailout Deal",
				"Greece reached an agreement with its creditors over the reforms needed to start talks for a third bailout in five years and remain in the euro.",
				"2015-06-16 22:30:00", "2015-08-13");
		newsDAO.update(expectedNews);
		NewsTO actualNews = newsDAO.read(new Long(1));
		assertNewsEquals(expectedNews, actualNews);
	}
	
	@Test
	public void deleteTest() throws DAOException {
		NewsTO expectedNews = null;
		newsDAO.delete(new Long(1));
		NewsTO actualNews = newsDAO.read(new Long(1));
		assertEquals(expectedNews, actualNews);
	}
	
	@Test
	public void countAllNewsTest() throws DAOException {
		int expectedNumber = 2;
		int actualNumber = newsDAO.countAllNews();
		assertEquals(expectedNumber, actualNumber);
	}
	
	@Test 
	public void readNewsByTagIdTest() throws DAOException {
		NewsTO actualNews = newsDAO.readNewsByTagId(new Long(1));
		NewsTO expectedNews = setExpectedNews(2L, "European Summit", "European Summit Strikes Greece Bailout Deal",
				"Greece reached an agreement with its creditors over the reforms needed to start talks for a third bailout in five years and remain in the euro.",
				"2015-06-16 22:30:00", "2015-06-16");	
		assertNewsEquals(expectedNews, actualNews);
	}	
	
	@Test 
	public void bindingNewsWithTagTest() throws DAOException {
		Long expectedNewsId = new Long(2);
		Long tagId = new Long(2);
		newsDAO.bindNewsWithTag(tagId, expectedNewsId);
		NewsTO actualNews = newsDAO.readNewsByTagId(tagId);
		assertEquals(expectedNewsId, actualNews.getNewsId());
	}

	@Test
	public void deleteFromNewsTagByTagIdTest() throws DAOException {
		NewsTO expectedNews = null;
		Long tagId = new Long(1);
		newsDAO.deleteFromNewsTagByTagId(tagId);
		NewsTO actualNews = newsDAO.readNewsByTagId(tagId);
		assertEquals(expectedNews, actualNews);
	}
	
	@Test
	public void searchNewsByAuthorIdTest() throws DAOException {
		SearchCriteria searchCriteria = new SearchCriteria();
		searchCriteria.setAuthorId(new Long(1));
		List<NewsTO> actualNewsList = newsDAO.getNews(searchCriteria);
		NewsTO expectedNews = setExpectedNews(2L, "European Summit", "European Summit Strikes Greece Bailout Deal",
				"Greece reached an agreement with its creditors over the reforms needed to start talks for a third bailout in five years and remain in the euro.",
				"2015-06-16 22:30:00", "2015-06-16");
		assertNewsEquals(expectedNews, actualNewsList.get(0));
	}
	
	@Test 
	public void searchAllNewsTest() throws DAOException {
		SearchCriteria searchCriteria = new SearchCriteria();
		List<NewsTO> actualNewsList = newsDAO.getNews(searchCriteria);
		NewsTO expectedNews1 = setExpectedNews(1L, "��� � ������", "��� � ������: ��� ���������� ����� IT-���������� �� 20 ���",
				"��� ��� ����� 15 ��� �� ����� � ����� �����������, �� ��� ����� � �� �������� ������ ������. ...",
				"2015-06-16 22:30:00", "2015-06-17");
		assertNewsEquals(expectedNews1, actualNewsList.get(0));
		NewsTO expectedNews2 = setExpectedNews(2L, "European Summit", "European Summit Strikes Greece Bailout Deal",
				"Greece reached an agreement with its creditors over the reforms needed to start talks for a third bailout in five years and remain in the euro.",
				"2015-06-16 22:30:00", "2015-06-16");
		assertNewsEquals(expectedNews2, actualNewsList.get(1));
	}
	
	@Test 
	public void searchNewsByTagIdTest() throws DAOException {
		SearchCriteria searchCriteria = new SearchCriteria();
		List<Long> expectedTagList = new ArrayList<Long>();
		expectedTagList.add(new Long(1));
		expectedTagList.add(new Long(2));
		searchCriteria.setTagIdList(expectedTagList);
		List<NewsTO> actualNewsList = newsDAO.getNews(searchCriteria);
		NewsTO expectedNews = setExpectedNews(2L, "European Summit", "European Summit Strikes Greece Bailout Deal",
				"Greece reached an agreement with its creditors over the reforms needed to start talks for a third bailout in five years and remain in the euro.",
				"2015-06-16 22:30:00", "2015-06-16");
		assertNewsEquals(expectedNews, actualNewsList.get(0));		
	}
	
	@Test
	public void searchNewsByAuthorAndTagIdTest() throws DAOException {
		SearchCriteria searchCriteria = new SearchCriteria();
		List<Long> expectedTagList = new ArrayList<Long>();
		expectedTagList.add(new Long(1));
		expectedTagList.add(new Long(2));
		searchCriteria.setTagIdList(expectedTagList);
		searchCriteria.setAuthorId(new Long(1));
		List<NewsTO> actualNewsList = newsDAO.getNews(searchCriteria);
		NewsTO expectedNews = setExpectedNews(2L, "European Summit", "European Summit Strikes Greece Bailout Deal",
				"Greece reached an agreement with its creditors over the reforms needed to start talks for a third bailout in five years and remain in the euro.",
				"2015-06-16 22:30:00", "2015-06-16");
		assertNewsEquals(expectedNews, actualNewsList.get(0));	
	}
	
	private NewsTO setExpectedNews(Long newsId, String title, String shortText, String fullText, String creationDate, String modificationDate) {
		NewsTO expectedNews = new NewsTO();
		expectedNews.setNewsId(newsId);
		expectedNews.setTitle(title);
		expectedNews.setShortText(shortText);
		expectedNews.setFullText(fullText);
		expectedNews.setCreationDate(Timestamp.valueOf(creationDate));
		expectedNews.setModificationDate(Date.valueOf(modificationDate));
		return expectedNews;
	}
	
	private void assertNewsEquals(NewsTO expectedNews, NewsTO actualNews) {
		assertEquals(expectedNews.getNewsId(), actualNews.getNewsId());
		assertEquals(expectedNews.getTitle(), actualNews.getTitle());
		assertEquals(expectedNews.getShortText(), actualNews.getShortText());
		assertEquals(expectedNews.getFullText(), actualNews.getFullText());
		assertEquals(expectedNews.getCreationDate(), actualNews.getCreationDate());
		assertEquals(expectedNews.getModificationDate(), actualNews.getModificationDate());
	}


}
