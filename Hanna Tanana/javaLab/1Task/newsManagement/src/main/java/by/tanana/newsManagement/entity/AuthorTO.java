package by.tanana.newsManagement.entity;

import java.io.Serializable;
import java.util.Date;

public class AuthorTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3490045102558484888L;
	private Long authorId;
	private String name;
	private Date expired;
	
	public AuthorTO() {}
	
	public Long getAuthorId() {
		return authorId;
	}
	
	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Date getExpired() {
		return expired;
	}
	
	public void setExpired(Date expired) {
		this.expired = expired;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((expired == null) ? 0 : expired.hashCode());
		result = prime * result + ((authorId == null) ? 0 : authorId.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		AuthorTO other = (AuthorTO)obj;
		if (this.authorId == null) {
			if (other.authorId != null) {
				return false;
			}
		} else if (!this.authorId.equals(other.authorId)) {
			return false;
		}
		if (this.name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!this.name.equals(other.name)) {
			return false;
		}
		if (this.expired == null) {
			if (other.expired != null) {
				return false;
			}
		} else if (!this.expired.equals(other.expired)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("Author: authorId=").append(authorId.toString()).
		append(", name=").append(name);
		if (expired != null) {
			stringBuilder.append(", expired=").append(expired.toString());
		}
		return stringBuilder.toString();
	}
	
	
}
