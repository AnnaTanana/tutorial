package by.tanana.newsManagement.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import exception.DAOException;
import exception.ServiceException;
import by.tanana.newsManagement.dao.NewsDAO;
import by.tanana.newsManagement.entity.NewsTO;
import by.tanana.newsManagement.entity.SearchCriteria;
import by.tanana.newsManagement.service.NewsService;

public class NewsServiceImpl implements NewsService {
	
	private static final Logger logger = LoggerFactory.getLogger(NewsServiceImpl.class);
	
	private NewsDAO newsDAO;

	public void setNewsDAO(NewsDAO newsDAO) {
		this.newsDAO = newsDAO;
	}

	/* 
	 * @see by.tanana.newsManagement.service.NewsService#create(by.tanana.newsManagement.entity.NewsTO)
	 */
	@Override
	public Long create(NewsTO news) throws ServiceException {
		try {
			return newsDAO.create(news);
		} catch (DAOException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e.getMessage(), e);
		}
	}

	/* 
	 * @see by.tanana.newsManagement.service.NewsService#read(java.lang.Long)
	 */
	@Override
	public NewsTO read(Long newsId) throws ServiceException {
		try {
			return newsDAO.read(newsId);
		} catch (DAOException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e.getMessage(), e);
		}
	}

	/* 
	 * @see by.tanana.newsManagement.service.NewsService#update(by.tanana.newsManagement.entity.NewsTO)
	 */
	@Override
	public void update(NewsTO news) throws ServiceException {
		try {
			newsDAO.update(news);
		} catch (DAOException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e.getMessage(), e);
		}
		
	}

	/* 
	 * @see by.tanana.newsManagement.service.NewsService#delete(java.lang.Long)
	 */
	@Override
	public void delete(Long newsId) throws ServiceException {
		try {
			newsDAO.delete(newsId);
		} catch (DAOException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e.getMessage(), e);
		}
		
	}

	/* 
	 * @see by.tanana.newsManagement.service.NewsService#countAllNews()
	 */
	@Override
	public int countAllNews() throws ServiceException {
		try {
			return newsDAO.countAllNews();
		} catch (DAOException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e.getMessage(), e);
		}
	}

	/* 
	 * @see by.tanana.newsManagement.service.NewsService#bindNewsWithTag(java.lang.Long, java.lang.Long)
	 */
	@Override
	public void bindNewsWithTag(Long tagId, Long newsId) throws ServiceException {
		try {
			newsDAO.bindNewsWithTag(tagId, newsId);
		} catch (DAOException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e.getMessage(), e);
		}
		
	}

	/* 
	 * @see by.tanana.newsManagement.service.NewsService#deleteFromNewsTagByTagId(java.lang.Long)
	 */
	@Override
	public void deleteFromNewsTagByTagId(Long tagId) throws ServiceException {
		try {
			newsDAO.deleteFromNewsTagByTagId(tagId);;
		} catch (DAOException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e.getMessage(), e);
		}		
	}

	/* 
	 * @see by.tanana.newsManagement.service.NewsService#getNews(by.tanana.newsManagement.entity.SearchCriteria)
	 */
	@Override
	public List<NewsTO> getNews(SearchCriteria searchCriteria) throws ServiceException {
		try {
			return newsDAO.getNews(searchCriteria);
		} catch (DAOException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e.getMessage(), e);
		}		
	}

	/* 
	 * @see by.tanana.newsManagement.service.NewsService#readNewsByTagId(java.lang.Long)
	 */
	@Override
	public NewsTO readNewsByTagId(Long tagId) throws ServiceException {
		try {
			return newsDAO.readNewsByTagId(tagId);
		} catch (DAOException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e.getMessage(), e);
		}		
	}

}
