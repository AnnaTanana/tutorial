package by.tanana.newsManagement.dao.impl;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;








import org.springframework.jdbc.datasource.DataSourceUtils;

import exception.DAOException;
import by.tanana.newsManagement.dao.NewsDAO;
import by.tanana.newsManagement.entity.NewsTO;
import by.tanana.newsManagement.entity.SearchCriteria;
import by.tanana.newsManagement.util.DBUtil;

public class NewsDAOImpl implements NewsDAO {
	
	private static final String SQL_CREATE_NEWS 
			= "INSERT INTO news "
			+ "(news_id, title, short_text, full_text, creation_date, modification_date) "
			+ "VALUES (news_seq.nextval, ?, ?, ?, ?, ?)";
	
	private static final String SQL_READ_NEWS 
			= "SELECT news_id, title, short_text, full_text, creation_date, modification_date "
			+ "FROM news "
			+ "WHERE news_id = ?"; 
	
	private static final String SQL_UPDATE_NEWS 
			= "UPDATE news "
			+ "SET title = ?, short_text = ?, full_text = ?, modification_date = ? "
			+ "WHERE news_id = ?";
	
	private static final String SQL_DELETE_NEWS 
			= "DELETE FROM news WHERE news_id = ?";
	
	private static final String SQL_COUNT_NEWS 
			= "SELECT COUNT(news_id) as CNT FROM news";
	
	private static final String SQL_BIND_NEWS_TAG 
			= "INSERT INTO news_tag "
			+ "(tag_id, news_id) "
			+ "VALUES (?, ?)";
	
	private static final String SQL_DELETE_FROM_NEWS_TAG_BY_TAG_ID 
			= "DELETE FROM news_tag "
			+ "WHERE tag_id = ?";
	
	private static final String SQL_READ_NEWS_BY_TAG_ID 
			= "SELECT n.news_id, n.title, n.short_text, n.full_text, n.creation_date, n.modification_date "
			+ "FROM news n "
			+ "JOIN news_tag ON n.news_id = news_tag.news_id "
			+ "WHERE news_tag.tag_id = ?";
	
	private static final String SQL_START_SELECT_NEWS_BY_SEARCH_CRITERIA 
			= "SELECT n.news_id, n.title, n.short_text, n.full_text, n.creation_date, n.modification_date, "
			+ "COUNT(c.comment_id) as cnt "
			+ "FROM news n "
			+ "LEFT JOIN comments c ON c.news_id = n.news_id";
	
	private static final String SQL_JOIN_AUTHOR
			= " JOIN news_author "
			+ "ON news_author.news_id = n.news_id";
	
	private static final String SQL_JOIN_TAGS
			=" JOIN news_tag "
			+ "ON news_tag.news_id = n.news_id";
	
	private static final String SQL_ADD_WHERE_CLAUSE
			= " WHERE 1=1";
	
	private static final String SQL_ADD_WHERE_CLAUSE_FOR_TAGS
			= " AND news_tag.tag_id IN (?";
	
	private static final String SQL_ADD_WHERE_CLAUSE_FOR_AUTHOR
			= " AND news_author.author_id = ?";
	
	private static final String SQL_END_SEARCH_CRITERIA
			= " GROUP BY n.news_id, n.title, n.short_text, n.full_text, n.creation_date, n.modification_date "
			+ "ORDER BY cnt DESC, n.modification_date DESC";
	
	private static final String PREPARED_STATEMENT_ATTRIBUTE = ", ?";
	
	private static final String CLOSING_BRACKET = ")";
	
	private DataSource dataSource;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }
	
	/* 
	 * @see by.tanana.newsManagement.dao.NewsDAO#create(by.tanana.newsManagement.entity.NewsTO)
	 */
	@Override
	public Long create(NewsTO news) throws DAOException {
		Connection connection = null;
        PreparedStatement statement = null;
        ResultSet generatedKeys = null;
        try {
        	connection = DataSourceUtils.doGetConnection(dataSource);
        	statement = connection.prepareStatement(SQL_CREATE_NEWS, new String [] {"NEWS_ID"});
        	Long newsId = null;
        	statement.setString(1, news.getTitle());
        	statement.setString(2, news.getShortText());
        	statement.setString(3, news.getFullText());
        	statement.setTimestamp(4, (Timestamp)news.getCreationDate());
        	statement.setDate(5, (Date) news.getModificationDate());
        	statement.executeUpdate();
        	generatedKeys = statement.getGeneratedKeys();
        	if (null != generatedKeys && generatedKeys.next()) {
        		newsId = Long.parseLong(generatedKeys.getString(1));
            }
        	return newsId;        	
        } catch (SQLException e) {
            throw new DAOException("Error while creating news: " + news, e);
        } finally {
        	DBUtil.close(generatedKeys, statement, connection, dataSource);
        }
	}

	/* 
	 * @see by.tanana.newsManagement.dao.NewsDAO#read(java.lang.Long)
	 */
	@Override
	public NewsTO read(Long newsId) throws DAOException {
		Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
        	connection = DataSourceUtils.doGetConnection(dataSource);
        	statement = connection.prepareStatement(SQL_READ_NEWS);
        	statement.setLong(1, newsId);			
        	NewsTO news = null;	
        	resultSet = statement.executeQuery();
        	if (resultSet.next()) {
        		news = new NewsTO();
        		news.setNewsId(resultSet.getLong("NEWS_ID"));;
        		news.setTitle(resultSet.getString("TITLE"));
        		news.setShortText(resultSet.getString("SHORT_TEXT"));
        		news.setFullText(resultSet.getString("FULL_TEXT"));
        		news.setCreationDate(resultSet.getTimestamp("CREATION_DATE"));
        		news.setModificationDate(resultSet.getDate("MODIFICATION_DATE"));
        	}
        	return news;
        } catch (SQLException e) {
        	throw new DAOException("Error while reading news by news id: " + newsId, e);
		} finally {
			DBUtil.close(resultSet, statement, connection, dataSource);
		} 
	}

	/* 
	 * @see by.tanana.newsManagement.dao.NewsDAO#update(by.tanana.newsManagement.entity.NewsTO)
	 */
	@Override
	public void update(NewsTO news) throws DAOException {
		Connection connection = null;
        PreparedStatement statement = null;
        try {
        	connection = DataSourceUtils.doGetConnection(dataSource);
        	statement = connection.prepareStatement(SQL_UPDATE_NEWS);
        	statement.setString(1, news.getTitle());
        	statement.setString(2, news.getShortText());
        	statement.setString(3, news.getFullText());
        	statement.setDate(4, (Date) news.getModificationDate());
        	statement.setLong(5, news.getNewsId());
        	statement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Error while updating news: " + news, e);
        } finally {
        	DBUtil.close(statement, connection, dataSource);
        }
		
	}

	/* 
	 * @see by.tanana.newsManagement.dao.NewsDAO#delete(java.lang.Long)
	 */
	@Override
	public void delete(Long newsId) throws DAOException {
		Connection connection = null;
        PreparedStatement statement = null;
        try {
        	connection = DataSourceUtils.doGetConnection(dataSource);
        	statement = connection.prepareStatement(SQL_DELETE_NEWS);
        	statement.setLong(1, newsId);
        	statement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Error while deleting an author by news id: " + newsId, e);
        } finally {
        	DBUtil.close(statement, connection, dataSource);
        }		
	}

	/* 
	 * @see by.tanana.newsManagement.dao.NewsDAO#countAllNews()
	 */
	@Override
	public int countAllNews() throws DAOException {
		Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
        	connection = DataSourceUtils.doGetConnection(dataSource);
        	statement = connection.createStatement();
        	int numberOfNews = 0;
        	resultSet = statement.executeQuery(SQL_COUNT_NEWS);
        	while (resultSet.next()) {
        		numberOfNews = resultSet.getInt("CNT");
        	}
        	return numberOfNews;
        } catch (SQLException e) {
            throw new DAOException("Error while counting news", e);
        } finally {
        	DBUtil.close(resultSet, statement, connection, dataSource);
        }		
        
	}

	/* 
	 * @see by.tanana.newsManagement.dao.NewsDAO#bindNewsWithTag(java.lang.Long, java.lang.Long)
	 */
	@Override
	public void bindNewsWithTag(Long tagId, Long newsId) throws DAOException {
		Connection connection = null;
        PreparedStatement statement = null;
        try {
        	connection = DataSourceUtils.doGetConnection(dataSource);
        	statement = connection.prepareStatement(SQL_BIND_NEWS_TAG);
        	statement.setLong(1, tagId);
        	statement.setLong(2, newsId);
        	statement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Error while binding tag: id=" + tagId + " with news: id=" + newsId, e);
        } finally {
        	DBUtil.close(statement, connection, dataSource);
        }				
		
	}
	
	/* 
	 * @see by.tanana.newsManagement.dao.NewsDAO#deleteFromNewsTagByTagId(java.lang.Long)
	 */
	@Override
	public void deleteFromNewsTagByTagId(Long tagId) throws DAOException {
		Connection connection = null;
        PreparedStatement statement = null;
        try {
        	connection = DataSourceUtils.doGetConnection(dataSource);
        	statement = connection.prepareStatement(SQL_DELETE_FROM_NEWS_TAG_BY_TAG_ID);
        	statement.setLong(1, tagId);
        	statement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Error while deleting links between tags and news by tag id: " + tagId, e);
        } finally {
        	DBUtil.close(statement, connection, dataSource);
        }		
		
	}

	/* 
	 * @see by.tanana.newsManagement.dao.NewsDAO#readNewsByTagId(java.lang.Long)
	 */
	@Override
	public NewsTO readNewsByTagId(Long tagId) throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			statement = connection.prepareStatement(SQL_READ_NEWS_BY_TAG_ID);
			statement.setLong(1, tagId);			
			NewsTO news = null;		
			resultSet = statement.executeQuery();
			if (resultSet.next()) {
				news = new NewsTO();
				news.setNewsId(Long.parseLong(resultSet.getString("NEWS_ID")));
				news.setTitle(resultSet.getString("TITLE"));
				news.setShortText(resultSet.getString("SHORT_TEXT"));
				news.setFullText(resultSet.getString("FULL_TEXT"));
				news.setCreationDate(resultSet.getTimestamp("CREATION_DATE"));
				news.setModificationDate(resultSet.getDate("MODIFICATION_DATE"));
			}
			return news;
		} catch (SQLException e) {
			throw new DAOException("Error while reading news by tag id: " + tagId, e);
		} finally {
			DBUtil.close(resultSet, statement, connection, dataSource);
		} 		
	}

	/* 
	 * @see by.tanana.newsManagement.dao.NewsDAO#getNews(by.tanana.newsManagement.entity.SearchCriteria)
	 */
	@Override
	public List<NewsTO> getNews(SearchCriteria searchCriteria) throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			String sqlExpression = createQueryWithSearchCriteria(searchCriteria);
			statement = connection.prepareStatement(sqlExpression);
			int i=1;
			if (searchCriteria.getAuthorId() != null) {
				statement.setLong(i, searchCriteria.getAuthorId());
				if (searchCriteria.getTagIdList() != null && !searchCriteria.getTagIdList().isEmpty()) {
					for (Long tagId:searchCriteria.getTagIdList()) {
						statement.setLong(++i, tagId);
					}
				}				
			} 
			else if (searchCriteria.getTagIdList() != null && !searchCriteria.getTagIdList().isEmpty()) {
				for (Long tagId:searchCriteria.getTagIdList()) {
					statement.setLong(i, tagId);
					i++;
				}
			}
			resultSet = statement.executeQuery();
			List<NewsTO> newsList = null;
			if (resultSet.next()) {
				newsList = new ArrayList<NewsTO>();
				do {
					NewsTO news = new NewsTO();
					news.setNewsId(resultSet.getLong("NEWS_ID"));
	        		news.setTitle(resultSet.getString("TITLE"));
	        		news.setShortText(resultSet.getString("SHORT_TEXT"));
	        		news.setFullText(resultSet.getString("FULL_TEXT"));
	        		news.setCreationDate(resultSet.getTimestamp("CREATION_DATE"));
	        		news.setModificationDate(resultSet.getDate("MODIFICATION_DATE"));
					newsList.add(news);	
				} while (resultSet.next());
			}
			return newsList;			
		} catch (SQLException e) {
			throw new DAOException("Error while searching news by search criteria: " + searchCriteria, e);
		} finally {
			DBUtil.close(resultSet, statement, connection, dataSource);
		} 		
	}

	private String createQueryWithSearchCriteria(SearchCriteria searchCriteria) {
		StringBuilder joinStringBuilder = new StringBuilder();
		StringBuilder whereStringBuilder = new StringBuilder();
		joinStringBuilder.append(SQL_START_SELECT_NEWS_BY_SEARCH_CRITERIA);
		whereStringBuilder.append(SQL_ADD_WHERE_CLAUSE);
		if (searchCriteria.getAuthorId() != null) {
			joinStringBuilder.append(SQL_JOIN_AUTHOR);
			whereStringBuilder.append(SQL_ADD_WHERE_CLAUSE_FOR_AUTHOR);
			if (searchCriteria.getTagIdList() != null && !searchCriteria.getTagIdList().isEmpty()) {					
				joinStringBuilder.append(SQL_JOIN_TAGS);
				whereStringBuilder.append(SQL_ADD_WHERE_CLAUSE_FOR_TAGS);
				for (int i=0; i<searchCriteria.getTagIdList().size()-1; i++) {
					whereStringBuilder.append(PREPARED_STATEMENT_ATTRIBUTE);
				}
				whereStringBuilder.append(CLOSING_BRACKET);
			}			
		}
		else if (searchCriteria.getTagIdList() != null && !searchCriteria.getTagIdList().isEmpty()) {
			joinStringBuilder.append(SQL_JOIN_TAGS);
			whereStringBuilder.append(SQL_ADD_WHERE_CLAUSE_FOR_TAGS);
			for (int i=0; i<searchCriteria.getTagIdList().size()-1; i++) {
				whereStringBuilder.append(PREPARED_STATEMENT_ATTRIBUTE);
			}
			whereStringBuilder.append(CLOSING_BRACKET);
		}
		whereStringBuilder.append(SQL_END_SEARCH_CRITERIA);
		joinStringBuilder.append(whereStringBuilder);
		return joinStringBuilder.toString();
	}

}
