package by.tanana.newsManagement.service;


import java.util.List;

import exception.DAOException;
import exception.ServiceException;
import by.tanana.newsManagement.entity.NewsTO;
import by.tanana.newsManagement.entity.SearchCriteria;

public interface NewsService {
	
	/**Create a new NewsTO entity.
	 * @param news
	 * @return news id
	 * @throws ServiceException
	 */
	public Long create(NewsTO news) throws ServiceException;
	
	/**Read news by it's id.
	 * @param newsId
	 * @return news
	 * @throws ServiceException
	 */
	public NewsTO read(Long newsId) throws ServiceException;
	
	/**Read news by tag id.
	 * @param tagId
	 * @return news
	 * @throws DAOException
	 */
	public NewsTO readNewsByTagId(Long tagId) throws ServiceException;
	
	/**Update news by changing some NewsTO's fields.
	 * @param news
	 * @throws ServiceException
	 */
	public void update(NewsTO news) throws ServiceException;
	
	/**Delete news by it's id.
	 * @param newsId
	 * @throws ServiceException
	 */
	public void delete(Long newsId) throws ServiceException;
	
	/**Count all news.
	 * @return number of news
	 * @throws ServiceException
	 */
	public int countAllNews() throws ServiceException;
	
	/**Bind news with tag by their id.
	 * @param tagId
	 * @param newsId
	 * @throws ServiceException
	 */
	public void bindNewsWithTag(Long tagId, Long newsId) throws ServiceException;
	
	/**Unbind news with tag by tagId.
	 * @param tagId
	 * @throws ServiceException
	 */
	public void deleteFromNewsTagByTagId(Long tagId) throws ServiceException;
	
	/**Reading news by search criteria.
	 * @param searchCriteria
	 * @return list of news
	 * @throws ServiceException
	 */
	public List<NewsTO> getNews(SearchCriteria searchCriteria) throws ServiceException;
	
	

}
