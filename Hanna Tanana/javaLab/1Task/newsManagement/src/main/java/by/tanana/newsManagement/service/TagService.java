package by.tanana.newsManagement.service;

import java.util.List;

import exception.ServiceException;
import by.tanana.newsManagement.entity.TagTO;

public interface TagService {
	
	/**Create a new TagTO entity.
	 * @param tag
	 * @return tag id
	 * @throws ServiceException
	 */
	public Long create(TagTO tag) throws ServiceException;
	
	/**Read a tag by it's id.
	 * @param tagId
	 * @return tag
	 * @throws ServiceException
	 */
	public TagTO read(Long tagId) throws ServiceException;
	
	/**Read a list of tags by newsId.
	 * @param newsId
	 * @return list of tags
	 * @throws ServiceException
	 */
	public List<TagTO> readListOfTagsByNewsId(Long newsId) throws ServiceException;
	
	/**Update a tag by changing some TagTO's fields.
	 * @param tag
	 * @throws ServiceException
	 */
	public void update(TagTO tag) throws ServiceException;
	
	/**Delete tag by it's id.
	 * @param tagId
	 * @throws ServiceException
	 */
	public void delete(Long tagId) throws ServiceException;
	
	/**Bind news with multiple tags by their id.
	 * @param list of tag id
	 * @param newsId
	 * @throws ServiceException
	 */
	public void bindMultipleTagsWithNews(List<Long> tagList, Long newsId) throws ServiceException;
	
	/**Unbind news with multiple tags by newsId.
	 * @param newsId
	 * @throws ServiceException
	 */
	public void deleteTagsFromNewsTagByNewsId(Long newsId) throws ServiceException;
}
