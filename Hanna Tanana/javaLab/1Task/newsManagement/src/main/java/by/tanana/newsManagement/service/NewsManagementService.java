package by.tanana.newsManagement.service;

import java.util.List;

import exception.ServiceException;
import by.tanana.newsManagement.entity.AuthorTO;
import by.tanana.newsManagement.entity.NewsTO;


public interface NewsManagementService {
	
	/**Creating news entity, 
	 * bunches between News&Tag and News&Author.
	 * @param news
	 * @param author
	 * @param tags
	 * @return news id
	 * @throws ServiceException
	 */
	public Long addNews(NewsTO news, AuthorTO author, List<Long> tags) throws ServiceException;
	
	/**Delete news by deleting news entity,
	 * bunches Author&News, Tag&News 
	 * and comments of this news.
	 * @param newsId
	 * @throws ServiceException
	 */
	public void deleteNews(Long newsId) throws ServiceException;
	
	/**Delete tag by deleting tag entity
	 * and News&Tag bunches.
	 * @param tagId
	 * @throws ServiceException
	 */
	public void deleteTag(Long tagId) throws ServiceException;
}
