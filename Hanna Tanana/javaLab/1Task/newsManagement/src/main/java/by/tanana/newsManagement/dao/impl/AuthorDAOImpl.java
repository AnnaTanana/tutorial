package by.tanana.newsManagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import exception.DAOException;
import by.tanana.newsManagement.dao.AuthorDAO;
import by.tanana.newsManagement.entity.AuthorTO;
import by.tanana.newsManagement.util.DBUtil;


public class AuthorDAOImpl implements AuthorDAO {

	private static final String SQL_CREATE_AUTHOR 
			= "INSERT INTO author"
			+ "(author_id, author_name, expired)"
			+ "VALUES (author_seq.nextval, ?, ?)";
	
	private static final String SQL_READ_AUTHOR 
			= "SELECT author_id, author_name, expired "
			+ "FROM author "
			+ "WHERE author_id = ?"; 
	
	private static final String SQL_UPDATE_AUTHOR 
			= "UPDATE author "
			+ "SET author_name = ?, expired = ? "
			+ "WHERE author_id = ?";
	
	private static final String SQL_DELETE_AUTHOR 
			= "UPDATE author "
			+ "SET expired = ? "
			+ "WHERE author_id = ?";
	
	private static final String SQL_READ_AUTHOR_BY_NEWS_ID 
			= "SELECT author.author_id, author.author_name, author.expired "
			+ "FROM author "
			+ "JOIN news_author ON news_author.author_id = author.author_id "
			+ "WHERE news_author.news_id = ?";
	
	private static final String SQL_BIND_NEWS_AUTHOR 
			= "INSERT INTO news_author "
			+ "(news_id, author_id) "
			+ "VALUES (?, ?)";
	
	private static final String SQL_DELETE_FROM_NEWS_AUTHOR_BY_NEWS_ID 
			= "DELETE FROM news_author "
			+ "WHERE news_id = ?";
	
	private DataSource dataSource;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}


	/* 
	 * @see by.tanana.newsManagement.dao.AuthorDAO#create(by.tanana.newsManagement.entity.AuthorTO)
	 */
	@Override
	public Long create(AuthorTO author) throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet generatedKeys = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			statement = connection.prepareStatement(SQL_CREATE_AUTHOR, new String [] {"AUTHOR_ID"});
			Long authorId = null;
			statement.setString(1, author.getName());
			statement.setTimestamp(2, (Timestamp)author.getExpired());
			statement.executeUpdate();
			generatedKeys = statement.getGeneratedKeys();
			if (null != generatedKeys && generatedKeys.next()) {
				authorId =  Long.parseLong(generatedKeys.getString(1));
			}
			return authorId;        	
		} catch (SQLException e) {
			throw new DAOException("Error while creating an author: "+author, e);
		} finally {		
			DBUtil.close(generatedKeys, statement, connection, dataSource);	
		}
	}

	/* 
	 * @see by.tanana.newsManagement.dao.AuthorDAO#read(java.lang.Long)
	 */
	@Override
	public AuthorTO read(Long authorId) throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			statement = connection.prepareStatement(SQL_READ_AUTHOR);
			statement.setLong(1, authorId);			
			AuthorTO author = null;		
			resultSet = statement.executeQuery();
			if (resultSet.next()) {
				author = new AuthorTO();
				author.setAuthorId(resultSet.getLong("AUTHOR_ID"));
				author.setName(resultSet.getString("AUTHOR_NAME"));
				author.setExpired(resultSet.getTimestamp("EXPIRED"));
			}
			return author;
		} catch (SQLException e) {
			throw new DAOException("Error while reading an author by author id: "+ authorId, e);
		} finally {
			DBUtil.close(resultSet, statement, connection, dataSource);
		} 
	}

	/* 
	 * @see by.tanana.newsManagement.dao.AuthorDAO#update(by.tanana.newsManagement.entity.AuthorTO)
	 */
	@Override
	public void update(AuthorTO author) throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			statement = connection.prepareStatement(SQL_UPDATE_AUTHOR);
			statement.setString(1, author.getName());
			statement.setTimestamp(2, (Timestamp)author.getExpired());
			statement.setLong(3, author.getAuthorId());
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("Error while updating an author: "+ author, e);
		} finally {
			DBUtil.close(statement, connection, dataSource);
		}
	}

	/* 
	 * @see by.tanana.newsManagement.dao.AuthorDAO#delete(java.lang.Long, java.sql.Timestamp)
	 */
	@Override
	public void delete(Long authorId, Timestamp expiredTime) throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			statement = connection.prepareStatement(SQL_DELETE_AUTHOR);
			statement.setTimestamp(1, expiredTime);
			statement.setLong(2, authorId);
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("Error while deleting an author by author id: "+ authorId, e);
		} finally {
			DBUtil.close(statement, connection, dataSource);
		}		
	}

	/* 
	 * @see by.tanana.newsManagement.dao.AuthorDAO#readByNewsId(java.lang.Long)
	 */
	@Override
	public AuthorTO readByNewsId(Long newsId) throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			statement = connection.prepareStatement(SQL_READ_AUTHOR_BY_NEWS_ID);
			statement.setLong(1, newsId);			
			AuthorTO author = null;		
			resultSet = statement.executeQuery();
			if (resultSet.next()) {
				author = new AuthorTO();
				author.setAuthorId(Long.parseLong(resultSet.getString("AUTHOR_ID")));
				author.setName(resultSet.getString("AUTHOR_NAME"));
				author.setExpired(resultSet.getTimestamp("EXPIRED"));
			}
			return author;
		} catch (SQLException e) {
			throw new DAOException("Error while reading an author by news id: "+newsId, e);
		} finally {
			DBUtil.close(resultSet, statement, connection, dataSource);
		} 


	}

	/* 
	 * @see by.tanana.newsManagement.dao.AuthorDAO#bindNewsWithAuthor(java.lang.Long, java.lang.Long)
	 */
	@Override
	public void bindNewsWithAuthor(Long authorId, Long newsId)
			throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			statement = connection.prepareStatement(SQL_BIND_NEWS_AUTHOR);
			statement.setLong(1, newsId);
			statement.setLong(2, authorId);
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("Error while binding author: id="+authorId+ " with news: id= "+newsId, e);
		} finally {
			DBUtil.close(statement, connection, dataSource);
		}				
	}

	/* 
	 * @see by.tanana.newsManagement.dao.AuthorDAO#deleteFromNewsAuthorByNewsId(java.lang.Long)
	 */
	@Override
	public void deleteFromNewsAuthorByNewsId(Long newsId) throws DAOException {
		Connection connection = null;
        PreparedStatement statement = null;
        try {
        	connection = DataSourceUtils.doGetConnection(dataSource);
        	statement = connection.prepareStatement(SQL_DELETE_FROM_NEWS_AUTHOR_BY_NEWS_ID);
        	statement.setLong(1, newsId);
        	statement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Error while deleting links between authors and news by news id: "+newsId, e);
        } finally {
        	DBUtil.close(statement, connection, dataSource);
        }		
		
	}


}
