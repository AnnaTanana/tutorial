package by.tanana.newsManagement.dao;

import java.sql.Timestamp;

import exception.DAOException;
import by.tanana.newsManagement.entity.AuthorTO;

public interface AuthorDAO {
		
	/**Insert a new author entity to database
	 * by auto generating it's id.
	 * @param author
	 * @return generated author id
	 * @throws DAOException
	 */
	public Long create(AuthorTO author) throws DAOException;
	
	/**Read an author from database
	 * by author's id.
	 * @param authorId
	 * @return author
	 * @throws DAOException
	 */
	public AuthorTO read(Long authorId) throws DAOException;
	
	/**Read an author from database by newsId 
	 * using NEWS_AUTHOR table to get authorId.
	 * @param newsId
	 * @return author
	 * @throws DAOException
	 */
	public AuthorTO readByNewsId(Long newsId) throws DAOException;
	
	/**Update an author by changing some AuthorTO's fields.
	 * @param author
	 * @throws DAOException
	 */
	public void update(AuthorTO author) throws DAOException;
	
	/**Expire an author by changing the expiration date.
	 * @param authorId
	 * @param expiredTime
	 * @throws DAOException
	 */
	public void delete(Long authorId, Timestamp expiredTime) throws DAOException;
	
	/**Bind news with author 
	 * by inserting newsId and authorId in NEWS_AUTHOR table.
	 * @param authorId
	 * @param newsId
	 * @throws DAOException
	 */
	public void bindNewsWithAuthor(Long authorId, Long newsId) throws DAOException;
	
	/**Unbind news with author
	 * by deleting a bunch from NEWS_AUTHOR table.
	 * @param newsId
	 * @throws DAOException
	 */
	public void deleteFromNewsAuthorByNewsId(Long newsId) throws DAOException;

}
