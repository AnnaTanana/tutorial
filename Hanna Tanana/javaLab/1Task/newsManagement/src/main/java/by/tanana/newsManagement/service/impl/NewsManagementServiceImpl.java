package by.tanana.newsManagement.service.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import exception.ServiceException;
import by.tanana.newsManagement.entity.AuthorTO;
import by.tanana.newsManagement.entity.NewsTO;
import by.tanana.newsManagement.service.AuthorService;
import by.tanana.newsManagement.service.CommentService;
import by.tanana.newsManagement.service.NewsManagementService;
import by.tanana.newsManagement.service.NewsService;
import by.tanana.newsManagement.service.TagService;

@Transactional(rollbackFor=ServiceException.class)
public class NewsManagementServiceImpl implements NewsManagementService {

	private AuthorService authorService;
	private CommentService commentService;
	private NewsService newsService;
	private TagService tagService;

	public void setAuthorService(AuthorService authorService) {
		this.authorService = authorService;
	}

	public void setCommentService(CommentService commentService) {
		this.commentService = commentService;
	}

	public void setNewsService(NewsService newsService) {
		this.newsService = newsService;
	}

	public void setTagService(TagService tagService) {
		this.tagService = tagService;
	}

	/* 
	 * @see by.tanana.newsManagement.service.NewsManagementService#addNews(by.tanana.newsManagement.entity.NewsTO, by.tanana.newsManagement.entity.AuthorTO, java.util.List)
	 */
	@Override
	public Long addNews(NewsTO news, AuthorTO author, List<Long> tags)
			throws ServiceException {
		Long newsId = newsService.create(news);
		authorService.bindNewsWithAuthor(author.getAuthorId(), newsId);
		tagService.bindMultipleTagsWithNews(tags, newsId);
		return newsId;
	}

	/* 
	 * @see by.tanana.newsManagement.service.NewsManagementService#deleteNews(java.lang.Long)
	 */
	@Override
	public void deleteNews(Long newsId) throws ServiceException {
		newsService.delete(newsId);
		authorService.deleteAuthorsFromNewsAuthorByNewsId(newsId);
		tagService.deleteTagsFromNewsTagByNewsId(newsId);
		commentService.deleteCommentByNewsId(newsId);
	}

	/* 
	 * @see by.tanana.newsManagement.service.NewsManagementService#deleteTag(java.lang.Long)
	 */
	@Override
	public void deleteTag(Long tagId) throws ServiceException {
		tagService.delete(tagId);
		newsService.deleteFromNewsTagByTagId(tagId);		
	}		
}
