package by.tanana.newsManagement.service;

import java.sql.Timestamp;

import exception.ServiceException;
import by.tanana.newsManagement.entity.AuthorTO;

public interface AuthorService {
	
	/**Create a new authorTO entity.
	 * @param author
	 * @return author id
	 * @throws ServiceException
	 */
	public Long create(AuthorTO author) throws ServiceException;
	
	/**Read an author by it's id.
	 * @param authorId
	 * @return author
	 * @throws ServiceException
	 */
	public AuthorTO read(Long authorId) throws ServiceException;
	
	/**Read an author by news id.
	 * @param newsId
	 * @return author
	 * @throws ServiceException
	 */
	public AuthorTO readByNewsId(Long newsId) throws ServiceException;
	
	/**Update an author by changing some AuthorTO's fields.
	 * @param author
	 * @throws ServiceException
	 */
	public void update(AuthorTO author) throws ServiceException;
	
	/**Expire an author by changing the expiration date.
	 * @param authorId
	 * @param expiredTime
	 * @throws ServiceException
	 */
	public void delete(Long authorId, Timestamp expiredTime) throws ServiceException;
	
	/**Bind news with author by their id.
	 * @param authorId
	 * @param newsId
	 * @throws ServiceException
	 */
	public void bindNewsWithAuthor(Long authorId, Long newsId) throws ServiceException;
	
	/**Unbind news with author by newsId.
	 * @param newsId
	 * @throws ServiceException
	 */
	public void deleteAuthorsFromNewsAuthorByNewsId(Long newsId) throws ServiceException;
}
