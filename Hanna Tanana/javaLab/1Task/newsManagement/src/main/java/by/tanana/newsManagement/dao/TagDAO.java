package by.tanana.newsManagement.dao;

import java.util.List;

import exception.DAOException;
import by.tanana.newsManagement.entity.TagTO;

public interface TagDAO {
	
	/**Insert a new tag entity to database
	 * by auto generating it's id.
	 * @param tag
	 * @return generated tag id
	 * @throws DAOException
	 */
	public Long create(TagTO tag) throws DAOException;
	
	/**Read a tag from database
	 * by tag's id.
	 * @param tagId
	 * @return tag
	 * @throws DAOException
	 */
	public TagTO read(Long tagId) throws DAOException;
	
	/**Update a tag by changing some TagTO's fields.
	 * @param tag
	 * @throws DAOException
	 */
	public void update(TagTO tag) throws DAOException;
	
	/**Deleting tag by it's id from database.
	 * @param tagId
	 * @throws DAOException
	 */
	public void delete(Long tagId) throws DAOException;
	
	/**Read a list of tags from database by newsId 
	 * using NEWS_TAG table to get tagId.
	 * @param newsId
	 * @return list of tags
	 * @throws DAOException
	 */
	public List<TagTO> readListOfTagsByNewsId(Long newsId) throws DAOException;
	
	/**Bind tags with news 
	 * by inserting newsId and tagId in NEWS_TAG table.
	 * @param a list of tag's id
	 * @param newsId
	 * @throws DAOException
	 */
	public void bindMultipleTagsWithNews(List<Long> tagList, Long newsId) throws DAOException;
	
	/**Unbind tags with news
	 * by deleting bunches from NEWS_TAG table.
	 * @param newsId
	 * @throws DAOException
	 */
	public void deleteFromNewsTagByNewsId(Long newsId) throws DAOException;
}
