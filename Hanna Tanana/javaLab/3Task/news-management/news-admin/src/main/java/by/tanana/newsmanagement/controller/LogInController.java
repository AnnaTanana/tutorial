package by.tanana.newsmanagement.controller;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class LogInController {

    @Autowired
    private MessageSource messageSource;

    @RequestMapping({"/"})
    public String defaultPage(Model model) {
	return "login";

    }

    @RequestMapping({"/login"})
    public String login(@RequestParam(value = "error", required = false) String error, Locale locale, Model model) {

	if (error != null) {
	    model.addAttribute("error", messageSource.getMessage("message.loginError", null, locale));
	}
	return "login";

    }


    //for 403 access denied page
    @RequestMapping({"/403"})
    public String accesssDenied(Model model) {

	//check if user is login
	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	if (!(auth instanceof AnonymousAuthenticationToken)) {
	    UserDetails userDetail = (UserDetails) auth.getPrincipal();	
	    model.addAttribute("username", userDetail.getUsername());
	}
	return "403";
    }

}
