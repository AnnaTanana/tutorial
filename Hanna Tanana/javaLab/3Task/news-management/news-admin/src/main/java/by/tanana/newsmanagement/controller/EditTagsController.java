package by.tanana.newsmanagement.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import exception.ServiceException;
import by.tanana.newsManagement.entity.Tag;
import by.tanana.newsManagement.service.TagService;
import by.tanana.newsmanagement.exception.ControllerException;

@Controller
public class EditTagsController extends BaseController {

    private static final Logger logger = LoggerFactory.getLogger(EditTagsController.class);

    @Autowired
    private TagService tagService;

    @RequestMapping({"/editTags"})
    public String editTags(Model model) throws ControllerException {
	try {
	    List<Tag> tags = tagService.getTags();
	    model.addAttribute("tags", tags);
	    if (!model.containsAttribute("tagForm")) {
		model.addAttribute("tagForm", new Tag());
	    }
	} catch (ServiceException e) {
	    logger.error(e.getMessage());
	    throw new ControllerException(e.getMessage(), e);
	}
	return "editTags";
    }

    @RequestMapping({"/editTag"})
    public String editTag(@ModelAttribute("tagForm") @Valid Tag tagForm, BindingResult result,
	    HttpServletRequest request, RedirectAttributes redirectAttributes, @RequestParam(value="command") String command) throws ControllerException {
	if (!result.hasErrors()) {
	    try {
		if ("delete".equals(command.toLowerCase())) {
		    tagService.delete(tagForm.getTagId());
		} else if ("update".equals(command.toLowerCase())) {
		    tagService.update(tagForm);
		} } catch (ServiceException e) {
		    logger.error(e.getMessage());
		    throw new ControllerException(e.getMessage(), e);
		} 
	} else {
	    BaseController.handleValidationErrors(redirectAttributes, result);
	}
	return "redirect:/editTags";
    }

    @RequestMapping({"/addTag"})
    public String addAuthor(@ModelAttribute("tagForm") @Valid Tag tagForm, BindingResult result,
	    HttpServletRequest request, RedirectAttributes redirectAttributes) throws ControllerException {
	if (!result.hasErrors()) {
	    try {
		tagService.create(tagForm);
	    } catch (ServiceException e) {
		logger.error(e.getMessage());
		throw new ControllerException(e.getMessage(), e);
	    }

	} else {
	    BaseController.handleValidationErrors(redirectAttributes, result);
	}
	return "redirect:/editTags";
    }
}
