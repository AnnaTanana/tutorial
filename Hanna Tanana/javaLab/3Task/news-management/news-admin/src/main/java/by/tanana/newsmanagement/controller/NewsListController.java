package by.tanana.newsmanagement.controller;


import java.util.Arrays;
import java.util.List;
import java.util.Locale;




import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import exception.ServiceException;
import by.tanana.newsManagement.entity.Author;
import by.tanana.newsManagement.entity.News;
import by.tanana.newsManagement.entity.SearchCriteria;
import by.tanana.newsManagement.entity.Tag;
import by.tanana.newsManagement.service.AuthorService;
import by.tanana.newsManagement.service.NewsService;
import by.tanana.newsManagement.service.TagService;
import by.tanana.newsmanagement.exception.ControllerException;
import by.tanana.newsmanagement.utils.Pagination;

@Controller
public class NewsListController {

    private static final Logger logger = LoggerFactory.getLogger(NewsListController.class);

    @Value("${pagination.newsPerPage}")
    private int numberOfNews;
    
    @Autowired
    private TagService tagService;

    @Autowired
    private AuthorService authorService;

    @Autowired
    private NewsService newsService;

    @Autowired
    private MessageSource messageSource;

    @RequestMapping({"/newsList"})
    public String showNewsList(Model model, Locale locale, HttpServletRequest request) throws ControllerException {
	try {
	    //init author list
	    List<Author> authorsList = authorService.getAuthors();
	    model.addAttribute("authorsList", authorsList);

	    List<Tag> tagList = tagService.getTags();
	    model.addAttribute("tagList", tagList);

	    SearchCriteria searchCriteriaFromSession = (SearchCriteria) request.getSession().getAttribute("searchCriteria");

	    SearchCriteria searchCriteria = (searchCriteriaFromSession != null) ? searchCriteriaFromSession : new SearchCriteria();
	   
	    Long numberOfSearchedNews = newsService.countSearchedNews(searchCriteria);
    
	    //prepare pagination
	    Pagination pagination = new Pagination();
	    pagination.defineCurrentPage(request, numberOfSearchedNews);
	    pagination.defineIndex(numberOfNews, numberOfSearchedNews);

	    List<News> newsList = newsService.getNews(searchCriteria, 
		    pagination.getFirstIndex(), 
		    pagination.getLastIndex());

	    if (newsList != null && !newsList.isEmpty()) {
		model.addAttribute("newsList", newsList);
		model.addAttribute("pages", pagination.definePages(numberOfSearchedNews));
		model.addAttribute("selectedPage", pagination.getCurrentPage());
	    }
	    else {
		model.addAttribute("emptyNewsListMess", messageSource.getMessage("message.emptyNewsList", null, locale));
	    }
	} catch (ServiceException e) {
	    logger.error(e.getMessage());
	    throw new ControllerException(e.getMessage(), e);
	}
	model.addAttribute("searchCriteria", new SearchCriteria());
	return "newsList";
    }

    @RequestMapping({"/filter"})
    public String filter(SearchCriteria searchCriteria, HttpServletRequest request){
	request.getSession().setAttribute("searchCriteria", searchCriteria);
	return "redirect:" + "newsList";
    }

    @RequestMapping({"/reset"})
    public String reset(HttpServletRequest request) {
	SearchCriteria searchCriteria = new SearchCriteria();
	request.getSession().setAttribute("searchCriteria", searchCriteria);
	return "redirect:" + "newsList";

    }

    @RequestMapping({"/deleteNews"})
    public String deleteNews(@RequestParam(value="newsId") Long[] newsIdArr, RedirectAttributes redirectAttributes, Locale locale) throws ControllerException{
	try {
	    newsService.delete(Arrays.asList(newsIdArr));
	}
	catch (ServiceException e) {
	    logger.error(e.getMessage());
	    throw new ControllerException(e.getMessage(), e);
	}
	return "redirect:" + "newsList";
    }

}
