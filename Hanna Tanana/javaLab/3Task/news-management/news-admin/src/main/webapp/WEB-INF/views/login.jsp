<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@page session="true"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">




<form class="login-form" name='loginForm'
	action="<c:url value='/login' />" method='POST'>
	<div class="form-row">
		<div class="label">
			<spring:message code="label.login" />
			:
		</div>
		<input type='text' name='username'>
	</div>
	<div class="form-row">
		<div class="label">
			<spring:message code="label.password" />
			:
		</div>
		<input type='password' name='password' />
		<c:if test="${not empty error}">
			<div class="error-message">${error}</div>
		</c:if>
		<c:if test="${not empty msg}">
			<div class="error-message">${msg}</div>
		</c:if>
	</div>
	<div class="form-row">
		<div class="form-cell"></div>
		<div class="form-cell">
			<input class="submit-foorm-button" name="submit" type="submit"
				value="<spring:message code="button.login"/>" />
		</div>


	</div>

	<input type="hidden" name="${_csrf.parameterName}"
		value="${_csrf.token}" />
</form>