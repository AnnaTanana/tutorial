<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles-extras"
	prefix="tilesx"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<c:url var="cssStyle" value="/resources/css/sumoselect.css" />
<link href="${cssStyle}" rel="stylesheet">

<c:url var="css1Style" value="/resources/css/jquery-ui.css" />
<link href="${css1Style}" rel="stylesheet">

<c:url var="css2Style" value="/resources/css/jquery.datetimepicker.css" />
<link href="${css2Style}" rel="stylesheet">

<c:url var="css3Style" value="/resources/css/style.css" />
<link href="${css3Style}" rel="stylesheet">

<c:url var="js1Style" value="/resources/js/jquery-1.11.2.min.js" />
<script src="${js1Style}"></script>

<c:url var="jsStyle" value="/resources/js/jquery.sumoselect.min.js" />
<script src="${jsStyle}"></script>

<c:url var="js2Style" value="/resources/js/jquery-ui.min.js" />
<script src="${js2Style}"></script>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><tiles:getAsString name="title" /></title>
</head>
<body>
	<div id="header">
		<tiles:insertAttribute name="header" />
	</div>
	<div id="content">

		<div id="body">
			<tiles:insertAttribute name="body" />
		</div>
	</div>

	<div id="footer">
		<tiles:insertAttribute name="footer" />
	</div>
</body>
</html>

