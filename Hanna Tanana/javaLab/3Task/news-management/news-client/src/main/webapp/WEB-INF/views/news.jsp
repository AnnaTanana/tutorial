<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<div>
<div class="title">
	<c:out value="${newsVO.news.title}"></c:out>
</div>
<div class="author">
	<c:out value="(by ${newsVO.author.name})"></c:out>
</div>
</div>
<fmt:message var="format" key="date.format" />
<fmt:formatDate var="date" value="${newsVO.news.modificationDate}"
	pattern="${format}"></fmt:formatDate>
<div class="label">
	<c:out value="${date}"></c:out>
</div>
<div class="text">
<c:out value="${newsVO.news.fullText}"></c:out>
</div>

<c:forEach var="comment" items="${newsVO.comments}" varStatus="status">
	<form name="deleteCommentForm"
		action="<c:url value='/deleteComment' />" method="POST">

		<fmt:formatDate var="date1" value="${comment.creationDate}"
			pattern="${format}"></fmt:formatDate>
		<div class="label"><c:out value="${date1}"></c:out></div>

		<div class="comment-text"><c:out value="${comment.text}"></c:out>
		 </div>
		<input type="hidden" name="commentId" value="${comment.commentId}">
		<input type="hidden" name="newsId" value="${newsVO.news.newsId}">

	</form>
</c:forEach>


<form:form name="leaveCommentForm" method="POST"
	action="/news-admin/leaveComment" commandName="commentForm">
	<input type="hidden" name="newsId" value="${newsVO.news.newsId}">
	<form:textarea path="text" ROWS="3" COLS="30"></form:textarea>
	<div class="error-message"><form:errors path="text" cssClass="error" /></div>
	<input class="submit-foorm-button" type="submit"
		value="<spring:message code="button.postComment"/>" />
</form:form>