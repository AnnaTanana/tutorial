package by.tanana.newsmanagement.controller;

import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

public class BaseController {
    
    public static void handleValidationErrors(RedirectAttributes redirectAttributes, BindingResult result) {
	 redirectAttributes.addFlashAttribute(BindingResult.MODEL_KEY_PREFIX + result.getObjectName(), result);
	 redirectAttributes.addFlashAttribute(result.getObjectName(), result.getTarget());
    }

}
