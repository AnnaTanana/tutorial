package by.tanana.newsManagement.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import exception.DAOException;
import exception.OptimisticLockDAOException;
import exception.OptimisticLockServiceException;
import exception.ServiceException;
import by.tanana.newsManagement.dao.NewsDAO;
import by.tanana.newsManagement.entity.Author;
import by.tanana.newsManagement.entity.News;
import by.tanana.newsManagement.entity.NewsVOForm;
import by.tanana.newsManagement.entity.SearchCriteria;
import by.tanana.newsManagement.entity.Tag;
import by.tanana.newsManagement.service.NewsService;

/**
 * @author Hanna_Tanana
 *
 */
/**
 * @author Hanna_Tanana
 *
 */
public class NewsServiceImpl implements NewsService {

    private static final Logger logger = LoggerFactory.getLogger(NewsServiceImpl.class);

    private NewsDAO newsDAO;

    public void setNewsDAO(NewsDAO newsDAO) {
	this.newsDAO = newsDAO;
    }

    /* 
     * @see by.tanana.newsManagement.service.NewsService#create(by.tanana.newsManagement.entity.News)
     */
    @Override
    public Long create(NewsVOForm newsForm) throws ServiceException {
	try {
	    News news = newsForm.getNews();
	    Author author = new Author();
	    author.setAuthorId(newsForm.getAuthorId());
	    if (newsForm.getTagIdList() != null) {
		List<Tag> tagList = new ArrayList<Tag>();
		for (Long tagId: newsForm.getTagIdList()) {
		    Tag tag = new Tag();
		    tag.setTagId(tagId);
		    tagList.add(tag);
		}
		news.setTagList(tagList);
	    }
	    news.setAuthor(author);

	    return newsDAO.create(news);
	} catch (DAOException e) {
	    logger.error(e.getMessage());
	    throw new ServiceException(e.getMessage(), e);
	}
    }

    /* 
     * @see by.tanana.newsManagement.service.NewsService#read(java.lang.Long)
     */
    @Override
    public News read(Long newsId) throws ServiceException {
	try {
	    return newsDAO.read(newsId);
	} catch (DAOException e) {
	    logger.error(e.getMessage());
	    throw new ServiceException(e.getMessage(), e);
	}
    }

    /* 
     * @see by.tanana.newsManagement.service.NewsService#update(by.tanana.newsManagement.entity.News)
     */
    @Override
    public void update(NewsVOForm newsForm) throws ServiceException, OptimisticLockServiceException {
	try {
	    News news = newsForm.getNews();
	    Author author = new Author();
	    author.setAuthorId(newsForm.getAuthorId());
	    if (newsForm.getTagIdList() != null) {
		List<Tag> tagList = new ArrayList<Tag>();
		for (Long tagId: newsForm.getTagIdList()) {
		    Tag tag = new Tag();
		    tag.setTagId(tagId);
		    tagList.add(tag);
		}
		news.setTagList(tagList);
	    }
	    news.setAuthor(author);

	    newsDAO.update(news);
	} catch (OptimisticLockDAOException e) {
	    logger.error(e.getMessage());
	    throw new OptimisticLockServiceException(e);
	}
	catch (DAOException e) {
	    logger.error(e.getMessage());
	    throw new ServiceException(e.getMessage(), e);
	}

    }

    /* 
     * @see by.tanana.newsManagement.service.NewsService#delete(java.lang.Long)
     */
    @Override
    public void delete(List<Long> listNewsIds) throws ServiceException {
	try {
	    newsDAO.delete(listNewsIds);
	} catch (DAOException e) {
	    logger.error(e.getMessage());
	    throw new ServiceException(e.getMessage(), e);
	}

    }

    /* 
     * @see by.tanana.newsManagement.service.NewsService#getNews(by.tanana.newsManagement.entity.SearchCriteria)
     */
    @Override
    public List<News> getNews(SearchCriteria searchCriteria, int startIndex, int endIndex) throws ServiceException {
	try {
	    return newsDAO.getNews(searchCriteria, startIndex, endIndex);
	} catch (DAOException e) {
	    logger.error(e.getMessage());
	    throw new ServiceException(e.getMessage(), e);
	}		
    }


    /* 
     * @see by.tanana.newsManagement.service.NewsService#countSearchedNews(by.tanana.newsManagement.entity.SearchCriteria)
     */
    @Override
    public Long countSearchedNews(SearchCriteria searchCriteria)
	    throws ServiceException {
	try {
	    return newsDAO.countSearchedNews(searchCriteria);
	} catch (DAOException e) {
	    logger.error(e.getMessage());
	    throw new ServiceException(e.getMessage(), e);
	}	
    }

    @Override
    public NewsVOForm getNewsVOForm(Long newsId) throws ServiceException {
	try {
	    News news = newsDAO.read(newsId);
	    NewsVOForm newsForm = new NewsVOForm();
	    newsForm.setNews(news);
	    newsForm.setAuthorId(news.getAuthor().getAuthorId());
	    List<Long> tagIdList = new ArrayList<Long>();
	    for (Tag tag: news.getTagList()) {
		tagIdList.add(tag.getTagId());
	    }
	    newsForm.setTagIdList(tagIdList);
	    return newsForm;
	} catch (DAOException e) {
	    logger.error(e.getMessage());
	    throw new ServiceException(e.getMessage(), e);
	}	
    }
    
    

}
