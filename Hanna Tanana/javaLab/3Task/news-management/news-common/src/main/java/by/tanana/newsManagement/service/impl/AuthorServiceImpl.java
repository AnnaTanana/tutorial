package by.tanana.newsManagement.service.impl;

import java.sql.Timestamp;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import exception.DAOException;
import exception.ServiceException;
import by.tanana.newsManagement.dao.AuthorDAO;
import by.tanana.newsManagement.entity.Author;
import by.tanana.newsManagement.service.AuthorService;

public class AuthorServiceImpl implements AuthorService{

	private static final Logger logger = LoggerFactory.getLogger(AuthorServiceImpl.class);
	
	private AuthorDAO authorDAO;

	public void setAuthorDAO(AuthorDAO authorDAO) {
		this.authorDAO = authorDAO;
	}

	/* 
	 * @see by.tanana.newsManagement.service.AuthorService#create(by.tanana.newsManagement.entity.Author)
	 */
	@Override
	public Long create(Author author) throws ServiceException {
		try {
			return authorDAO.create(author);
		} catch (DAOException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e.getMessage(), e);
		}
	}
	
	/* 
	 * @see by.tanana.newsManagement.service.AuthorService#read(java.lang.Long)
	 */
	@Override
	public Author read(Long authorId) throws ServiceException {
	    try {
		return authorDAO.read(authorId);
	} catch (DAOException e) {
		logger.error(e.getMessage());
		throw new ServiceException(e.getMessage(), e);
	}
	}

	/* 
	 * @see by.tanana.newsManagement.service.AuthorService#update(by.tanana.newsManagement.entity.Author)
	 */
	@Override
	public void update(Author author) throws ServiceException {
		try {
			authorDAO.update(author);
		} catch (DAOException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e.getMessage(), e);
		}
		
	}

	/* 
	 * @see by.tanana.newsManagement.service.AuthorService#delete(java.lang.Long, java.sql.Timestamp)
	 */
	@Override
	public void delete(Long authorId, Timestamp expiredTime) throws ServiceException {
		try {
			authorDAO.delete(authorId, expiredTime);
		} catch (DAOException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e.getMessage(), e);
		}
		
	}


	@Override
	public List<Author> getAuthors() throws ServiceException {
		try {
			return authorDAO.getAuthors();
		} catch (DAOException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e.getMessage(), e);
		}
	}

}
