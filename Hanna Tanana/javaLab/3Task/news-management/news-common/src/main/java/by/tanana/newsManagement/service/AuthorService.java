package by.tanana.newsManagement.service;

import java.sql.Timestamp;
import java.util.List;

import exception.ServiceException;
import by.tanana.newsManagement.entity.Author;

public interface AuthorService {
	
	/**Create a new authorTO entity.
	 * @param author
	 * @return author id
	 * @throws ServiceException
	 */
	public Long create(Author author) throws ServiceException;
	
	/**Read an author by it's id.
	 * @param authorId
	 * @return author
	 * @throws ServiceException
	 */
	public Author read(Long authorId) throws ServiceException;
	
	/**Update an author by changing some Author's fields.
	 * @param author
	 * @throws ServiceException
	 */
	public void update(Author author) throws ServiceException;
	
	/**Expire an author by changing the expiration date.
	 * @param authorId
	 * @param expiredTime
	 * @throws ServiceException
	 */
	public void delete(Long authorId, Timestamp expiredTime) throws ServiceException;
	
	/**Get all authors.
	 * @return list of authors
	 * @throws ServiceException
	 */
	public List<Author> getAuthors() throws ServiceException;
}
