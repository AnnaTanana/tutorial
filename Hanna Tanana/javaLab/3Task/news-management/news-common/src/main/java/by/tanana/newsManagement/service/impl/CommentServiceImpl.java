package by.tanana.newsManagement.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import exception.DAOException;
import exception.ServiceException;
import by.tanana.newsManagement.dao.CommentDAO;
import by.tanana.newsManagement.entity.Comment;
import by.tanana.newsManagement.service.CommentService;

public class CommentServiceImpl implements CommentService {

	private static final Logger logger = LoggerFactory.getLogger(CommentServiceImpl.class);
	
	private CommentDAO commentDAO;

	public void setCommentDAO(CommentDAO commentDAO) {
		this.commentDAO = commentDAO;
	}

	/* 
	 * @see by.tanana.newsManagement.service.CommentService#create(by.tanana.newsManagement.entity.Comment)
	 */
	@Override
	public Long create(Comment comment) throws ServiceException {
		try {
			return commentDAO.create(comment);
		} catch (DAOException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e.getMessage(), e);
		}
	}

	/* 
	 * @see by.tanana.newsManagement.service.CommentService#read(java.lang.Long)
	 */
	@Override
	public Comment read(Long commentId) throws ServiceException {
	    try {
		return commentDAO.read(commentId);
	} catch (DAOException e) {
		logger.error(e.getMessage());
		throw new ServiceException(e.getMessage(), e);
	}
	}

	/* 
	 * @see by.tanana.newsManagement.service.CommentService#update(by.tanana.newsManagement.entity.Comment)
	 */
	@Override
	public void update(Comment comment) throws ServiceException {
	    try {
		commentDAO.update(comment);
	} catch (DAOException e) {
		logger.error(e.getMessage());
		throw new ServiceException(e.getMessage(), e);
	}
	    
	}

	/* 
	 * @see by.tanana.newsManagement.service.CommentService#delete(java.lang.Long)
	 */
	@Override
	public void delete(Long commentId) throws ServiceException {
		try {
			commentDAO.delete(commentId);
		} catch (DAOException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e.getMessage(), e);
		}
		
	}

}
