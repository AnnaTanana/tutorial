package by.tanana.newsManagement.dao;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import exception.DAOException;
import by.tanana.newsManagement.entity.Author;

@Transactional(rollbackFor=DAOException.class)
public interface AuthorDAO {
		
	/**Insert a new author entity to database
	 * by auto generating it's id.
	 * @param author
	 * @return generated author id
	 * @throws DAOException
	 */
	public Long create(Author author) throws DAOException;
	
	/**Read an author from database
	 * by author's id.
	 * @param authorId
	 * @return author
	 * @throws DAOException
	 */
	public Author read(Long authorId) throws DAOException;
		
	/**Update an author by changing some Author's fields.
	 * @param author
	 * @throws DAOException
	 */
	public void update(Author author) throws DAOException;
	
	/**Expire an author by changing the expiration date.
	 * @param authorId
	 * @param expiredTime
	 * @throws DAOException
	 */
	public void delete(Long authorId, Timestamp expiredTime) throws DAOException;
	
	/**Get all authors from database.
	 * @return list of authors
	 * @throws DAOException
	 */
	public List<Author> getAuthors() throws DAOException;

}
