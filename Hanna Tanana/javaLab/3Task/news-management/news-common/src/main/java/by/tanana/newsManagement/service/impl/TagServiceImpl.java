package by.tanana.newsManagement.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import exception.DAOException;
import exception.ServiceException;
import by.tanana.newsManagement.dao.TagDAO;
import by.tanana.newsManagement.entity.Tag;
import by.tanana.newsManagement.service.TagService;

public class TagServiceImpl implements TagService {

	private static final Logger logger = LoggerFactory.getLogger(TagServiceImpl.class);
	
	private TagDAO tagDAO;
	
	public void setTagDAO(TagDAO tagDAO) {
		this.tagDAO = tagDAO;
	}

	/* 
	 * @see by.tanana.newsManagement.service.TagService#create(by.tanana.newsManagement.entity.Tag)
	 */
	@Override
	public Long create(Tag tag) throws ServiceException {
		try {
			return tagDAO.create(tag);
		} catch (DAOException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e.getMessage(), e);
		}
	}

	/* 
	 * @see by.tanana.newsManagement.service.TagService#read(java.lang.Long)
	 */
	@Override
	public Tag read(Long tagId) throws ServiceException {
		try {
			return tagDAO.read(tagId);
		} catch (DAOException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e.getMessage(), e);
		}
	}

	/* 
	 * @see by.tanana.newsManagement.service.TagService#update(by.tanana.newsManagement.entity.Tag)
	 */
	@Override
	public void update(Tag tag) throws ServiceException {
		try {
			tagDAO.update(tag);
		} catch (DAOException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e.getMessage(), e);
		}		
	}

	/* 
	 * @see by.tanana.newsManagement.service.TagService#delete(java.lang.Long)
	 */
	@Override
	public void delete(Long tagId) throws ServiceException {
		try {
			tagDAO.delete(tagId);
		} catch (DAOException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e.getMessage(), e);
		}
		
	}

	/* 
	 * @see by.tanana.newsManagement.service.TagService#getTags()
	 */
	@Override
	public List<Tag> getTags() throws ServiceException {
		try {
			return tagDAO.getTags();
		} catch (DAOException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e.getMessage(), e);
		}
	}

}
