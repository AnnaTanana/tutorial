package by.tanana.newsManagement.service;

import java.util.List;

import exception.ServiceException;
import by.tanana.newsManagement.entity.Tag;

public interface TagService {
	
	/**Create a new Tag entity.
	 * @param tag
	 * @return tag id
	 * @throws ServiceException
	 */
	public Long create(Tag tag) throws ServiceException;
	
	/**Read a tag by it's id.
	 * @param tagId
	 * @return tag
	 * @throws ServiceException
	 */
	public Tag read(Long tagId) throws ServiceException;
	
	/**Update a tag by changing some Tag's fields.
	 * @param tag
	 * @throws ServiceException
	 */
	public void update(Tag tag) throws ServiceException;
	
	/**Delete tag by it's id.
	 * @param tagId
	 * @throws ServiceException
	 */
	public void delete(Long tagId) throws ServiceException;
	
	/**Get all tags.
	 * @return list of tags
	 * @throws ServiceException
	 */
	public List<Tag> getTags() throws ServiceException;
}
