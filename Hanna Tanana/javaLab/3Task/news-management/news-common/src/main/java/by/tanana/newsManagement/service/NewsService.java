package by.tanana.newsManagement.service;


import java.util.List;

import exception.OptimisticLockServiceException;
import exception.ServiceException;
import by.tanana.newsManagement.entity.News;
import by.tanana.newsManagement.entity.NewsVOForm;
import by.tanana.newsManagement.entity.SearchCriteria;

public interface NewsService {
	
	/**Create a new News entity.
	 * @param news
	 * @return news id
	 * @throws ServiceException
	 */
	public Long create(NewsVOForm newsForm) throws ServiceException;
	
	/**Read news by it's id.
	 * @param newsId
	 * @return news
	 * @throws ServiceException
	 */
	public News read(Long newsId) throws ServiceException;
	
	/**Update news by changing some News's fields.
	 * @param news
	 * @throws ServiceException
	 */
	public void update(NewsVOForm newsForm) throws ServiceException, OptimisticLockServiceException;
	
	/**Delete news by it's id.
	 * @param newsId
	 * @throws ServiceException
	 */
	public void delete(List<Long> listNewsIds) throws ServiceException;
	
	/**Reading news by search criteria.
	 * @param searchCriteria
	 * @return list of news
	 * @throws ServiceException
	 */
	public List<News> getNews(SearchCriteria searchCriteria, int startIndex, int endIndex) throws ServiceException;
	
	/**Count the number of news found by search criteria.
	 * @param searchCriteria
	 * @return number of news
	 * @throws ServiceException
	 */
	public Long countSearchedNews(SearchCriteria searchCriteria) throws ServiceException;
	
	public NewsVOForm getNewsVOForm(Long newsId) throws ServiceException;
	
	
}
