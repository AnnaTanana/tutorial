package by.tanana.newsManagement.dao.eclipselinkImpl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.criterion.Disjunction;
import org.springframework.transaction.annotation.Transactional;

import exception.DAOException;
import exception.OptimisticLockDAOException;
import by.tanana.newsManagement.dao.NewsDAO;
import by.tanana.newsManagement.entity.Author;
import by.tanana.newsManagement.entity.CommentNumber;
import by.tanana.newsManagement.entity.News;
import by.tanana.newsManagement.entity.SearchCriteria;
import by.tanana.newsManagement.entity.Tag;

@Transactional(rollbackFor=DAOException.class)
public class NewsDAOEclipselinkImpl implements NewsDAO {

    @PersistenceContext 
    private EntityManager entityManager;

    @Override
    public Long create(News news) throws DAOException {
	try {
	    entityManager.persist(news);
	} catch (PersistenceException e) {
	    throw new DAOException("Exception occurred while calling "
		    + "NewsDAOEclipselinkImpl.create(News) method with parameter " 
		    + news, e);
	}
	return news.getNewsId();
    }

    @Override
    public News read(Long newsId) throws DAOException {
	try {
	    return (News)entityManager.find(News.class, newsId);
	} catch (PersistenceException e) {
	    throw new DAOException("Exception occurred while calling "
		    + "NewsDAOEclipselinkImpl.read(Long) method with parameter " 
		    + newsId, e);
	}
    }

    @Override
    public void update(News news) throws DAOException, OptimisticLockDAOException {
	try {
	    entityManager.merge(news); 
	} catch (OptimisticLockException e) {
	    throw new OptimisticLockDAOException(e);
	}
	catch (PersistenceException e) {
	    throw new DAOException("Exception occurred while calling "
		    + "NewsDAOEclipselinkImpl.update(News) method with parameter " 
		    + news, e);
	}	

    }

    @Override
    public void delete(List<Long> listNewsIds) throws DAOException {
	try {
	    for (Long newsId: listNewsIds) {
		News news = (News)entityManager.find(News.class, newsId);
		entityManager.remove(news);
	    }
	} catch (PersistenceException e) {
	    throw new DAOException("Exception occurred while calling "
		    + "NewsDAOEclipselinkImpl.delete(List<Long>) method with parameter " 
		    + listNewsIds, e);
	}

    }

    private CriteriaQuery<News> createDisjunctionForSearchCriteria(Root<News> newsRoot, CriteriaBuilder cb, CriteriaQuery<News> cq, SearchCriteria searchCriteria) {
	Predicate predicate = cb.disjunction();
	if (searchCriteria.getAuthorId() != null) {
	    Join<News, Author> author = newsRoot.join("author", JoinType.LEFT);
	    predicate = cb.equal(author.get("authorId"), searchCriteria.getAuthorId());
	    cq.where(predicate);
	}
	if (searchCriteria.getTagIdList() != null && !searchCriteria.getTagIdList().isEmpty()) {
	    Join<News, Tag> tag = newsRoot.join("tagList", JoinType.LEFT);
	    predicate = cb.and(predicate, tag.get("tagId").in(searchCriteria.getTagIdList()));
	    cq.where(predicate);
	}
	return cq;
    }

    @Override
    public List<News> getNews(SearchCriteria searchCriteria, int startIndex, int endIndex) throws DAOException {
	try {
	    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
	    CriteriaQuery<News> cq = cb.createQuery(News.class);
	    Root<News> newsRoot = cq.from(News.class);
	    cq = createDisjunctionForSearchCriteria(newsRoot, cb, cq, searchCriteria);
	    cq.orderBy(cb.desc(newsRoot.get("modificationDate")));
	    cq.orderBy(cb.desc(newsRoot.get("commentNumber").get("numberOfComments")));
	    cq.distinct(true);
	    Query query = entityManager.createQuery(cq);
	    query.setFirstResult(startIndex);
	    query.setMaxResults(endIndex - startIndex);
	    return query.getResultList();
	} catch (PersistenceException e) {
	    throw new DAOException("Exception occurred while calling "
		    + "NewsDAOEclipselinkImpl.getNews(SearchCriteria, int, int) method with parameters " 
		    + searchCriteria +" and " + startIndex + " and " + endIndex, e);
	}
    }

    @Override
    public Long countSearchedNews(SearchCriteria searchCriteria) throws DAOException {
	try {
	    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
	    CriteriaQuery<News> cq = cb.createQuery(News.class);
	    Root<News> newsRoot = cq.from(News.class);
	    cq = createDisjunctionForSearchCriteria(newsRoot, cb, cq, searchCriteria);
	    cq.distinct(true);
	    return Long.valueOf(entityManager.createQuery(cq).getResultList().size());
	} catch (PersistenceException e) {
	    throw new DAOException("Exception occurred while calling "
		    + "NewsDAOEclipselinkImpl.countSearchedNews(SearchCriteria) method with parameter " 
		    + searchCriteria, e);
	}
    }

}
