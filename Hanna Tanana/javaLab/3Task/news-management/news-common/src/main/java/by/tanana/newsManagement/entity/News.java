package by.tanana.newsManagement.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.Size;

import org.eclipse.persistence.annotations.BatchFetch;
import org.eclipse.persistence.annotations.BatchFetchType;
import org.hibernate.annotations.BatchSize;

@Entity
@Table(name = "NEWS")
public class News implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4752280792410509500L;
	
	@Version 
	@Column(name="opt_lock")
	private Long version;
	
	@SequenceGenerator(sequenceName="news_seq", name = "news_seq", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "news_seq")
	@Id
	@Column(name = "news_id", unique = true, nullable = false)
	private Long newsId;
	
	@Column(name = "title", nullable = false, length = 30)
	@Size(min=1, max=30)
	private String title;
	
	@Column(name = "short_text", nullable = false, length = 100)
	@Size(min=1, max=100)
	private String shortText;
	
	@Column(name = "full_text", nullable = false, length = 2000)
	@Size(min=1, max=2000)
	private String fullText;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "creation_date", nullable = false)
	private Date creationDate;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "modification_date", nullable = false)
	private Date modificationDate;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinTable(name="NEWS_AUTHOR", joinColumns = @JoinColumn( name="news_id"), inverseJoinColumns = @JoinColumn( name="author_id"))
	@BatchFetch( value = BatchFetchType.JOIN)
	private Author author;
	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name="NEWS_TAG", joinColumns = @JoinColumn( name="news_id"), inverseJoinColumns = @JoinColumn( name="tag_id"))
	@BatchFetch( value = BatchFetchType.IN)
	@BatchSize(size=100)
	private List<Tag> tagList;
	
	@OneToMany(cascade = CascadeType.REMOVE, fetch = FetchType.LAZY)
	@JoinColumn(name="news_id")
	@BatchFetch( value = BatchFetchType.IN)
	@BatchSize(size=100)
	private List<Comment> commentList;
	
	@OneToOne
	@JoinColumn(name="news_id", referencedColumnName="news_id", insertable = false, updatable = false)
	@BatchFetch( value = BatchFetchType.JOIN)
	private CommentNumber commentNumber;
	
	
	public List<Comment> getCommentList() {
	    return commentList;
	}


	public void setCommentList(List<Comment> commentList) {
	    this.commentList = commentList;
	}

	public Author getAuthor() {
	    return author;
	}


	public void setAuthor(Author author) {
	    this.author = author;
	}


	public List<Tag> getTagList() {
	    return tagList;
	}


	public void setTagList(List<Tag> tagList) {
	    this.tagList = tagList;
	}

	
	public News() {}
	
	
	public Long getNewsId() {
		return newsId;
	}
	
	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getShortText() {
		return shortText;
	}
	
	public void setShortText(String shortText) {
		this.shortText = shortText;
	}
	
	public String getFullText() {
		return fullText;
	}
	
	public void setFullText(String fullText) {
		this.fullText = fullText;
	}
	
	public Date getCreationDate() {
		return creationDate;
	}
	
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	
	public Date getModificationDate() {
		return modificationDate;
	}
	
	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	

	public CommentNumber getCommentNumber() {
	    return commentNumber;
	}


	public void setCommentNumber(CommentNumber commentNumber) {
	    this.commentNumber = commentNumber;
	}

	public Long getVersion() {
	    return version;
	}


	public void setVersion(Long version) {
	    this.version = version;
	}
	


	@Override
	public int hashCode() {
	    final int prime = 31;
	    int result = 1;
	    result = prime * result + ((author == null) ? 0 : author.hashCode());
	    result = prime * result + ((commentList == null) ? 0 : commentList.hashCode());
	    result = prime * result + ((commentNumber == null) ? 0 : commentNumber.hashCode());
	    result = prime * result + ((creationDate == null) ? 0 : creationDate.hashCode());
	    result = prime * result + ((fullText == null) ? 0 : fullText.hashCode());
	    result = prime * result + ((modificationDate == null) ? 0 : modificationDate.hashCode());
	    result = prime * result + ((newsId == null) ? 0 : newsId.hashCode());
	    result = prime * result + ((shortText == null) ? 0 : shortText.hashCode());
	    result = prime * result + ((tagList == null) ? 0 : tagList.hashCode());
	    result = prime * result + ((title == null) ? 0 : title.hashCode());
	    result = prime * result + ((version == null) ? 0 : version.hashCode());
	    return result;
	}


	@Override
	public boolean equals(Object obj) {
	    if (this == obj)
		return true;
	    if (obj == null)
		return false;
	    if (getClass() != obj.getClass())
		return false;
	    News other = (News) obj;
	    if (author == null) {
		if (other.author != null)
		    return false;
	    } else if (!author.equals(other.author))
		return false;
	    if (commentList == null) {
		if (other.commentList != null)
		    return false;
	    } else if (!commentList.equals(other.commentList))
		return false;
	    if (commentNumber == null) {
		if (other.commentNumber != null)
		    return false;
	    } else if (!commentNumber.equals(other.commentNumber))
		return false;
	    if (creationDate == null) {
		if (other.creationDate != null)
		    return false;
	    } else if (!creationDate.equals(other.creationDate))
		return false;
	    if (fullText == null) {
		if (other.fullText != null)
		    return false;
	    } else if (!fullText.equals(other.fullText))
		return false;
	    if (modificationDate == null) {
		if (other.modificationDate != null)
		    return false;
	    } else if (!modificationDate.equals(other.modificationDate))
		return false;
	    if (newsId == null) {
		if (other.newsId != null)
		    return false;
	    } else if (!newsId.equals(other.newsId))
		return false;
	    if (shortText == null) {
		if (other.shortText != null)
		    return false;
	    } else if (!shortText.equals(other.shortText))
		return false;
	    if (tagList == null) {
		if (other.tagList != null)
		    return false;
	    } else if (!tagList.equals(other.tagList))
		return false;
	    if (title == null) {
		if (other.title != null)
		    return false;
	    } else if (!title.equals(other.title))
		return false;
	    if (version == null) {
		if (other.version != null)
		    return false;
	    } else if (!version.equals(other.version))
		return false;
	    return true;
	}


	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("News: newsId=").append(newsId.toString()).
		append(", title=").append(title).
		append(", short text=").append(shortText).
		append(", full text=").append(fullText).
		append(", creation date=").append(creationDate.toString()).
		append(", modification date=").append(modificationDate.toString()).
		append(", author=").append(author.toString()).
		append(", tagList=").append(tagList.toString()).
		append(", commentList=").append(commentList.toString());
		return stringBuilder.toString();
	}
	
	

}
