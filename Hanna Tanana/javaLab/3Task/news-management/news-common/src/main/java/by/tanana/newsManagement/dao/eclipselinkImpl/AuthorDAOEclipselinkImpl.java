package by.tanana.newsManagement.dao.eclipselinkImpl;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import org.springframework.transaction.annotation.Transactional;

import exception.DAOException;
import by.tanana.newsManagement.dao.AuthorDAO;
import by.tanana.newsManagement.entity.Author;

@Transactional(rollbackFor=DAOException.class)
public class AuthorDAOEclipselinkImpl implements AuthorDAO {

    @PersistenceContext 
    private EntityManager entityManager;


    @Override
    public Long create(Author author) throws DAOException {
	try {
	    entityManager.persist(author);
	} catch (PersistenceException e) {
	    throw new DAOException("Exception occurred while calling "
		    + "AuthorDAOEclipselinkImpl.create(Author) method with parameter " 
		    + author, e);
	}
	return author.getAuthorId();
    }

    @Override
    public Author read(Long authorId) throws DAOException {
	try {
	    return (Author)entityManager.find(Author.class, authorId);
	} catch (PersistenceException e) {
	    throw new DAOException("Exception occurred while calling "
		    + "AuthorDAOEclipselinkImpl.read(Long) method with parameter " 
		    + authorId, e);
	}
    }

    @Override
    public void update(Author author) throws DAOException {
	try {
	    entityManager.merge(author); 
	} catch (PersistenceException e) {
	    throw new DAOException("Exception occurred while calling "
		    + "AuthorDAOEclipselinkImpl.update(Author) method with parameter " 
		    + author, e);
	}
    }

    @Override
    public void delete(Long authorId, Timestamp expiredTime) throws DAOException {
	try {
	    Author author = (Author)entityManager.find(Author.class, authorId);
	    Long milliseconds = expiredTime.getTime();
	    Date date = new Date(milliseconds);
	    author.setExpired(date);
	    entityManager.merge(author); 
	} catch (PersistenceException e) {
	    throw new DAOException("Exception occurred while calling "
		    + "AuthorDAOEclipselinkImpl.delete(Long, Timestamp) method with parameters " 
		    + authorId + " and " + expiredTime, e);
	}


    }

    @Override
    public List<Author> getAuthors() throws DAOException {
	try {
	    Query query = entityManager.createQuery("select a from Author a", Author.class);
	    List<Author> authors = query.getResultList();
	    return authors;
	} catch (PersistenceException e) {
	    throw new DAOException("Exception occurred while calling "
		    + "AuthorDAOEclipselinkImpl.getAuthors() method");
	}

    }

}
