package by.tanana.newsManagement.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

@Entity
@Table(name = "AUTHOR")
public class Author implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3490045102558484888L;
	
	@SequenceGenerator(sequenceName = "author_seq", name = "author_seq", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "author_seq")
	@Id
	@Column(name = "author_id", unique = true, nullable = false)
	private Long authorId;
	
	@Column(name = "author_name", nullable = false, length = 50)
	@Size(min=1, max=50)
	private String name;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "expired")
	private Date expired;
	
	public Author() {}
	
	public Long getAuthorId() {
		return authorId;
	}
	
	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Date getExpired() {
		return expired;
	}
	
	public void setExpired(Date expired) {
		this.expired = expired;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((expired == null) ? 0 : expired.hashCode());
		result = prime * result + ((authorId == null) ? 0 : authorId.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		Author other = (Author)obj;
		if (this.authorId == null) {
			if (other.authorId != null) {
				return false;
			}
		} else if (!this.authorId.equals(other.authorId)) {
			return false;
		}
		if (this.name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!this.name.equals(other.name)) {
			return false;
		}
		if (this.expired == null) {
			if (other.expired != null) {
				return false;
			}
		} else if (!this.expired.equals(other.expired)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("Author: authorId=").append(authorId.toString()).
		append(", name=").append(name);
		if (expired != null) {
			stringBuilder.append(", expired=").append(expired.toString());
		}
		return stringBuilder.toString();
	}
	
	
}
