package by.tanana.newsManagement.dao;


import exception.DAOException;
import by.tanana.newsManagement.entity.Comment;

public interface CommentDAO {
	
    /**Insert a new comment entity to database
	 * by auto generating it's id.
	 * @param comment
	 * @return generated comment id
	 * @throws DAOException
	 */
	public Long create(Comment comment) throws DAOException;
	
	/**Read a comment from database
	 * by comment's id.
	 * @param commentId
	 * @return comment
	 * @throws DAOException
	 */
	public Comment read(Long commentId) throws DAOException;
	
	/**Update a comment by changing some CommentTO's fields.
	 * @param comment
	 * @throws DAOException
	 */
	public void update(Comment comment) throws DAOException;
	
	/**Deleting comment by it's id from database.
	 * @param commentId
	 * @throws DAOException
	 */
	public void delete(Long commentId) throws DAOException;
		

}
