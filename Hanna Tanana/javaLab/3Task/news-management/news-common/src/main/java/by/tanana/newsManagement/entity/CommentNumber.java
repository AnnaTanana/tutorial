package by.tanana.newsManagement.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

@Entity
@Table(name = "COMMENT_NUMBER_VIEW")
@Immutable
public class CommentNumber {

    @Id
    @Column(name = "news_id", nullable = false)
    private Long newsId;
    
    @Column(name = "comment_number", nullable = true)
    private int numberOfComments;
    

    public CommentNumber() {
	super();
    }

    public Long getNewsId() {
        return newsId;
    }

    public void setNewsId(Long newsId) {
        this.newsId = newsId;
    }

    public int getNumberOfComments() {
        return numberOfComments;
    }

    public void setNumberOfComments(int numberOfComments) {
        this.numberOfComments = numberOfComments;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((newsId == null) ? 0 : newsId.hashCode());
	result = prime * result + numberOfComments;
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	CommentNumber other = (CommentNumber) obj;
	if (newsId == null) {
	    if (other.newsId != null)
		return false;
	} else if (!newsId.equals(other.newsId))
	    return false;
	if (numberOfComments != other.numberOfComments)
	    return false;
	return true;
    }
    
    
    
}
