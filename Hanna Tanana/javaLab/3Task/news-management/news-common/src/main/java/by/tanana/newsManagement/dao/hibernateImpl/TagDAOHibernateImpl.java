package by.tanana.newsManagement.dao.hibernateImpl;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import exception.DAOException;
import by.tanana.newsManagement.dao.TagDAO;
import by.tanana.newsManagement.entity.Tag;

public class TagDAOHibernateImpl implements TagDAO {

    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
	this.sessionFactory = sessionFactory;
    }

    @Override
    public Long create(Tag tag) throws DAOException {
	Session session = sessionFactory.openSession();
	Transaction transaction = null;
	try {
	    transaction = session.beginTransaction();
	    Serializable id = session.save(tag);
	    transaction.commit();
	    return (Long) id;
	} catch (HibernateException e){
	    if (transaction != null){
		transaction.rollback();
	    }
	    throw new DAOException("Exception occurred while calling "
		    + "TagDAOHibernateImpl.create(Tag) method with parameter " 
		    + tag, e);
	}
	finally {
	    session.close();
	}
    }

    @Override
    public Tag read(Long tagId) throws DAOException {
	Session session = sessionFactory.openSession();
	try {
	    Tag tag = (Tag) session.get(Tag.class, tagId);
	    return tag;
	} catch (HibernateException e){
	    throw new DAOException("Exception occurred while calling "
		    + "TagDAOHibernateImpl.read(Long) method with parameter " 
		    + tagId, e);
	}
	finally {
	    session.close();
	}
    }

    @Override
    public void update(Tag tag) throws DAOException {
	Session session = sessionFactory.openSession();
	Transaction transaction = null;
	try {
	    transaction = session.beginTransaction();
	    session.update(tag);
	    transaction.commit();
	} catch (HibernateException e){
	    if (transaction != null){
		transaction.rollback();
	    }
	    throw new DAOException("Exception occurred while calling "
		    + "TagDAOHibernateImpl.update(Tag) method with parameter " 
		    + tag, e);
	}
	finally {
	    session.close();
	}
    }

    @Override
    public void delete(Long tagId) throws DAOException {
	Session session = sessionFactory.openSession();
	Transaction transaction = null;
	try {
	    transaction = session.beginTransaction();
	    Tag tag = (Tag) session.load(Tag.class, tagId);
	    session.delete(tag);
	    transaction.commit();
	} catch (HibernateException e){
	    if (transaction != null){
		transaction.rollback();
	    }
	    throw new DAOException("Exception occurred while calling "
		    + "TagDAOHibernateImpl.delete(Long) method with parameter " 
		    + tagId, e);
	}
	finally {
	    session.close();
	}

    }

    @Override
    public List<Tag> getTags() throws DAOException {
	Session session = sessionFactory.openSession();
	try {
	    Criteria criteria = session.createCriteria(Tag.class);
	    List<Tag> tags = criteria.list();
	    return tags;
	} catch (HibernateException e){
	    throw new DAOException("Exception occurred while calling "
		    + "TagDAOHibernateImpl.getTags() method", e);
	}
	finally {
	    session.close();
	}

    }

}
