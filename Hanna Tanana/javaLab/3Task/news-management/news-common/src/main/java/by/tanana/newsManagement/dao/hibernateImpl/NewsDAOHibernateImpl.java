package by.tanana.newsManagement.dao.hibernateImpl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.StaleObjectStateException;
import org.hibernate.Transaction;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.hibernate.sql.JoinType;

import exception.DAOException;
import exception.OptimisticLockDAOException;
import by.tanana.newsManagement.dao.NewsDAO;
import by.tanana.newsManagement.entity.News;
import by.tanana.newsManagement.entity.SearchCriteria;

public class NewsDAOHibernateImpl implements NewsDAO {

    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
	this.sessionFactory = sessionFactory;
    }

    @Override
    public Long create(News news) throws DAOException {
	Session session = sessionFactory.openSession();
	Transaction transaction = null;
	try {
	    transaction = session.beginTransaction();
	    Serializable id = session.save(news);
	    transaction.commit();
	    return (Long) id;
	} catch (HibernateException e){
	    if (transaction != null){
		transaction.rollback();
	    }
	    throw new DAOException("Exception occurred while calling "
		    + "NewsDAOHibernateImpl.create(News) method with parameter " 
		    + news, e);
	}
	finally {
	    session.close();
	}
    }

    @Override
    public News read(Long newsId) throws DAOException {
	Session session = sessionFactory.openSession();
	try {
	    News news = (News) session.get(News.class, newsId);
	    news.getAuthor().getAuthorId();
	    news.getTagList().size();
	    news.getCommentList().size();
	    news.getCommentNumber().getNumberOfComments();
	    return news;
	} catch (HibernateException e){
	    throw new DAOException("Exception occurred while calling "
		    + "NewsDAOHibernateImpl.read(Long) method with parameter " 
		    + newsId, e);
	}
	finally {
	    session.close();
	}
    }

    @Override
    public void update(News news) throws DAOException, OptimisticLockDAOException {
	Session session = sessionFactory.openSession();
	Transaction transaction = null;
	try {
	    transaction = session.beginTransaction();
	    session.merge(news);
	    transaction.commit();
	} catch (StaleObjectStateException e) {
	    throw new OptimisticLockDAOException(e);
	} 
	catch (HibernateException e){
	    if (transaction != null){
		transaction.rollback();
	    }
	    throw new DAOException("Exception occurred while calling "
		    + "NewsDAOHibernateImpl.update(news) method with parameter " 
		    + news, e);
	} 
	finally {
	    session.close();
	}

    }

    @Override
    public void delete(List<Long> listNewsIds) throws DAOException {
	Session session = sessionFactory.openSession();
	Transaction transaction = null;
	try {
	    transaction = session.beginTransaction();
	    for (Long newsId : listNewsIds) {
		News news = (News) session.load(News.class, newsId);
		session.delete(news);
	    }  
	    transaction.commit();
	} catch (HibernateException e){
	    if (transaction != null){
		transaction.rollback();
	    }
	    throw new DAOException("Exception occurred while calling "
		    + "NewsDAOHibernateImpl.delete(List<Long>) method with parameter " 
		    + listNewsIds, e);
	}
	finally {
	    session.close();
	}

    }

    private Disjunction createDisjunctionForSearchCriteria(SearchCriteria searchCriteria) {
	Disjunction disjunction = Restrictions.disjunction();
	if (searchCriteria.getAuthorId() != null) {
	    disjunction.add(Restrictions.eq("a.authorId", searchCriteria.getAuthorId()));
	}
	if (searchCriteria.getTagIdList() != null && !searchCriteria.getTagIdList().isEmpty()) {
	    disjunction.add(Restrictions.in("t.tagId", searchCriteria.getTagIdList()));
	}
	return disjunction;
    }
    @Override
    public List<News> getNews(SearchCriteria searchCriteria, int startIndex, int endIndex) throws DAOException {
	Session session = sessionFactory.openSession();
	Transaction transaction = null;
	List<News> newsList = null;
	try {
	    transaction = session.beginTransaction();
	    DetachedCriteria detachedCriteria = DetachedCriteria.forClass(News.class)
		    .createAlias("author", "a", JoinType.LEFT_OUTER_JOIN)
		    .createAlias("tagList", "t", JoinType.LEFT_OUTER_JOIN)
		    .setResultTransformer(Criteria.PROJECTION)
		    .setProjection(Projections.distinct(Projections.property("newsId")))
		    .setProjection(Projections.property("newsId"))
		    .add(createDisjunctionForSearchCriteria(searchCriteria));
		    
	    Criteria criteria = session.createCriteria(News.class)
		    .createAlias("commentNumber", "commentNum", JoinType.LEFT_OUTER_JOIN)
		    .add(Subqueries.propertyIn("newsId", detachedCriteria))
		    .addOrder(Order.desc("commentNum.numberOfComments"))
		    .addOrder(Order.desc("modificationDate"));

	    criteria.setMaxResults((endIndex-startIndex));
	    criteria.setFirstResult(startIndex);
	    if (criteria.list() != null && !criteria.list().isEmpty()) {
		newsList = criteria.list();
		for (News news: newsList) {
		    news.getAuthor().getAuthorId();
		    news.getCommentList().size();
		    news.getTagList().size();
		    news.getCommentNumber().getNumberOfComments();
		}
	    }
	    return newsList;
	} catch (HibernateException e){
	    if (transaction != null){
		transaction.rollback();
	    }
	    throw new DAOException("Exception occurred while calling "
		    + "getNews(SearchCriteria, int, int) method with parameter " 
		    + searchCriteria + ", "+startIndex + ", " + endIndex, e);
	}
	finally {
	    session.close();
	}
    }

    @Override
    public Long countSearchedNews(SearchCriteria searchCriteria) throws DAOException {
	Session session = sessionFactory.openSession();
	Transaction transaction = null;
	Long numberOfNews = 0L;
	try {
	    transaction = session.beginTransaction();
	    Criteria criteria = session.createCriteria(News.class)
		    .createAlias("author", "a", JoinType.LEFT_OUTER_JOIN)
		    .createAlias("tagList", "t", JoinType.LEFT_OUTER_JOIN)
		    .add(createDisjunctionForSearchCriteria(searchCriteria))
		    .setResultTransformer(Criteria.PROJECTION)
		    .setProjection(Projections.countDistinct("newsId"));
	    numberOfNews =  (Long) criteria.uniqueResult();
	    return numberOfNews;
	} catch (HibernateException e){
	    if (transaction != null){
		transaction.rollback();
	    }
	    throw new DAOException("Exception occurred while calling "
		    + "countSearchedNews(SearchCriteria) method with parameter " 
		    + searchCriteria, e);
	}
	finally {
	    session.close();
	}
    }

}
