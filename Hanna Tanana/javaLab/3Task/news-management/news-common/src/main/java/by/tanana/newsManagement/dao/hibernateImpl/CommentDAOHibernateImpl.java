package by.tanana.newsManagement.dao.hibernateImpl;

import java.io.Serializable;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import exception.DAOException;
import by.tanana.newsManagement.dao.CommentDAO;
import by.tanana.newsManagement.entity.Comment;
import by.tanana.newsManagement.entity.News;

public class CommentDAOHibernateImpl implements CommentDAO {

    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
	this.sessionFactory = sessionFactory;
    }

    @Override
    public Long create(Comment comment) throws DAOException {
	Session session = sessionFactory.openSession();
	Transaction transaction = null;
	try {
	    transaction = session.beginTransaction();
	    Serializable id = session.save(comment); 
	    transaction.commit();
	    return (Long) id;
	} catch (HibernateException e){
	    if (transaction != null){
		transaction.rollback();
	    }
	    throw new DAOException("Exception occurred while calling "
		    + "CommentDAOHibernateImpl.create(Comment) method with parameter " 
		    + comment, e);
	}
	finally {
	    session.close();
	}
    }
    
    @Override
    public Comment read(Long commentId) throws DAOException {
	Session session = sessionFactory.openSession();
	try {
	    Comment comment = (Comment) session.get(Comment.class, commentId);
	    return comment;
	} catch (HibernateException e){
	    throw new DAOException("Exception occurred while calling "
		    + "CommentDAOHibernateImpl.read(Long) method with parameter " 
		    + commentId, e);
	}
	finally {
	    session.close();
	}
    }

    @Override
    public void update(Comment comment) throws DAOException {
	Session session = sessionFactory.openSession();
	Transaction transaction = null;
	try {
	    transaction = session.beginTransaction();
	    session.update(comment);
	    transaction.commit();
	} catch (HibernateException e){
	    if (transaction != null){
		transaction.rollback();
	    }
	    throw new DAOException("Exception occurred while calling "
		    + "CommentDAOHibernateImpl.update(Comment) method with parameter " 
		    + comment, e);
	}
	finally {
	    session.close();
	}
    }

    @Override
    public void delete(Long commentId) throws DAOException {
	Session session = sessionFactory.openSession();
	Transaction transaction = null;
	try {
	    transaction = session.beginTransaction();
	    Comment comment = (Comment) session.load(Comment.class, commentId);
	    session.delete(comment);
	    transaction.commit();
	} catch (HibernateException e){
	    if (transaction != null){
		transaction.rollback();
	    }
	    throw new DAOException("Exception occurred while calling "
		    + "CommentDAOHibernateImpl.delete(Long) method with parameter " 
		    + commentId, e);
	}
	finally {
	    session.close();
	}
    }
    
}
