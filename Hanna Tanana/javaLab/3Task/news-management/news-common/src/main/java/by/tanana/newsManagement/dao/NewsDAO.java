package by.tanana.newsManagement.dao;

import java.util.List;

import exception.DAOException;
import exception.OptimisticLockDAOException;
import by.tanana.newsManagement.entity.News;
import by.tanana.newsManagement.entity.SearchCriteria;

public interface NewsDAO {
	
	/**Insert new news entity to database
	 * by auto generating it's id.
	 * @param news
	 * @return generated news id
	 * @throws DAOException
	 */
	public Long create(News news) throws DAOException;
	
	/**Read news from database
	 * by news's id.
	 * @param newsId
	 * @return news
	 * @throws DAOException
	 */
	public News read(Long newsId) throws DAOException;
	
	/**Update news by changing some News's fields.
	 * @param news
	 * @throws DAOException
	 */
	public void update(News news) throws DAOException, OptimisticLockDAOException;
	
	/**Deleting news from database by it's id.
	 * @param newsId
	 * @throws DAOException
	 */
	public void delete(List<Long> listNewsIds) throws DAOException;
			
	
	/**Reading news from database by search criteria.
	 * @param searchCriteria
	 * @return list of news
	 * @throws DAOException
	 */
	public List<News> getNews(SearchCriteria searchCriteria, int startIndex, int endIndex) throws DAOException;
	
	/**Count the number of news found by search criteria.
	 * @param searchCriteria
	 * @return number of news
	 * @throws DAOException
	 */
	public Long countSearchedNews(SearchCriteria searchCriteria) throws DAOException;
}
