package by.tanana.newsManagement.dao.hibernateImpl;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import exception.DAOException;
import by.tanana.newsManagement.dao.AuthorDAO;
import by.tanana.newsManagement.entity.Author;


public class AuthorDAOHibernateImpl implements AuthorDAO {

    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
	this.sessionFactory = sessionFactory;
    }

    @Override
    public Long create(Author author) throws DAOException {
	Session session = sessionFactory.openSession();
	Transaction transaction = null;
	try {
	    transaction = session.beginTransaction();
	    Serializable id = session.save(author);
	    transaction.commit();
	    return (Long) id;
	} catch (HibernateException e){
	    if (transaction != null){
		transaction.rollback();
	    }
	    throw new DAOException("Exception occurred while calling "
		    + "AuthorDAOHibernateImpl.create(Author) method with parameter " 
		    + author, e);
	}
	finally {
	    session.close();
	}
    }

    @Override
    public Author read(Long authorId) throws DAOException {
	Session session = sessionFactory.openSession();
	try {
	    Author author = (Author) session.get(Author.class, authorId);
	    return author;
	} catch (HibernateException e){
	    throw new DAOException("Exception occurred while calling "
		    + "AuthorDAOHibernateImpl.read(Long) method with parameter " 
		    + authorId, e);
	}
	finally {
	    session.close();
	}

    }

    @Override
    public void update(Author author) throws DAOException {
	Session session = sessionFactory.openSession();
	Transaction transaction = null;
	try {
	    transaction = session.beginTransaction();
	    session.update(author);
	    transaction.commit();
	} catch (HibernateException e){
	    if (transaction != null){
		transaction.rollback();
	    }
	    throw new DAOException("Exception occurred while calling "
		    + "AuthorDAOHibernateImpl.update(Author) method with parameter " 
		    + author, e);
	}
	finally {
	    session.close();
	}
    }
    
    @Override
    public void delete(Long authorId, Timestamp expiredTime) throws DAOException {
	Session session = sessionFactory.openSession();
	Transaction transaction = null;
	try {
	    transaction = session.beginTransaction();
	    Author author = (Author) session.load(Author.class, authorId);
	    Long milliseconds = expiredTime.getTime();
	    Date date = new Date(milliseconds);
	    author.setExpired(date);
	    session.update(author);
	    transaction.commit();
	} catch (HibernateException e){
	    if (transaction != null){
		transaction.rollback();
	    }
	    throw new DAOException("Exception occurred while calling "
		    + "AuthorDAOHibernateImpl.delete(Long, Timestamp) method with parameters " 
		    + authorId + ", " + expiredTime, e);
	}
	finally {
	    session.close();
	}
    }

    @Override
    public List<Author> getAuthors() throws DAOException {
	Session session = sessionFactory.openSession();
	try {
	    Criteria criteria = session.createCriteria(Author.class);
	    List<Author> authors = criteria.list();
	    return authors;
	} catch (HibernateException e){
	    throw new DAOException("Exception occurred while calling "
		    + "AuthorDAOHibernateImpl.getAuthors() method", e);
	}
	finally {
	    session.close();
	}

    }


}
