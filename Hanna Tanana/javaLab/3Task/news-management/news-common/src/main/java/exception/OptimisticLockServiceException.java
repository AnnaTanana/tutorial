package exception;

public class OptimisticLockServiceException extends Exception {
	 public OptimisticLockServiceException() {
	    }

	 public OptimisticLockServiceException(String message) {
	        super(message);
	    }

	 public OptimisticLockServiceException(String message, Throwable cause) {
	        super(message, cause);
	    }

	 public OptimisticLockServiceException(Throwable cause) {
	        super(cause);
	    }

	 public OptimisticLockServiceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
	        super(message, cause, enableSuppression, writableStackTrace);
	    }
}
