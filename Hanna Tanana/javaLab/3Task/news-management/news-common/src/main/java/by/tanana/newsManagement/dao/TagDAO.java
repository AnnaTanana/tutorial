package by.tanana.newsManagement.dao;

import java.util.List;

import exception.DAOException;
import by.tanana.newsManagement.entity.Tag;

public interface TagDAO {
	
	/**Insert a new tag entity to database
	 * by auto generating it's id.
	 * @param tag
	 * @return generated tag id
	 * @throws DAOException
	 */
	public Long create(Tag tag) throws DAOException;
	
	/**Read a tag from database
	 * by tag's id.
	 * @param tagId
	 * @return tag
	 * @throws DAOException
	 */
	public Tag read(Long tagId) throws DAOException;
	
	/**Update a tag by changing some Tag's fields.
	 * @param tag
	 * @throws DAOException
	 */
	public void update(Tag tag) throws DAOException;
	
	/**Deleting tag by it's id from database.
	 * @param tagId
	 * @throws DAOException
	 */
	public void delete(Long tagId) throws DAOException;
	
	/**Get all tags from database.
	 * @return list of tags
	 * @throws DAOException
	 */
	public List<Tag> getTags() throws DAOException;
}
