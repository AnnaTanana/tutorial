package by.tanana.newsManagement.dao.eclipselinkImpl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

import org.springframework.transaction.annotation.Transactional;

import exception.DAOException;
import by.tanana.newsManagement.dao.CommentDAO;
import by.tanana.newsManagement.entity.Comment;
import by.tanana.newsManagement.entity.News;

@Transactional(rollbackFor=DAOException.class)
public class CommentDAOEclipselinkImpl implements CommentDAO {

    @PersistenceContext 
    private EntityManager entityManager;

    @Override
    public Long create(Comment comment) throws DAOException {
	try {
	    News news = entityManager.find(News.class, comment.getNewsId());
	    news.getCommentList().add(comment);
	    entityManager.persist(news);
	    entityManager.persist(comment);
	} catch (PersistenceException e) {
	    throw new DAOException("Exception occurred while calling "
		    + "CommentDAOEclipselinkImpl.create(Comment) method with parameter " 
		    + comment, e);
	}
	return comment.getCommentId();
    }

    @Override
    public Comment read(Long commentId) throws DAOException {
	try {
	    return (Comment)entityManager.find(Comment.class, commentId);
	} catch (PersistenceException e) {
	    throw new DAOException("Exception occurred while calling "
		    + "CommentDAOEclipselinkImpl.read(Long) method with parameter " 
		    + commentId, e);
	}
    }

    @Override
    public void update(Comment comment) throws DAOException {
	try {
	    entityManager.merge(comment); 
	} catch (PersistenceException e) {
	    throw new DAOException("Exception occurred while calling "
		    + "CommentDAOEclipselinkImpl.update(Comment) method with parameter " 
		    + comment, e);
	}	
    }

    @Override
    public void delete(Long commentId) throws DAOException {
	try {
	    Comment comment = (Comment)entityManager.find(Comment.class, commentId);
	    entityManager.remove(comment); 
	    entityManager.getEntityManagerFactory().getCache().evictAll();
	} catch (PersistenceException e) {
	    throw new DAOException("Exception occurred while calling "
		    + "CommentDAOEclipselinkImpl.delete(Long) method with parameter " 
		    + commentId, e);
	}
	   
    }
     
}
