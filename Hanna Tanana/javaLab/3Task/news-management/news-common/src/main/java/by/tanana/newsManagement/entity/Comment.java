package by.tanana.newsManagement.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;


@Entity
@Table(name = "COMMENTS")
public class Comment implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7279963540767562215L;
	
	@SequenceGenerator(sequenceName = "comment_seq", name = "comment_seq", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "comment_seq")
	@Id
	@Column(name = "comment_id", unique = true, nullable = false)
	private Long commentId;
	
	@Column(name = "news_id", nullable = false)
	private Long newsId;
	
	@Column(name = "comment_text", nullable = false, length = 100)
	@Size(min=1, max=100)
	private String text;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "creation_date", nullable = false)
	private Date creationDate;
	
	public Comment() {}
	
	public Long getCommentId() {
		return commentId;
	}

	public void setCommentId(Long commentId) {
		this.commentId = commentId;
	}
	

	public Long getNewsId() {
	    return newsId;
	}

	public void setNewsId(Long newsId) {
	    this.newsId = newsId;
	}

	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}


	@Override
	public int hashCode() {
	    final int prime = 31;
	    int result = 1;
	    result = prime * result + ((commentId == null) ? 0 : commentId.hashCode());
	    result = prime * result + ((creationDate == null) ? 0 : creationDate.hashCode());
	    result = prime * result + ((newsId == null) ? 0 : newsId.hashCode());
	    result = prime * result + ((text == null) ? 0 : text.hashCode());
	    return result;
	}

	@Override
	public boolean equals(Object obj) {
	    if (this == obj)
		return true;
	    if (obj == null)
		return false;
	    if (getClass() != obj.getClass())
		return false;
	    Comment other = (Comment) obj;
	    if (commentId == null) {
		if (other.commentId != null)
		    return false;
	    } else if (!commentId.equals(other.commentId))
		return false;
	    if (creationDate == null) {
		if (other.creationDate != null)
		    return false;
	    } else if (!creationDate.equals(other.creationDate))
		return false;
	    if (newsId == null) {
		if (other.newsId != null)
		    return false;
	    } else if (!newsId.equals(other.newsId))
		return false;
	    if (text == null) {
		if (other.text != null)
		    return false;
	    } else if (!text.equals(other.text))
		return false;
	    return true;
	}

	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("Comment: commentId=").append(commentId.toString()).
		append(", newsId=").append(newsId.toString()).
		append(", text=").append(text).
		append(", creation date=").append(creationDate.toString());
		return stringBuilder.toString();
	}
}
