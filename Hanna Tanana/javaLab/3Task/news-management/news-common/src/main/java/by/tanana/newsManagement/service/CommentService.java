package by.tanana.newsManagement.service;


import exception.ServiceException;
import by.tanana.newsManagement.entity.Comment;

public interface CommentService {
	
	/**Create a new commentTO entity.
	 * @param comment
	 * @return comment id
	 * @throws ServiceException
	 */
	public Long create(Comment comment) throws ServiceException;
	
	/**Read a comment by it's id.
	 * @param commentId
	 * @return comment
	 * @throws ServiceException
	 */
	public Comment read(Long commentId) throws ServiceException;
	
	/**Update a comment by changing some CommentTO's fields.
	 * @param comment
	 * @throws ServiceException
	 */
	public void update(Comment comment) throws ServiceException;
		
	/**Delete comment by it's id.
	 * @param commentId
	 * @throws ServiceException
	 */
	public void delete(Long commentId) throws ServiceException;
	
}
