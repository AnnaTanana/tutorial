package exception;

public class OptimisticLockDAOException extends Exception {
	 public OptimisticLockDAOException() {
	    }

	 public OptimisticLockDAOException(String message) {
	        super(message);
	    }

	 public OptimisticLockDAOException(String message, Throwable cause) {
	        super(message, cause);
	    }

	 public OptimisticLockDAOException(Throwable cause) {
	        super(cause);
	    }

	 public OptimisticLockDAOException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
	        super(message, cause, enableSuppression, writableStackTrace);
	    }
}