package by.tanana.newsManagement.dao.eclipselinkImpl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import org.springframework.transaction.annotation.Transactional;

import exception.DAOException;
import by.tanana.newsManagement.dao.TagDAO;
import by.tanana.newsManagement.entity.Tag;

@Transactional(rollbackFor=DAOException.class)
public class TagDAOEclipselinkImpl implements TagDAO {

    @PersistenceContext 
    private EntityManager entityManager;
    
    @Override
    public Long create(Tag tag) throws DAOException {
	try {
	    entityManager.persist(tag);
	} catch (PersistenceException e) {
	    throw new DAOException("Exception occurred while calling "
		    + "TagDAOEclipselinkImpl.create(Tag) method with parameter " 
		    + tag, e);
	}
	return tag.getTagId();
    }

    @Override
    public Tag read(Long tagId) throws DAOException {
	try {
	    return (Tag)entityManager.find(Tag.class, tagId);
	} catch (PersistenceException e) {
	    throw new DAOException("Exception occurred while calling "
		    + "TagDAOEclipselinkImpl.read(Long) method with parameter " 
		    + tagId, e);
	}
    }

    @Override
    public void update(Tag tag) throws DAOException {
	try {
	    entityManager.merge(tag); 
	} catch (PersistenceException e) {
	    throw new DAOException("Exception occurred while calling "
		    + "TagDAOEclipselinkImpl.update(Tag) method with parameter " 
		    + tag, e);
	}	
    }

    @Override
    public void delete(Long tagId) throws DAOException {
	try {
	    Tag tag = (Tag)entityManager.find(Tag.class, tagId);
	    entityManager.remove(tag); 
	} catch (PersistenceException e) {
	    throw new DAOException("Exception occurred while calling "
		    + "TagDAOEclipselinkImpl.delete(Long) method with parameter " 
		    + tagId, e);
	}
	
    }

    @Override
    public List<Tag> getTags() throws DAOException {
	try {
	    Query query = entityManager.createQuery("select t from Tag t", Tag.class);
	    List<Tag> tags = query.getResultList();
	    return tags;
	} catch (PersistenceException e) {
	    throw new DAOException("Exception occurred while calling "
		    + "TagDAOEclipselinkImpl.getTags() method");
	}
    }

}
