package by.tanana.newsManagement.dao.test;

import static org.junit.Assert.assertEquals;

import java.util.List;

import javax.sql.DataSource;

import org.junit.Test;
import org.unitils.UnitilsJUnit4;
import org.unitils.database.annotations.Transactional;
import org.unitils.database.util.TransactionMode;
import org.unitils.dbunit.annotation.DataSet;
import org.unitils.spring.annotation.SpringApplicationContext;
import org.unitils.spring.annotation.SpringBean;

import exception.DAOException;
import by.tanana.newsManagement.dao.TagDAO;
import by.tanana.newsManagement.entity.Tag;

@SpringApplicationContext("test-applicationContext.xml")
@DataSet("TagDAOTest.xml")
@Transactional(TransactionMode.ROLLBACK)
public class TagDAOTest extends UnitilsJUnit4 {
    @SpringBean("dataSource")
    private DataSource dataSource;
    @SpringBean("tagDAO")
    private TagDAO tagDAO;

    @Test
    public void readTest() throws DAOException {
	Tag actualTag = tagDAO.read(new Long(1));
	Tag expectedTag = setExpectedTag(1L, "social");
	assertTagsEquals(expectedTag, actualTag);
    }

    @Test
    public void createTest() throws DAOException {
	Tag expectedTag = setExpectedTag(3L, "crazy");
	Long id = tagDAO.create(expectedTag);
	expectedTag.setTagId(id);
	Tag actualTag = tagDAO.read(id);
	assertTagsEquals(expectedTag, actualTag);
    }

    @Test 
    public void updateTest() throws DAOException {
	Tag expectedTag = setExpectedTag(1L, "society");
	tagDAO.update(expectedTag);
	Tag actualTag = tagDAO.read(new Long(1));
	assertTagsEquals(expectedTag, actualTag);
    }

    @Test
    public void deleteTest() throws DAOException {
	Tag expectedTag = null;
	tagDAO.delete(new Long(2));
	Tag actualTag = tagDAO.read(new Long(2));
	assertEquals(expectedTag, actualTag);
    }

    @Test 
    public void getTags() throws DAOException {
	List<Tag> tagList = tagDAO.getTags();
	Tag actualTag1 = tagList.get(0);
	Tag actualTag2 = tagList.get(1);
	Tag expectedTag1 = setExpectedTag(1L, "social");
	Tag expectedTag2 = setExpectedTag(2L, "media");
	assertEquals(expectedTag1, actualTag1);
	assertEquals(expectedTag2, actualTag2);
    }

    private Tag setExpectedTag(Long tagId, String name) {
	Tag expectedTag = new Tag();
	expectedTag.setTagId(tagId);
	expectedTag.setName(name);
	return expectedTag;
    }

    private void assertTagsEquals(Tag expectedTag, Tag actualTag) {
	assertEquals(expectedTag.getTagId(), actualTag.getTagId());
	assertEquals(expectedTag.getName(), actualTag.getName());
    }


}
