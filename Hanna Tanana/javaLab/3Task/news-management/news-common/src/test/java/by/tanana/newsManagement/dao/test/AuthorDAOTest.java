package by.tanana.newsManagement.dao.test;

import static org.junit.Assert.assertEquals;

import java.sql.Timestamp;
import java.util.List;





import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.Before;
import org.junit.Test;
import org.unitils.UnitilsJUnit4;
import org.unitils.database.annotations.Transactional;
import org.unitils.database.util.TransactionMode;
import org.unitils.dbunit.annotation.DataSet;
import org.unitils.spring.annotation.SpringApplicationContext;
import org.unitils.spring.annotation.SpringBean;

import exception.DAOException;
import by.tanana.newsManagement.dao.AuthorDAO;
import by.tanana.newsManagement.entity.Author;


@SpringApplicationContext("test-applicationContext.xml")
@DataSet("AuthorDAOTest.xml")
@Transactional(TransactionMode.ROLLBACK)
public class AuthorDAOTest extends UnitilsJUnit4 {

    @PersistenceContext
    EntityManager entityManager;
    
    @SpringBean("authorDAO")
    private AuthorDAO authorDAO;

    @Test
    public void readTest() throws DAOException {
	Author actualAuthor = authorDAO.read(new Long(1));
	Author expectedAuthor = setExpectedAuthor(1L, "Mark", null);
	assertAuthorsEquals(expectedAuthor, actualAuthor);
    }

    @Test
    public void createTest() throws DAOException {
	Author expectedAuthor = setExpectedAuthor(null, "Anna", null);
	Long id = authorDAO.create(expectedAuthor);
	expectedAuthor.setAuthorId(id);
	Author actualAuthor = authorDAO.read(id);
	assertAuthorsEquals(expectedAuthor, actualAuthor);	
    }

    @Test
    public void updateTest() throws DAOException {
	Author expectedAuthor = setExpectedAuthor(1L, "Mathew", null);
	authorDAO.update(expectedAuthor);
	Author actualAuthor = authorDAO.read(new Long(1));
	assertAuthorsEquals(expectedAuthor, actualAuthor);	
    }

    @Test
    public void deleteTest() throws DAOException {
	Author actualAuthor = new Author();
	authorDAO.delete(new Long(1), Timestamp.valueOf("2007-09-23 10:10:10.0"));
	actualAuthor = authorDAO.read(new Long(1));
	assertEquals(Timestamp.valueOf("2007-09-23 10:10:10.0"), actualAuthor.getExpired());
    }

    @Test
    public void getAuthors() throws DAOException {
	List<Author> listAuthor = authorDAO.getAuthors();
	Author actualAuthor = listAuthor.get(0);
	Author expectedAuthor = setExpectedAuthor(1L, "Mark", null);
	assertAuthorsEquals(expectedAuthor, actualAuthor);
    }

    private Author setExpectedAuthor(Long authorId, String name, Timestamp expired) {
	Author expectedAuthor = new Author();
	expectedAuthor.setAuthorId(authorId);
	expectedAuthor.setName(name);
	expectedAuthor.setExpired(expired);
	return expectedAuthor;
    }

    private void assertAuthorsEquals(Author expectedAuthor, Author actualAuthor) {
	assertEquals(expectedAuthor.getAuthorId(), actualAuthor.getAuthorId());
	assertEquals(expectedAuthor.getName(), actualAuthor.getName());
	assertEquals(expectedAuthor.getExpired(), actualAuthor.getExpired());
    }

}
