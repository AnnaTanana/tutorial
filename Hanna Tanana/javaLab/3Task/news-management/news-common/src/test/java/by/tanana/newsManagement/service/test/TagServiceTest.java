package by.tanana.newsManagement.service.test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import exception.DAOException;
import exception.ServiceException;
import by.tanana.newsManagement.dao.TagDAO;
import by.tanana.newsManagement.entity.Tag;
import by.tanana.newsManagement.service.TagService;

@ContextConfiguration(locations = {"classpath:/test-applicationContext.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class TagServiceTest {

	@InjectMocks
	@Autowired
	private TagService tagService;

	@Mock
	private TagDAO tagDAO;

	private Tag tag;

	@Before
	public void setUp() throws Exception
	{
		MockitoAnnotations.initMocks( this );
		tag = new Tag();
		tag.setTagId(new Long(1));
		tag.setName("cool");
	}
	
	@Test
	public void createTest() throws ServiceException, DAOException {
		when(tagDAO.create(tag)).thenReturn(tag.getTagId());
		Long actualId = tagService.create(tag);
		assertEquals(tag.getTagId(), actualId);
		verify(tagDAO, times(1)).create(tag);
	}

	@Test
	public void readTest() throws ServiceException, DAOException {
		Long tagId = new Long(1);
		when(tagDAO.read(tagId)).thenReturn(tag);
		Tag actualTag = tagService.read(tagId);
		assertEquals(tag, actualTag);
		verify(tagDAO, times(1)).read(tagId);
	}

	@Test
	public void updateTest() throws ServiceException, DAOException {
		tagService.update(tag);
		verify(tagDAO, times(1)).update(tag);
	}

	@Test
	public void deleteTest() throws ServiceException, DAOException {
		Long tagId = new Long(1);
		tagService.delete(tagId);
		verify(tagDAO, times(1)).delete(tagId);
	}
	
	@Test
	public void getTagsTest() throws ServiceException, DAOException {
	    List<Tag> tagList = new ArrayList<Tag>();
	    when(tagDAO.getTags()).thenReturn(tagList);
	    List<Tag> actualTagList = tagDAO.getTags();
	    assertEquals(tagList, actualTagList);
	    verify(tagDAO, times(1)).getTags();
	}

}
