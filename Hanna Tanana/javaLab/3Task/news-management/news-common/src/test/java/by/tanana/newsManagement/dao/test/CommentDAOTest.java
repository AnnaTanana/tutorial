package by.tanana.newsManagement.dao.test;

import static org.junit.Assert.assertEquals;

import java.sql.Timestamp;

import javax.sql.DataSource;

import org.junit.Test;
import org.unitils.UnitilsJUnit4;
import org.unitils.database.annotations.Transactional;
import org.unitils.database.util.TransactionMode;
import org.unitils.dbunit.annotation.DataSet;
import org.unitils.spring.annotation.SpringApplicationContext;
import org.unitils.spring.annotation.SpringBean;

import exception.DAOException;
import by.tanana.newsManagement.dao.CommentDAO;
import by.tanana.newsManagement.entity.Comment;

@SpringApplicationContext("test-applicationContext.xml")
@DataSet("CommentDAOTest.xml")
@Transactional(TransactionMode.ROLLBACK)
public class CommentDAOTest extends UnitilsJUnit4 {
	@SpringBean("dataSource")
	private DataSource dataSource;
	@SpringBean("commentDAO")
	private CommentDAO commentDAO;

	@Test
	public void readTest() throws DAOException {
		Comment actualComment = commentDAO.read(new Long(1));
		Comment expectedComment = setExpectedComment(1L, 1L, "Good news!", "2015-03-22 23:59:59");
		assertCommentsEquals(expectedComment, actualComment);
	}

	@Test
	public void createTest() throws DAOException {
		Comment expectedComment = setExpectedComment(null, 1L, "WOW!", "2015-05-10 12:15:00");
		Long id = commentDAO.create(expectedComment);
		expectedComment.setCommentId(id);
		Comment actualComment = commentDAO.read(id);
		assertCommentsEquals(expectedComment, actualComment);
	}

	@Test 
	public void updateTest() throws DAOException {
		Comment expectedComment = setExpectedComment(1L, 1L, "Bad news!", "2015-03-22 23:59:59");
		commentDAO.update(expectedComment);
		Comment actualComment = commentDAO.read(new Long(1));
		assertCommentsEquals(expectedComment, actualComment);
	}

	@Test
	public void deleteTest() throws DAOException {
		Comment expectedComment = null;
		commentDAO.delete(new Long(1));
		Comment actualComment = commentDAO.read(new Long(1));
		assertEquals(expectedComment, actualComment);
	}
		
	private Comment setExpectedComment(Long commentId, Long newsId, String text, String creationDate) {
		Comment expectedComment = new Comment();
		expectedComment.setCommentId(commentId);
		expectedComment.setNewsId(newsId);
		expectedComment.setText(text);
		expectedComment.setCreationDate(Timestamp.valueOf(creationDate));
		return expectedComment;
	}
	
	private void assertCommentsEquals(Comment expectedComment, Comment actualComment) {
		assertEquals(expectedComment.getCommentId(), actualComment.getCommentId());
		assertEquals(expectedComment.getNewsId(), actualComment.getNewsId());
		assertEquals(expectedComment.getText(), actualComment.getText());
		assertEquals(expectedComment.getCreationDate(), actualComment.getCreationDate());
	}


}
