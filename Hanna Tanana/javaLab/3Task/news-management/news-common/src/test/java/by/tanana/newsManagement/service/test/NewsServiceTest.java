package by.tanana.newsManagement.service.test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import exception.DAOException;
import exception.OptimisticLockDAOException;
import exception.OptimisticLockServiceException;
import exception.ServiceException;
import by.tanana.newsManagement.dao.NewsDAO;
import by.tanana.newsManagement.entity.News;
import by.tanana.newsManagement.entity.NewsVOForm;
import by.tanana.newsManagement.entity.SearchCriteria;
import by.tanana.newsManagement.service.NewsService;


@ContextConfiguration(locations = {"classpath:/test-applicationContext.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class NewsServiceTest {

    @InjectMocks
    @Autowired
    private NewsService newsService;

    @Mock
    private NewsDAO newsDAO;

    private News news;

    @Before
    public void setUp() throws Exception
    {
	MockitoAnnotations.initMocks( this );
	news = new News();
	news.setNewsId(new Long(1));
	news.setTitle("European Summit");
	news.setShortText("European Summit Strikes Greece Bailout Deal");
	news.setFullText("Greece reached an agreement with its creditors over the reforms needed to start talks for a third bailout in five years and remain in the euro.");
	news.setCreationDate(Timestamp.valueOf("2015-06-16 22:30:00"));
	news.setModificationDate(Date.valueOf("2015-06-16"));
    }

    @Test
    public void createTest() throws ServiceException, DAOException {
	NewsVOForm newsForm = new NewsVOForm();
	newsForm.setNews(news);
	when(newsDAO.create(news)).thenReturn(news.getNewsId());
	Long actualId = newsService.create(newsForm);
	assertEquals(news.getNewsId(), actualId);
	verify(newsDAO, times(1)).create(news);	
    }

    @Test
    public void readTest() throws ServiceException, DAOException {
	Long newsId = new Long(1);	
	when(newsDAO.read(newsId)).thenReturn(news);
	News actualNews = newsService.read(newsId);
	assertEquals(news, actualNews);
	verify(newsDAO, times(1)).read(newsId);
    }

    @Test
    public void updateTest() throws ServiceException, DAOException, OptimisticLockServiceException, OptimisticLockDAOException {
	NewsVOForm newsForm = new NewsVOForm();
	newsForm.setNews(news);
	newsService.update(newsForm);
	verify(newsDAO, times(1)).update(news);
    }

    @Test
    public void deleteTest() throws ServiceException, DAOException {
	List<Long> newsIdList = new ArrayList<Long>();
	newsService.delete(newsIdList);
	verify(newsDAO, times(1)).delete(newsIdList);
    }

    @Test
    public void countSearchedNewsTest() throws ServiceException, DAOException {
	Long count = 2L;
	SearchCriteria searchCriteria = new SearchCriteria();
	when(newsDAO.countSearchedNews(searchCriteria)).thenReturn(count);
	Long actualCount = newsService.countSearchedNews(searchCriteria);
	assertEquals(count, actualCount);
	verify(newsDAO, times(1)).countSearchedNews(searchCriteria);
    }

    @Test
    public void searchNewsByAuthorIdTest() throws ServiceException, DAOException {
	SearchCriteria searchCriteria = new SearchCriteria();
	searchCriteria.setAuthorId(new Long(1));
	List<News> expectedNewsList = new ArrayList<News>();
	expectedNewsList.add(news);
	when(newsDAO.getNews(searchCriteria, 1, 3)).thenReturn(expectedNewsList);
	List<News> actualNewsList = newsService.getNews(searchCriteria, 1, 3);
	assertEquals(expectedNewsList.get(0), actualNewsList.get(0));
	verify(newsDAO, times(1)).getNews(searchCriteria, 1, 3);
    }

    @Test
    public void searchAllNewsTest() throws ServiceException, DAOException {
	SearchCriteria searchCriteria = new SearchCriteria();
	List<News> expectedNewsList = new ArrayList<News>();
	expectedNewsList.add(news);
	when(newsDAO.getNews(searchCriteria, 1, 3)).thenReturn(expectedNewsList);
	List<News> actualNewsList = newsService.getNews(searchCriteria, 1, 3);
	assertEquals(news, actualNewsList.get(0));
	verify(newsDAO, times(1)).getNews(searchCriteria, 1, 3);
    }

    @Test
    public void searchNewsByTagIdTest() throws ServiceException, DAOException {
	SearchCriteria searchCriteria = new SearchCriteria();
	List<Long> expectedTagList = new ArrayList<Long>();
	expectedTagList.add(new Long(1));
	expectedTagList.add(new Long(2));
	searchCriteria.setTagIdList(expectedTagList);
	List<News> expectedNewsList = new ArrayList<News>();
	expectedNewsList.add(news);
	when(newsDAO.getNews(searchCriteria, 1, 3)).thenReturn(expectedNewsList);
	List<News> actualNewsList = newsService.getNews(searchCriteria, 1, 3);
	assertEquals(news, actualNewsList.get(0));
	verify(newsDAO, times(1)).getNews(searchCriteria, 1, 3);
    }

    @Test
    public void searchNewsByAuthorAndTagIdTest() throws ServiceException, DAOException {
	SearchCriteria searchCriteria = new SearchCriteria();
	List<Long> expectedTagList = new ArrayList<Long>();
	expectedTagList.add(new Long(1));
	expectedTagList.add(new Long(2));
	searchCriteria.setTagIdList(expectedTagList);
	searchCriteria.setAuthorId(new Long(1));
	List<News> expectedNewsList = new ArrayList<News>();
	expectedNewsList.add(news);
	when(newsDAO.getNews(searchCriteria, 1, 3)).thenReturn(expectedNewsList);
	List<News> actualNewsList = newsService.getNews(searchCriteria, 1, 3);
	assertEquals(news, actualNewsList.get(0));
	verify(newsDAO, times(1)).getNews(searchCriteria, 1, 3);
    }
}
