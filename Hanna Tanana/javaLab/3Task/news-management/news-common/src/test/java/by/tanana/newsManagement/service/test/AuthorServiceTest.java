package by.tanana.newsManagement.service.test;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import exception.DAOException;
import exception.ServiceException;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import by.tanana.newsManagement.dao.AuthorDAO;
import by.tanana.newsManagement.entity.Author;
import by.tanana.newsManagement.service.AuthorService;


@ContextConfiguration(locations = {"classpath:/test-applicationContext.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class AuthorServiceTest {
	@InjectMocks
	@Autowired
	private AuthorService authorService;

	@Mock
	private AuthorDAO authorDAO;
	
	private Author author;

	@Before
	public void setUp() throws Exception
	{
		MockitoAnnotations.initMocks( this );
		author = new Author();
		author.setAuthorId(new Long(1));
		author.setName("Bill");
	}

	@Test
	public void createTest() throws ServiceException, DAOException {		
		when(authorDAO.create(author)).thenReturn(author.getAuthorId());
		Long actualId = authorService.create(author);
		verify(authorDAO, times(1)).create(author);	
		assertEquals(author.getAuthorId(), actualId);
	}

	@Test
	public void readTest() throws ServiceException, DAOException {
		Long authorId = new Long(1);	
		when(authorDAO.read(authorId)).thenReturn(author);
		Author actualAuthor = authorService.read(authorId);
		assertEquals(author, actualAuthor);
		verify(authorDAO, times(1)).read(authorId);
	}
	
	@Test
	public void updateTest() throws ServiceException, DAOException {
		authorService.update(author);
		verify(authorDAO, times(1)).update(author);
	}

	@Test
	public void deleteTest() throws ServiceException, DAOException {
		Long authorId = new Long(1);
		Timestamp expiredTime = Timestamp.valueOf("2015-06-16 22:30:00");
		authorService.delete(authorId, expiredTime);
		verify(authorDAO, times(1)).delete(authorId, expiredTime);
	}
	
	@Test
	public void getAuthorsTest() throws ServiceException, DAOException {
	    List<Author> authorList = new ArrayList<Author>();
	    when(authorDAO.getAuthors()).thenReturn(authorList);
	    List<Author> actualAuthorList = authorDAO.getAuthors();
	    assertEquals(authorList, actualAuthorList);
	    verify(authorDAO, times(1)).getAuthors();
	}

}
