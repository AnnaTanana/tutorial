## traces-app-0.0.21 (2017-03-07)

Features:
 - None
  1.
  2.
  3.

Bug fixes:
 - Fix DC4 Prod LTM for Stay Preferences service (was pointing to ST1 LTM)
 1.
 2.
 3.
 - Fix unstable unit tests
 - Add app version to Swagger UI
 - Use correct params for deployment to Staging region in Jenkinsfile