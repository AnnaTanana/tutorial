package by.tanana.newsManagement.service.test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import exception.ServiceException;
import by.tanana.newsManagement.entity.AuthorTO;
import by.tanana.newsManagement.entity.NewsTO;
import by.tanana.newsManagement.service.AuthorService;
import by.tanana.newsManagement.service.CommentService;
import by.tanana.newsManagement.service.NewsManagementService;
import by.tanana.newsManagement.service.NewsService;
import by.tanana.newsManagement.service.TagService;


@ContextConfiguration(locations = {"classpath:/test-applicationContext.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class NewsManagementServiceTest {
	@InjectMocks
	@Autowired
	private NewsManagementService newsManagementService;

	@Mock
	private AuthorService authorService;
	@Mock
	private CommentService commentService;
	@Mock
	private TagService tagService;
	@Mock
	private NewsService newsService;

	private NewsTO news;
	private AuthorTO author;

	@Before
	public void setUp() throws Exception
	{
		MockitoAnnotations.initMocks( this );

		news = new NewsTO();
		news.setNewsId(new Long(1));
		news.setTitle("European Summit");
		news.setShortText("European Summit Strikes Greece Bailout Deal");
		news.setFullText("Greece reached an agreement with its creditors over the reforms needed to start talks for a third bailout in five years and remain in the euro.");
		news.setCreationDate(Timestamp.valueOf("2015-06-16 22:30:00"));
		news.setModificationDate(Date.valueOf("2015-06-16"));

		author = new AuthorTO();
		author.setAuthorId(new Long(1));
		author.setName("Bill");
	}

	@Test
	public void addNews() throws ServiceException {
		InOrder inOrder = Mockito.inOrder(newsService, authorService, tagService);
		List<Long> tags = new ArrayList<Long>();
		tags.add(new Long(1));
		tags.add(new Long(2));
		when(newsService.create(news)).thenReturn(news.getNewsId());
		Long actualId = newsManagementService.addNews(news, author, tags);
		assertEquals(news.getNewsId(), actualId);
		inOrder.verify(newsService, times(1)).create(news);
		inOrder.verify(authorService, times(1)).bindNewsWithAuthor(author.getAuthorId(), news.getNewsId());
		inOrder.verify(tagService, times(1)).bindMultipleTagsWithNews(tags, news.getNewsId());		
	}

	@Test 
	public void deleteNews() throws ServiceException {
		InOrder inOrder = Mockito.inOrder(newsService, authorService, tagService, commentService);
		Long newsId = new Long(1);
		newsManagementService.deleteNews(newsId);	
		inOrder.verify(newsService, times(1)).delete(newsId);
		inOrder.verify(authorService, times(1)).deleteAuthorsFromNewsAuthorByNewsId(newsId);
		inOrder.verify(tagService, times(1)).deleteTagsFromNewsTagByNewsId(newsId);
		inOrder.verify(commentService, times(1)).deleteCommentByNewsId(newsId);

	}
	
	@Test
	public void deleteTag() throws ServiceException {
		InOrder inOrder = Mockito.inOrder(tagService, newsService);
		Long tagId = new Long(1);
		newsManagementService.deleteTag(tagId);
		inOrder.verify(tagService, times(1)).delete(tagId);
		inOrder.verify(newsService, times(1)).deleteFromNewsTagByTagId(tagId);
	}


}
