package by.tanana.newsManagement.service.test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import exception.DAOException;
import exception.ServiceException;
import by.tanana.newsManagement.dao.NewsDAO;
import by.tanana.newsManagement.entity.NewsTO;
import by.tanana.newsManagement.entity.SearchCriteria;
import by.tanana.newsManagement.service.NewsService;


@ContextConfiguration(locations = {"classpath:/test-applicationContext.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class NewsServiceTest {
	
	@InjectMocks
	@Autowired
	private NewsService newsService;

	@Mock
	private NewsDAO newsDAO;
	
	private NewsTO news;

	@Before
	public void setUp() throws Exception
	{
		MockitoAnnotations.initMocks( this );
		news = new NewsTO();
		news.setNewsId(new Long(1));
		news.setTitle("European Summit");
		news.setShortText("European Summit Strikes Greece Bailout Deal");
		news.setFullText("Greece reached an agreement with its creditors over the reforms needed to start talks for a third bailout in five years and remain in the euro.");
		news.setCreationDate(Timestamp.valueOf("2015-06-16 22:30:00"));
		news.setModificationDate(Date.valueOf("2015-06-16"));
	}
	
	@Test
	public void createTest() throws ServiceException, DAOException {		
		when(newsDAO.create(news)).thenReturn(news.getNewsId());
		Long actualId = newsService.create(news);
		assertEquals(news.getNewsId(), actualId);
		verify(newsDAO, times(1)).create(news);	
	}

	@Test
	public void readTest() throws ServiceException, DAOException {
		Long newsId = new Long(1);	
		when(newsDAO.read(newsId)).thenReturn(news);
		NewsTO actualNews = newsService.read(newsId);
		assertEquals(news, actualNews);
		verify(newsDAO, times(1)).read(newsId);
	}
	
	@Test
	public void readNewsByTagId() throws ServiceException, DAOException {
		Long tagId = new Long(1);
		when(newsDAO.readNewsByTagId(tagId)).thenReturn(news);
		NewsTO actualNews = newsService.readNewsByTagId(tagId);
		assertEquals(news, actualNews);
		verify(newsDAO, times(1)).readNewsByTagId(tagId);
	}

	@Test
	public void updateTest() throws ServiceException, DAOException {
		newsService.update(news);
		verify(newsDAO, times(1)).update(news);
	}

	@Test
	public void deleteTest() throws ServiceException, DAOException {
		Long newsId = new Long(1);
		newsService.delete(newsId);
		verify(newsDAO, times(1)).delete(newsId);
	}
	
	@Test
	public void countAllNewsTest() throws ServiceException, DAOException {
		when(newsDAO.countAllNews()).thenReturn(2);
		int numberOfNews = newsService.countAllNews();
		assertEquals(2, numberOfNews);
		verify(newsDAO, times(1)).countAllNews();
	}
	
	@Test
	public void bindingNewsWithTagTest() throws ServiceException, DAOException {
		Long newsId = new Long(1);
		Long tagId = new Long(1);
		newsService.bindNewsWithTag(tagId, newsId);
		verify(newsDAO, times(1)).bindNewsWithTag(tagId, newsId);
	}
	
	@Test
	public void deleteFromNewsTagByTagIdTest() throws ServiceException, DAOException {
		Long tagId = new Long(1);
		newsService.deleteFromNewsTagByTagId(tagId);
		verify(newsDAO, times(1)).deleteFromNewsTagByTagId(tagId);
	}
	
	/*@Test
	public void searchNewsByAuthorIdTest() throws ServiceException, DAOException {
		SearchCriteria searchCriteria = new SearchCriteria();
		searchCriteria.setAuthorId(new Long(1));
		List<NewsTO> expectedNewsList = new ArrayList<NewsTO>();
		expectedNewsList.add(news);
		when(newsDAO.getNews(searchCriteria)).thenReturn(expectedNewsList);
		List<NewsTO> actualNewsList = newsService.getNews(searchCriteria);
		assertEquals(expectedNewsList.get(0), actualNewsList.get(0));
		verify(newsDAO, times(1)).getNews(searchCriteria);
	}
	
	@Test
	public void searchAllNewsTest() throws ServiceException, DAOException {
		SearchCriteria searchCriteria = new SearchCriteria();
		List<NewsTO> expectedNewsList = new ArrayList<NewsTO>();
		expectedNewsList.add(news);
		when(newsDAO.getNews(searchCriteria)).thenReturn(expectedNewsList);
		List<NewsTO> actualNewsList = newsService.getNews(searchCriteria);
		assertEquals(news, actualNewsList.get(0));
		verify(newsDAO, times(1)).getNews(searchCriteria);
	}
	
	@Test
	public void searchNewsByTagIdTest() throws ServiceException, DAOException {
		SearchCriteria searchCriteria = new SearchCriteria();
		List<Long> expectedTagList = new ArrayList<Long>();
		expectedTagList.add(new Long(1));
		expectedTagList.add(new Long(2));
		searchCriteria.setTagIdList(expectedTagList);
		List<NewsTO> expectedNewsList = new ArrayList<NewsTO>();
		expectedNewsList.add(news);
		when(newsDAO.getNews(searchCriteria)).thenReturn(expectedNewsList);
		List<NewsTO> actualNewsList = newsService.getNews(searchCriteria);
		assertEquals(news, actualNewsList.get(0));
		verify(newsDAO, times(1)).getNews(searchCriteria);
	}

	@Test
	public void searchNewsByAuthorAndTagIdTest() throws ServiceException, DAOException {
		SearchCriteria searchCriteria = new SearchCriteria();
		List<Long> expectedTagList = new ArrayList<Long>();
		expectedTagList.add(new Long(1));
		expectedTagList.add(new Long(2));
		searchCriteria.setTagIdList(expectedTagList);
		searchCriteria.setAuthorId(new Long(1));
		List<NewsTO> expectedNewsList = new ArrayList<NewsTO>();
		expectedNewsList.add(news);
		when(newsDAO.getNews(searchCriteria)).thenReturn(expectedNewsList);
		List<NewsTO> actualNewsList = newsService.getNews(searchCriteria);
		assertEquals(news, actualNewsList.get(0));
		verify(newsDAO, times(1)).getNews(searchCriteria);
	}*/
}
