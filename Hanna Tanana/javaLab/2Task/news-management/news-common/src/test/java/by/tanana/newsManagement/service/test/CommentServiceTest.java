package by.tanana.newsManagement.service.test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.sql.Timestamp;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import exception.DAOException;
import exception.ServiceException;
import by.tanana.newsManagement.dao.CommentDAO;
import by.tanana.newsManagement.entity.CommentTO;
import by.tanana.newsManagement.service.CommentService;

@ContextConfiguration(locations = {"classpath:/test-applicationContext.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class CommentServiceTest {
	@InjectMocks
	@Autowired
	private CommentService commentService;

	@Mock
	private CommentDAO commentDAO;
	
	private CommentTO comment;

	@Before
	public void setUp() throws Exception
	{
		MockitoAnnotations.initMocks( this );
		comment = new CommentTO();
		comment.setCommentId(new Long(1));
		comment.setNewsId(new Long(1));
		comment.setText("I was shocked by the news");
		comment.setCreationDate(Timestamp.valueOf("2015-05-10 12:15:00"));
	}

	@Test
	public void createTest() throws ServiceException, DAOException {
		when(commentDAO.create(comment)).thenReturn(comment.getCommentId());
		Long actualId = commentService.create(comment);
		assertEquals(comment.getCommentId(), actualId);
		verify(commentDAO, times(1)).create(comment);
	}
	
	@Test
	public void readTest() throws ServiceException, DAOException {
		Long commentId = new Long(1);
		when(commentDAO.read(commentId)).thenReturn(comment);
		CommentTO actualComment = commentService.read(commentId);
		assertEquals(comment, actualComment);
		verify(commentDAO, times(1)).read(commentId);
	}
	
	@Test
	public void updateTest() throws ServiceException, DAOException {
		commentService.update(comment);
		verify(commentDAO, times(1)).update(comment);
	}
	
	@Test
	public void deleteTest() throws ServiceException, DAOException {
		Long commentId = new Long(1);
		commentService.delete(commentId);
		verify(commentDAO, times(1)).delete(commentId);
	}
	
	@Test
	public void deleteCommentByNewsIdTest() throws ServiceException, DAOException {
		Long newsId = new Long(1);
		commentService.deleteCommentByNewsId(newsId);
		verify(commentDAO, times(1)).deleteCommentByNewsId(newsId);
	}

}
