--------------------------------------------------------
--  DDL for Table AUTHOR
--------------------------------------------------------

  CREATE TABLE AUTHOR 
   (	AUTHOR_ID NUMBER(20) NOT NULL
   ,  AUTHOR_NAME NVARCHAR2(30) NOT NULL
   ,  EXPIRED TIMESTAMP
   ,  CONSTRAINT AUTHOR_PK PRIMARY KEY (AUTHOR_ID)
   );
--------------------------------------------------------
--  DDL for Table NEWS
--------------------------------------------------------
 CREATE TABLE NEWS
   (	NEWS_ID NUMBER(20) NOT NULL
   ,  TITLE NVARCHAR2(30) NOT NULL
   ,  SHORT_TEXT NVARCHAR2(100) NOT NULL
   ,  FULL_TEXT NVARCHAR2(2000) NOT NULL
   ,  CREATION_DATE TIMESTAMP NOT NULL  
   ,  MODIFICATION_DATE DATE NOT NULL
   ,  CONSTRAINT NEWS_PK PRIMARY KEY (NEWS_ID)
   );
--------------------------------------------------------
--  DDL for Table COMMENTS
--------------------------------------------------------

  CREATE TABLE COMMENTS 
   (	COMMENT_ID NUMBER(20) NOT NULL
   ,  NEWS_ID NUMBER(20) NOT NULL
   ,  COMMENT_TEXT NVARCHAR2(100) NOT NULL
   ,  CREATION_DATE TIMESTAMP NOT NULL
   ,  CONSTRAINT COMMENT_PK PRIMARY KEY (COMMENT_ID)
   ,  CONSTRAINT NEWS_FK
      FOREIGN KEY (NEWS_ID)
      REFERENCES NEWS(NEWS_ID)
   );
--------------------------------------------------------
--  DDL for Table ROLES
--------------------------------------------------------

  CREATE TABLE ROLES
   (	USER_ID NUMBER(20) NOT NULL
   ,  ROLE_NAME VARCHAR2(50) NOT NULL
   ,  CONSTRAINT USER_FK
      FOREIGN KEY (USER_ID)
      REFERENCES USERS(USER_ID)
   );
--------------------------------------------------------
--  DDL for Table TAG
--------------------------------------------------------

  CREATE TABLE TAG 
   (	TAG_ID NUMBER(20) NOT NULL
   ,  TAG_NAME NVARCHAR2(30) NOT NULL
   ,  CONSTRAINT TAG_PK PRIMARY KEY (TAG_ID)
   );
--------------------------------------------------------
--  DDL for Table USERS
--------------------------------------------------------

  CREATE TABLE USERS 
   (	USER_ID NUMBER(20) NOT NULL
   ,  USER_NAME NVARCHAR2(50) NOT NULL
   ,  LOGIN VARCHAR2(30) NOT NULL
   ,  PASSWORD VARCHAR2(30) NOT NULL
   ,  CONSTRAINT USER_PK PRIMARY KEY (USER_ID)
   );
--------------------------------------------------------
--  DDL for Table NEWS_AUTHOR
--------------------------------------------------------

  CREATE TABLE NEWS_AUTHOR
   (	NEWS_ID NUMBER(20) NOT NULL
   ,  AUTHOR_ID NUMBER(20) NOT NULL
   ,  CONSTRAINT NEWS_FK1
      FOREIGN KEY (NEWS_ID)
      REFERENCES NEWS(NEWS_ID)
   ,  CONSTRAINT AUTHOR_FK
      FOREIGN KEY (AUTHOR_ID)
      REFERENCES AUTHOR(AUTHOR_ID)
   );
--------------------------------------------------------
--  DDL for Table NEWS_TAG
--------------------------------------------------------

  CREATE TABLE NEWS_TAG 
   (	TAG_ID NUMBER(20) NOT NULL
   ,  NEWS_ID NUMBER(20) NOT NULL
   ,  CONSTRAINT NEWS_FK2
      FOREIGN KEY (NEWS_ID)
      REFERENCES NEWS(NEWS_ID)
   ,  CONSTRAINT TAG_FK
      FOREIGN KEY (TAG_ID)
      REFERENCES TAG(TAG_ID)
   ); 
--------------------------------------------------------
--  DDL for Sequence AUTHOR_SEQ
--------------------------------------------------------
    CREATE SEQUENCE AUTHOR_SEQ
    START WITH 1 
    INCREMENT BY 1 
    NOMAXVALUE;
--------------------------------------------------------
--  DDL for Sequence COMMENT_SEQ
--------------------------------------------------------
    CREATE SEQUENCE COMMENT_SEQ
    START WITH 1 
    INCREMENT BY 1 
    NOMAXVALUE;
--------------------------------------------------------
--  DDL for Sequence NEWS_SEQ
--------------------------------------------------------
    CREATE SEQUENCE NEWS_SEQ
    START WITH 1 
    INCREMENT BY 1 
    NOMAXVALUE;
--------------------------------------------------------
--  DDL for Sequence TAG_SEQ
--------------------------------------------------------
    CREATE SEQUENCE TAG_SEQ
    START WITH 1 
    INCREMENT BY 1 
    NOMAXVALUE;
--------------------------------------------------------
--  DDL for Sequence USERS_SEQ
--------------------------------------------------------
    CREATE SEQUENCE USERS_SEQ
    START WITH 1 
    INCREMENT BY 1 
    NOMAXVALUE;