package by.tanana.newsManagement.dao;

import java.util.List;

import exception.DAOException;
import by.tanana.newsManagement.entity.CommentTO;

public interface CommentDAO {
	
	/**Insert a new comment entity to database
	 * by auto generating it's id.
	 * @param comment
	 * @return generated comment id
	 * @throws DAOException
	 */
	public Long create(CommentTO comment) throws DAOException;
	
	/**Read a comment from database
	 * by comment's id.
	 * @param commentId
	 * @return comment
	 * @throws DAOException
	 */
	public CommentTO read(Long commentId) throws DAOException;
	
	/**Read a list of comments from database by newsId.
	 * @param newsId
	 * @return list of comments
	 * @throws DAOException
	 */
	public List<CommentTO> readListOfCommentsByNewsId(Long newsId) throws DAOException;
	
	/**Update a comment by changing some CommentTO's fields.
	 * @param comment
	 * @throws DAOException
	 */
	public void update(CommentTO comment) throws DAOException;
	
	/**Deleting comment by it's id from database.
	 * @param commentId
	 * @throws DAOException
	 */
	public void delete(Long commentId) throws DAOException;
	
	/**Deleting comment from database by newsId.
	 * @param newsId
	 * @throws DAOException
	 */
	public void deleteCommentByNewsId(Long newsId) throws DAOException;
	
	/**Count number of comments in database for the news by newsId.
	 * @return number of comments
	 * @throws DAOException
	 */
	public int countCommentsForTheNews(Long newsId) throws DAOException;

}
