package by.tanana.newsManagement.dao;

import java.util.List;

import exception.DAOException;
import by.tanana.newsManagement.entity.NewsTO;
import by.tanana.newsManagement.entity.SearchCriteria;

public interface NewsDAO {
	
	/**Insert new news entity to database
	 * by auto generating it's id.
	 * @param news
	 * @return generated news id
	 * @throws DAOException
	 */
	public Long create(NewsTO news) throws DAOException;
	
	/**Read news from database
	 * by news's id.
	 * @param newsId
	 * @return news
	 * @throws DAOException
	 */
	public NewsTO read(Long newsId) throws DAOException;
	
	/**Update news by changing some NewsTO's fields.
	 * @param news
	 * @throws DAOException
	 */
	public void update(NewsTO news) throws DAOException;
	
	/**Deleting news from database by it's id.
	 * @param newsId
	 * @throws DAOException
	 */
	public void delete(Long newsId) throws DAOException;
	
	/**Read news from database by tagId 
	 * using NEWS_TAG table to get newsId.
	 * @param tagId
	 * @return news
	 * @throws DAOException
	 */
	public NewsTO readNewsByTagId(Long tagId) throws DAOException;
	
	/**Count all news in database.
	 * @return number of news
	 * @throws DAOException
	 */
	public int countAllNews() throws DAOException;
	
	/**Bind news with tag
	 * by inserting newsId and tagId in NEWS_TAG table.
	 * @param tagId
	 * @param newsId
	 * @throws DAOException
	 */
	public void bindNewsWithTag(Long tagId, Long newsId) throws DAOException;
	
	/**Unbind news with tag
	 * by deleting a bunch from NEWS_TAG table.
	 * @param tagId
	 * @throws DAOException
	 */
	public void deleteFromNewsTagByTagId(Long tagId) throws DAOException;
	
	/**Reading news from database by search criteria.
	 * @param searchCriteria
	 * @return list of news
	 * @throws DAOException
	 */
	public List<NewsTO> getNews(SearchCriteria searchCriteria, int startIndex, int endIndex) throws DAOException;
	
	/**Count the number of news found by search criteria.
	 * @param searchCriteria
	 * @return number of news
	 * @throws DAOException
	 */
	public int countSearchedNews(SearchCriteria searchCriteria) throws DAOException;
}
