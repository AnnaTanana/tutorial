package by.tanana.newsManagement.service;

import java.util.List;

import exception.ServiceException;
import by.tanana.newsManagement.entity.AuthorTO;
import by.tanana.newsManagement.entity.NewsTO;
import by.tanana.newsManagement.entity.NewsVO;
import by.tanana.newsManagement.entity.NewsVOForm;
import by.tanana.newsManagement.entity.SearchCriteria;


public interface NewsManagementService {
	
	/**Creating news entity, 
	 * bunches between News&Tag and News&Author.
	 * @param news
	 * @param author
	 * @param tags
	 * @return news id
	 * @throws ServiceException
	 */
	public Long addNews(NewsTO news, Long authorId, List<Long> tags) throws ServiceException;
	
	/**Delete news by deleting news entity,
	 * bunches Author&News, Tag&News 
	 * and comments of this news.
	 * @param newsId
	 * @throws ServiceException
	 */
	public void deleteNews(Long newsId) throws ServiceException;
	
	/**Delete tag by deleting tag entity
	 * and News&Tag bunches.
	 * @param tagId
	 * @throws ServiceException
	 */
	public void deleteTag(Long tagId) throws ServiceException;
	
	/**Get list of NewsVO entities 
	 * according to searchCriteria
	 * @param searchCriteria
	 * @return list of NewsVO
	 * @throws ServiceException
	 */
	public List<NewsVO> getSearchedNews(SearchCriteria searchCriteria, int startIndex, int endIndex) throws ServiceException;
	
	/**Get newsVO by newsId
	 * @param newsId
	 * @return NewsVO
	 * @throws ServiceException
	 */
	public NewsVO getNews(Long newsId) throws ServiceException;
	
	/**Get newsVOForm by newsId
	 * @param newsId
	 * @return NewsVOForm
	 * @throws ServiceException
	 */
	public NewsVOForm getNewsVOForm(Long newsId) throws ServiceException;
	
	/**Edit news.
	 * @param news
	 * @param authorId
	 * @param tags
	 * @throws ServiceException
	 */
	public void editNews(NewsTO news, Long authorId, List<Long> tags) throws ServiceException;
}
