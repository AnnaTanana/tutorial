package by.tanana.newsManagement.entity;

import java.util.List;

public class NewsVO {
	
	private NewsTO news;
	private AuthorTO author;
	private List<TagTO> tags;
	private List<CommentTO> comments;
	private Integer numberOfComments;
	
	public Integer getNumberOfComments() {
		return numberOfComments;
	}

	public void setNumberOfComments(Integer numberOfComments) {
		this.numberOfComments = numberOfComments;
	}

	public NewsVO(){}
	
	public NewsTO getNews() {
		return news;
	}
	public void setNews(NewsTO news) {
		this.news = news;
	}
	public AuthorTO getAuthor() {
		return author;
	}
	public void setAuthor(AuthorTO author) {
		this.author = author;
	}
	public List<TagTO> getTags() {
		return tags;
	}
	public void setTags(List<TagTO> tags) {
		this.tags = tags;
	}
	public List<CommentTO> getComments() {
		return comments;
	}
	public void setComments(List<CommentTO> comments) {
		this.comments = comments;
	}
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((author == null) ? 0 : author.hashCode());
		result = prime * result
				+ ((comments == null) ? 0 : comments.hashCode());
		result = prime * result + ((news == null) ? 0 : news.hashCode());
		result = prime
				* result
				+ ((numberOfComments == null) ? 0 : numberOfComments.hashCode());
		result = prime * result + ((tags == null) ? 0 : tags.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NewsVO other = (NewsVO) obj;
		if (author == null) {
			if (other.author != null)
				return false;
		} else if (!author.equals(other.author))
			return false;
		if (comments == null) {
			if (other.comments != null)
				return false;
		} else if (!comments.equals(other.comments))
			return false;
		if (news == null) {
			if (other.news != null)
				return false;
		} else if (!news.equals(other.news))
			return false;
		if (numberOfComments == null) {
			if (other.numberOfComments != null)
				return false;
		} else if (!numberOfComments.equals(other.numberOfComments))
			return false;
		if (tags == null) {
			if (other.tags != null)
				return false;
		} else if (!tags.equals(other.tags))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("News: ").append(news.toString()).
		append(", author: ").append(author.toString()).
		append(", tags: ").append(tags.toString()).
		append(", comments: ").append(comments.toString()).
		append(", numberOfComments: ").append(numberOfComments.toString());
		
		return stringBuilder.toString();
	}
	
}
