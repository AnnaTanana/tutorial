package by.tanana.newsManagement.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import exception.ServiceException;
import by.tanana.newsManagement.entity.AuthorTO;
import by.tanana.newsManagement.entity.NewsTO;
import by.tanana.newsManagement.entity.NewsVO;
import by.tanana.newsManagement.entity.NewsVOForm;
import by.tanana.newsManagement.entity.SearchCriteria;
import by.tanana.newsManagement.entity.TagTO;
import by.tanana.newsManagement.service.AuthorService;
import by.tanana.newsManagement.service.CommentService;
import by.tanana.newsManagement.service.NewsManagementService;
import by.tanana.newsManagement.service.NewsService;
import by.tanana.newsManagement.service.TagService;

@Transactional(rollbackFor=ServiceException.class)
public class NewsManagementServiceImpl implements NewsManagementService {

    private AuthorService authorService;
    private CommentService commentService;
    private NewsService newsService;
    private TagService tagService;

    public void setAuthorService(AuthorService authorService) {
	this.authorService = authorService;
    }

    public void setCommentService(CommentService commentService) {
	this.commentService = commentService;
    }

    public void setNewsService(NewsService newsService) {
	this.newsService = newsService;
    }

    public void setTagService(TagService tagService) {
	this.tagService = tagService;
    }

    /* 
     * @see by.tanana.newsManagement.service.NewsManagementService#addNews(by.tanana.newsManagement.entity.NewsTO, by.tanana.newsManagement.entity.AuthorTO, java.util.List)
     */
    @Override
    public Long addNews(NewsTO news, Long authorId, List<Long> tags)
	    throws ServiceException {
	Long newsId = newsService.create(news);
	authorService.bindNewsWithAuthor(authorId, newsId);
	if (tags != null) {
	    tagService.bindMultipleTagsWithNews(tags, newsId);
	}
	return newsId;
    }
    
    /* 
     * @see by.tanana.newsManagement.service.NewsManagementService#editNews(by.tanana.newsManagement.entity.NewsTO, java.lang.Long, java.util.List)
     */
    @Override
    public void editNews(NewsTO news, Long authorId, List<Long> tags) throws ServiceException {
	newsService.update(news);
	authorService.deleteAuthorsFromNewsAuthorByNewsId(news.getNewsId());
	authorService.bindNewsWithAuthor(authorId, news.getNewsId());
	if (tags != null) {
	    tagService.deleteTagsFromNewsTagByNewsId(news.getNewsId());
	    tagService.bindMultipleTagsWithNews(tags, news.getNewsId());	
	}
    }

    /* 
     * @see by.tanana.newsManagement.service.NewsManagementService#deleteNews(java.lang.Long)
     */
    @Override
    public void deleteNews(Long newsId) throws ServiceException {
	authorService.deleteAuthorsFromNewsAuthorByNewsId(newsId);
	tagService.deleteTagsFromNewsTagByNewsId(newsId);
	commentService.deleteCommentByNewsId(newsId);
	newsService.delete(newsId);
    }

    /* 
     * @see by.tanana.newsManagement.service.NewsManagementService#deleteTag(java.lang.Long)
     */
    @Override
    public void deleteTag(Long tagId) throws ServiceException {
	newsService.deleteFromNewsTagByTagId(tagId);
	tagService.delete(tagId);

    }

    /* 
     * @see by.tanana.newsManagement.service.NewsManagementService#getSearchedNews(by.tanana.newsManagement.entity.SearchCriteria)
     */
    @Override
    public List<NewsVO> getSearchedNews(SearchCriteria searchCriteria, int startIndex, int endIndex) throws ServiceException {
	List<NewsTO> newsTOList = newsService.getNews(searchCriteria, startIndex, endIndex);
	List<NewsVO> newsList = null;
	if (newsTOList != null  && !newsTOList.isEmpty()) {
	    newsList = new ArrayList<NewsVO>();
	    for (NewsTO news: newsTOList) {
		NewsVO newsVO = new NewsVO();
		newsVO.setNews(news);
		newsVO.setAuthor(authorService.readByNewsId(news.getNewsId()));
		newsVO.setTags(tagService.readListOfTagsByNewsId(news.getNewsId()));
		newsVO.setComments(commentService.readListOfCommentsByNewsId(news.getNewsId()));
		newsVO.setNumberOfComments(commentService.countCommentsForTheNews(news.getNewsId()));
		newsList.add(newsVO);
	    }
	}
	return newsList;
    }

    @Override
    public NewsVO getNews(Long newsId) throws ServiceException {
	NewsVO newsVO = null;
	NewsTO news = newsService.read(newsId);
	if (news != null) {
	    newsVO = new NewsVO();
	    newsVO.setNews(news);
	    newsVO.setAuthor(authorService.readByNewsId(newsId));
	    newsVO.setTags(tagService.readListOfTagsByNewsId(newsId));
	    newsVO.setComments(commentService.readListOfCommentsByNewsId(newsId));
	    newsVO.setNumberOfComments(commentService.countCommentsForTheNews(newsId));
	}
	return newsVO;
    }

    /* 
     * @see by.tanana.newsManagement.service.NewsManagementService#getNewsVOForm(java.lang.Long)
     */
    @Override
    public NewsVOForm getNewsVOForm(Long newsId) throws ServiceException {
	NewsVOForm newsVOForm = null;
	NewsTO news = newsService.read(newsId);
	if (news != null) {
	    newsVOForm = new NewsVOForm();
	    newsVOForm.setNews(news);
	    AuthorTO author = authorService.readByNewsId(newsId);
	    if (author != null) {
		newsVOForm.setAuthorId(author.getAuthorId());
	    }
	    List<TagTO> tagList = tagService.readListOfTagsByNewsId(newsId);
	    if (tagList != null) {
		List<Long> tagIdList = new ArrayList<Long>();
		for (TagTO tag: tagList) {
		    tagIdList.add(tag.getTagId());
		}
		newsVOForm.setTagIdList(tagIdList);
	    }

	}
	return newsVOForm;
    }



}
