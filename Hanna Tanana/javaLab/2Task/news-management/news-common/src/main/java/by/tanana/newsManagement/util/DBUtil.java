package by.tanana.newsManagement.util;


import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.datasource.DataSourceUtils;

public class DBUtil {

	private static final Logger logger = LoggerFactory.getLogger(DBUtil.class);

	public static void close(ResultSet resultSet, Statement statement, Connection connection, DataSource dataSource) {
		if (resultSet != null) {
			try {
				resultSet.close();
			} catch (SQLException e) {
				logger.error("ResultSet is null " + e);
			}
		}
		if (statement != null) {
			try {
				statement.close();
			} catch (SQLException e) {
				logger.error("Statement is null " + e);
			}
		}
		if (connection != null) {
			DataSourceUtils.releaseConnection(connection, dataSource);
		} else {
			logger.error("Connection is null");
		}		
	}

	public static void close(Statement statement, Connection connection, DataSource dataSource) {
		if (statement != null) {
			try {
				statement.close();
			} catch (SQLException e) {
				logger.error("Statement is null " + e);
			}
		}
		if (connection != null) {
			DataSourceUtils.releaseConnection(connection, dataSource);
		} else {
			logger.error("Connection is null");
		}		
	}

}
