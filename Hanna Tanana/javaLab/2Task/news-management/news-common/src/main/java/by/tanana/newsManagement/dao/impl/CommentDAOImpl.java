package by.tanana.newsManagement.dao.impl;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;












import org.springframework.jdbc.datasource.DataSourceUtils;

import exception.DAOException;
import by.tanana.newsManagement.dao.CommentDAO;
import by.tanana.newsManagement.entity.CommentTO;
import by.tanana.newsManagement.util.DBUtil;

public class CommentDAOImpl implements CommentDAO {

	private static final String SQL_CREATE_COMMENT 
	= "INSERT INTO comments "
			+ "(comment_id, news_id, comment_text, creation_date) "
			+ "VALUES (comment_seq.nextval, ?, ?, ?)";

	private static final String SQL_READ_COMMENT 
	= "SELECT comment_id, news_id, comment_text, creation_date "
			+ "FROM comments "
			+ "WHERE comment_id = ?"; 

	private static final String SQL_UPDATE_COMMENT 
	= "UPDATE comments "
			+ "SET comment_text = ? "
			+ "WHERE comment_id = ?";

	private static final String SQL_DELETE_COMMENT 
	= "DELETE FROM comments "
			+ "WHERE comment_id = ?";

	private static final String SQL_DELETE_COMMENT_BY_NEWS_ID
	= "DELETE FROM comments "
			+ "WHERE news_id = ?";

	private static final String SQL_READ_LIST_OF_COMMENTS_BY_NEWS_ID
	= "SELECT comment_id, news_id, comment_text, creation_date "
			+ "FROM comments "
			+ "WHERE news_id = ? ";
	
	private static final String SQL_COUNT_COMMENT_FOR_NEWS 
	= "SELECT COUNT(comment_id) as CNT "
			+ "FROM comments "
			+ "WHERE news_id = ?";

	private DataSource dataSource;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	/* 
	 * @see by.tanana.newsManagement.dao.CommentDAO#create(by.tanana.newsManagement.entity.CommentTO)
	 */
	@Override
	public Long create(CommentTO comment) throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet generatedKeys = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			statement = connection.prepareStatement(SQL_CREATE_COMMENT, new String [] {"COMMENT_ID"});
			Long commentId = null;
			statement.setLong(1, comment.getNewsId());
			statement.setString(2, comment.getText());
			statement.setTimestamp(3, new Timestamp(comment.getCreationDate().getTime()));
			statement.executeUpdate();
			generatedKeys = statement.getGeneratedKeys();
			if (null != generatedKeys && generatedKeys.next()) {
				commentId = Long.parseLong(generatedKeys.getString(1));
			}
			return commentId;        	
		} catch (SQLException e) {
			throw new DAOException("Error while creating a comment: "+comment, e);
		} finally {
			DBUtil.close(generatedKeys, statement, connection, dataSource);
		}
	}

	/* 
	 * @see by.tanana.newsManagement.dao.CommentDAO#read(java.lang.Long)
	 */
	@Override
	public CommentTO read(Long commentId) throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			statement = connection.prepareStatement(SQL_READ_COMMENT);
			statement.setLong(1, commentId);			
			CommentTO comment = null;
			resultSet = statement.executeQuery();
			if (resultSet.next()) {
				comment = new CommentTO();
				comment.setCommentId(resultSet.getLong("COMMENT_ID"));
				comment.setNewsId(resultSet.getLong("NEWS_ID"));
				comment.setText(resultSet.getString("COMMENT_TEXT"));
				Timestamp timestamp = resultSet.getTimestamp("CREATION_DATE");
				Long milliseconds = timestamp.getTime();
				Date date = new Date(milliseconds);
				comment.setCreationDate(date);

			}
			return comment;
		} catch (SQLException e) {
			throw new DAOException("Error while reading a comment by comment id: " + commentId, e);
		} finally {
			DBUtil.close(resultSet, statement, connection, dataSource);
		} 
	}

	/* 
	 * @see by.tanana.newsManagement.dao.CommentDAO#update(by.tanana.newsManagement.entity.CommentTO)
	 */
	@Override
	public void update(CommentTO comment) throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			statement = connection.prepareStatement(SQL_UPDATE_COMMENT);
			statement.setString(1, comment.getText());
			statement.setLong(2, comment.getCommentId());
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("Error while updating a comment: " + comment, e);
		} finally {
			DBUtil.close(statement, connection, dataSource);
		}		
	}

	/* 
	 * @see by.tanana.newsManagement.dao.CommentDAO#delete(java.lang.Long)
	 */
	@Override
	public void delete(Long commentId) throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			statement = connection.prepareStatement(SQL_DELETE_COMMENT);
			statement.setLong(1, commentId);
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("Error while deleting a comment by comment id: " + commentId, e);
		} finally {
			DBUtil.close(statement, connection, dataSource);
		}
	}

	/* 
	 * @see by.tanana.newsManagement.dao.CommentDAO#deleteCommentByNewsId(java.lang.Long)
	 */
	@Override
	public void deleteCommentByNewsId(Long newsId) throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			statement = connection.prepareStatement(SQL_DELETE_COMMENT_BY_NEWS_ID);
			statement.setLong(1, newsId);
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("Error while deleting comments by news id: " + newsId, e);
		} finally {
			DBUtil.close(statement, connection, dataSource);
		}

	}

	/* 
	 * @see by.tanana.newsManagement.dao.CommentDAO#readListOfCommentsByNewsId(java.lang.Long)
	 */
	@Override
	public List<CommentTO> readListOfCommentsByNewsId(Long newsId) throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			statement = connection.prepareStatement(SQL_READ_LIST_OF_COMMENTS_BY_NEWS_ID);
			statement.setLong(1, newsId);			
			List<CommentTO> commentList = null;        	
			resultSet = statement.executeQuery();
			if (resultSet.next()) {
				commentList = new ArrayList<CommentTO>();
				do {
					CommentTO comment = new CommentTO();
					comment.setCommentId(Long.parseLong(resultSet.getString("COMMENT_ID")));
					comment.setNewsId(Long.parseLong(resultSet.getString("NEWS_ID")));
					comment.setText(resultSet.getString("COMMENT_TEXT"));
					Timestamp timestamp = resultSet.getTimestamp("CREATION_DATE");
					Long milliseconds = timestamp.getTime();
					Date date = new Date(milliseconds);
					comment.setCreationDate(date);
					commentList.add(comment);
				} while (resultSet.next());        		
			}
			return commentList;
		} catch (SQLException e) {
			throw new DAOException("Error while reading list of comments by news id: " + newsId, e);
		} finally {
			DBUtil.close(resultSet, statement, connection, dataSource);
		} 
	}

	/* 
	 * @see by.tanana.newsManagement.dao.CommentDAO#countCommentsForTheNews(java.lang.Long)
	 */
	@Override
	public int countCommentsForTheNews(Long newsId) throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			statement = connection.prepareStatement(SQL_COUNT_COMMENT_FOR_NEWS);
			statement.setLong(1, newsId);			      	
			resultSet = statement.executeQuery();
			int numberOfComments = 0;
			while (resultSet.next()) {
				numberOfComments = resultSet.getInt("CNT");
			}
			return numberOfComments;
		} catch (SQLException e) {
			throw new DAOException("Error while counting comments", e);
		} finally {
			DBUtil.close(resultSet, statement, connection, dataSource);
		}		
	}

}
