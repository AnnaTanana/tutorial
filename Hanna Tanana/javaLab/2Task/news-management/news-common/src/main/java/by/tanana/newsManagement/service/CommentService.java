package by.tanana.newsManagement.service;

import java.util.List;

import exception.DAOException;
import exception.ServiceException;
import by.tanana.newsManagement.entity.CommentTO;

public interface CommentService {
	
	/**Create a new commentTO entity.
	 * @param comment
	 * @return comment id
	 * @throws ServiceException
	 */
	public Long create(CommentTO comment) throws ServiceException;
	
	/**Read a comment by it's id.
	 * @param commentId
	 * @return comment
	 * @throws ServiceException
	 */
	public CommentTO read(Long commentId) throws ServiceException;
	
	/**Read a list of comments by newsId.
	 * @param newsId
	 * @return list of comments
	 * @throws ServiceException
	 */
	public List<CommentTO> readListOfCommentsByNewsId(Long newsId) throws ServiceException;
	
	/**Update a comment by changing some CommentTO's fields.
	 * @param comment
	 * @throws ServiceException
	 */
	public void update(CommentTO comment) throws ServiceException;
	
	/**Delete comment by it's id.
	 * @param commentId
	 * @throws ServiceException
	 */
	public void delete(Long commentId) throws ServiceException;
	
	/**Delete comment by newsId.
	 * @param newsId
	 * @throws ServiceException
	 */
	public void deleteCommentByNewsId(Long newsId) throws ServiceException;
	
	/**Count number of comments for the news by newsId.
	 * @return number of comments
	 * @throws ServiceException
	 */
	public int countCommentsForTheNews(Long newsId) throws ServiceException;
	
}
