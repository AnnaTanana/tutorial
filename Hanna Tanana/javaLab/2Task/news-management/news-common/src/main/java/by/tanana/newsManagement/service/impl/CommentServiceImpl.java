package by.tanana.newsManagement.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import exception.DAOException;
import exception.ServiceException;
import by.tanana.newsManagement.dao.CommentDAO;
import by.tanana.newsManagement.entity.CommentTO;
import by.tanana.newsManagement.service.CommentService;

public class CommentServiceImpl implements CommentService {

	private static final Logger logger = LoggerFactory.getLogger(CommentServiceImpl.class);
	
	private CommentDAO commentDAO;

	public void setCommentDAO(CommentDAO commentDAO) {
		this.commentDAO = commentDAO;
	}

	/* 
	 * @see by.tanana.newsManagement.service.CommentService#create(by.tanana.newsManagement.entity.CommentTO)
	 */
	@Override
	public Long create(CommentTO comment) throws ServiceException {
		try {
			return commentDAO.create(comment);
		} catch (DAOException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e.getMessage(), e);
		}
	}

	/* 
	 * @see by.tanana.newsManagement.service.CommentService#read(java.lang.Long)
	 */
	@Override
	public CommentTO read(Long commentId) throws ServiceException {
		try {
			return commentDAO.read(commentId);
		} catch (DAOException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e.getMessage(), e);
		}
	}

	/* 
	 * @see by.tanana.newsManagement.service.CommentService#update(by.tanana.newsManagement.entity.CommentTO)
	 */
	@Override
	public void update(CommentTO comment) throws ServiceException {
		try {
			commentDAO.update(comment);
		} catch (DAOException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e.getMessage(), e);
		}
		
	}

	/* 
	 * @see by.tanana.newsManagement.service.CommentService#delete(java.lang.Long)
	 */
	@Override
	public void delete(Long commentId) throws ServiceException {
		try {
			commentDAO.delete(commentId);
		} catch (DAOException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e.getMessage(), e);
		}
		
	}

	/* 
	 * @see by.tanana.newsManagement.service.CommentService#deleteCommentByNewsId(java.lang.Long)
	 */
	@Override
	public void deleteCommentByNewsId(Long newsId) throws ServiceException {
		try {
			commentDAO.deleteCommentByNewsId(newsId);
		} catch (DAOException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e.getMessage(), e);
		}
		
	}

	/* 
	 * @see by.tanana.newsManagement.service.CommentService#readListOfCommentsByNewsId(java.lang.Long)
	 */
	@Override
	public List<CommentTO> readListOfCommentsByNewsId(Long newsId) throws ServiceException {
		try {
			return commentDAO.readListOfCommentsByNewsId(newsId);
		} catch (DAOException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e.getMessage(), e);
		}
	}

	/* 
	 * @see by.tanana.newsManagement.service.CommentService#countCommentsForTheNews(java.lang.Long)
	 */
	@Override
	public int countCommentsForTheNews(Long newsId) throws ServiceException {
		try {
			return commentDAO.countCommentsForTheNews(newsId);
		} catch (DAOException e) {
			logger.error(e.getMessage());
			throw new ServiceException(e.getMessage(), e);
		}
	}

}
