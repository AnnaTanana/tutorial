package by.tanana.newsManagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;














import org.springframework.jdbc.datasource.DataSourceUtils;

import exception.DAOException;
import by.tanana.newsManagement.dao.TagDAO;
import by.tanana.newsManagement.entity.AuthorTO;
import by.tanana.newsManagement.entity.TagTO;
import by.tanana.newsManagement.util.DBUtil;

public class TagDAOImpl implements TagDAO {

	private static final String SQL_CREATE_TAG 
	= "INSERT INTO tag "
			+ "(tag_id, tag_name) "
			+ "VALUES (tag_seq.nextval, ?)";

	private static final String SQL_READ_TAG 
	= "SELECT tag_id, tag_name "
			+ "FROM tag "
			+ "WHERE tag_id = ?"; 

	private static final String SQL_UPDATE_TAG 
	= "UPDATE tag "
			+ "SET tag_name = ? "
			+ "WHERE tag_id = ?";

	private static final String SQL_DELETE_TAG 
	= "DELETE FROM tag "
			+ "WHERE tag_id = ?";

	private static final String SQL_READ_LIST_OF_TAGS_BY_NEWS_ID 
	= "SELECT tag.tag_id, tag.tag_name "
			+ "FROM tag "
			+ "JOIN news_tag ON tag.tag_id = news_tag.tag_id "
			+ "WHERE news_tag.news_id = ?";

	private static final String SQL_BIND_NEWS_TAG 
	= "INSERT INTO news_tag "
			+ "(tag_id, news_id) "
			+ "VALUES (?, ?)";

	private static final String SQL_DELETE_FROM_NEWS_TAG_BY_NEWS_ID 
	= "DELETE FROM news_tag "
			+ "WHERE news_id = ?";

	private static final String SQL_GET_TAGS
	="SELECT tag_id, tag_name "
			+ "FROM tag";

	private DataSource dataSource;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	/* 
	 * @see by.tanana.newsManagement.dao.TagDAO#create(by.tanana.newsManagement.entity.TagTO)
	 */
	@Override
	public Long create(TagTO tag) throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet generatedKeys = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			statement = connection.prepareStatement(SQL_CREATE_TAG, new String [] {"TAG_ID"});
			Long tagId = null;
			statement.setString(1, tag.getName());
			statement.executeUpdate();
			generatedKeys = statement.getGeneratedKeys();
			if (null != generatedKeys && generatedKeys.next()) {
				tagId = Long.parseLong(generatedKeys.getString(1));
			}
			return tagId;        	
		} catch (SQLException e) {
			throw new DAOException("Error while creating a tag: " + tag, e);
		} finally {
			DBUtil.close(generatedKeys, statement, connection, dataSource);	
		}
	}

	/* 
	 * @see by.tanana.newsManagement.dao.TagDAO#read(java.lang.Long)
	 */
	@Override
	public TagTO read(Long tagId) throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			statement = connection.prepareStatement(SQL_READ_TAG);
			statement.setLong(1, tagId);			
			TagTO tag = null;        	
			resultSet = statement.executeQuery();
			if (resultSet.next()) {
				tag = new TagTO();
				tag.setTagId(resultSet.getLong("TAG_ID"));
				tag.setName(resultSet.getString("TAG_NAME"));
			}
			return tag;
		} catch (SQLException e) {
			throw new DAOException("Error while reading a tag by tag id: " + tagId, e);
		} finally {
			DBUtil.close(resultSet, statement, connection, dataSource);
		} 
	}

	/*
	 * @see by.tanana.newsManagement.dao.TagDAO#update(by.tanana.newsManagement.entity.TagTO)
	 */
	@Override
	public void update(TagTO tag) throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			statement = connection.prepareStatement(SQL_UPDATE_TAG);
			statement.setString(1, tag.getName());
			statement.setLong(2, tag.getTagId());
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("Error while updating a tag: " + tag, e);
		} finally {
			DBUtil.close(statement, connection, dataSource);
		}		

	}

	/* 
	 * @see by.tanana.newsManagement.dao.TagDAO#delete(java.lang.Long)
	 */
	@Override
	public void delete(Long tagId) throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			statement = connection.prepareStatement(SQL_DELETE_TAG);
			statement.setLong(1, tagId);
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("Error while deleting a tag by tag id: " + tagId, e);
		} finally {
			DBUtil.close(statement, connection, dataSource);
		}

	}

	/* 
	 * @see by.tanana.newsManagement.dao.TagDAO#readListOfTagsByNewsId(java.lang.Long)
	 */
	@Override
	public List<TagTO> readListOfTagsByNewsId(Long newsId) throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			statement = connection.prepareStatement(SQL_READ_LIST_OF_TAGS_BY_NEWS_ID);
			statement.setLong(1, newsId);			
			List<TagTO> tagList = null;        	
			resultSet = statement.executeQuery();
			if (resultSet.next()) {
				tagList = new ArrayList<TagTO>();
				do {
					TagTO tag = new TagTO();
					tag.setTagId(Long.parseLong(resultSet.getString("TAG_ID")));
					tag.setName(resultSet.getString("TAG_NAME"));
					tagList.add(tag);
				} while (resultSet.next());        		
			}
			return tagList;
		} catch (SQLException e) {
			throw new DAOException("Error while reading list of tags by news id: " + newsId, e);
		} finally {
			DBUtil.close(resultSet, statement, connection, dataSource);
		} 
	}

	/* 
	 * @see by.tanana.newsManagement.dao.TagDAO#bindMultipleTagsWithNews(java.util.List, java.lang.Long)
	 */
	@Override
	public void bindMultipleTagsWithNews(List<Long> tagList, Long newsId)
			throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			statement = connection.prepareStatement(SQL_BIND_NEWS_TAG);
			for (Long tagId: tagList) {        		 
				statement.setLong(1, tagId);
				statement.setLong(2, newsId);
				statement.addBatch();
			}
			statement.executeBatch();
		} catch (SQLException e) {
			throw new DAOException("Error while binding multiple tags: " + tagList + " with news: id=" + newsId, e);
		} finally {
			DBUtil.close(statement, connection, dataSource);
		}				

	}

	/* 
	 * @see by.tanana.newsManagement.dao.TagDAO#deleteFromNewsTagByNewsId(java.lang.Long)
	 */
	@Override
	public void deleteFromNewsTagByNewsId(Long newsId) throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			statement = connection.prepareStatement(SQL_DELETE_FROM_NEWS_TAG_BY_NEWS_ID);
			statement.setLong(1, newsId);
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("Error while deleting links between tags and news by news id: " + newsId, e);
		} finally {
			DBUtil.close(statement, connection, dataSource);
		}		

	}

	/* 
	 * @see by.tanana.newsManagement.dao.TagDAO#getTags()
	 */
	@Override
	public List<TagTO> getTags() throws DAOException {
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		try {
			connection = DataSourceUtils.doGetConnection(dataSource);
			statement = connection.createStatement();
			List<TagTO> tagList = null;
			resultSet = statement.executeQuery(SQL_GET_TAGS);
			if (resultSet.next()) {
				tagList = new ArrayList<TagTO>();
				do{
					TagTO tag = new TagTO();
					tag.setTagId(resultSet.getLong("TAG_ID"));
					tag.setName(resultSet.getString("TAG_NAME"));
					tagList.add(tag);
				}
				while (resultSet.next());
			}
			return tagList;
		} catch (SQLException e) {
			throw new DAOException("Error while counting news", e);
		} finally {
			DBUtil.close(resultSet, statement, connection, dataSource);
		}		
	}




}
