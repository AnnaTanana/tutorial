<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<c:forEach var="author" items="${authors}" varStatus="status">
	<form:form name="editAuthorForm" action="/news-admin/editAuthor"
		method="POST" commandName="authorForm">
		<div class="form-row">
			<div class="label">
				<spring:message code="label.author" />
				:
			</div>
			<form:input class="customInput" path="name" value="${author.name}"
				readonly="true"></form:input>
				<div class="form-cell">
			<input class="editButton" name="editButton" type="button"
				onclick="edit(this);" value="<spring:message code="button.edit"/>" /></div>
				<div class="form-cell">
			<input class="updateButton" name="command" type="submit"
				style="display: none;"
				value="<spring:message code="button.update"/>" /> </div>
				<div class="form-cell">
				<input class="expireButton" name="command" type="submit"
				style="display: none;"
				value="<spring:message code="button.expire"/>" /> </div>
				<div class="form-cell">
				<input class="cancelButton" name="cancelButton" type="button"
				onclick="cancel(this);" style="display: none;"
				value="<spring:message code="button.cancel"/>" /> </div>
				<input class="hiddenInput" type="hidden" name="authorName"
				value="${author.name}">
			<form:input type="hidden" path="authorId" value="${author.authorId}"></form:input>
		</div>
	</form:form>
</c:forEach>


<form:form name="addAuthorForm"
	action="/news-admin/addAuthor" method="POST" commandName="authorForm">
	<div class="form-row">
		<div class="label">
			<spring:message code="label.addAuthor" />:
		</div>
		<form:input path="name"></form:input>
		<input name="save" type="submit" class="saveButton"
			value="<spring:message code="button.save"/>" />
		<form:input type="hidden" path="authorId" value="${author.authorId}"></form:input>
		<div class="error-message">
			<form:errors path="name" cssClass="error" />
		</div>
	</div>


</form:form>



<script>
	function edit(editButton) {
		$(editButton).css("display", "none");
		$(editButton).parent().parent().find("input.customInput")
				.removeAttr('readonly');
		$(editButton).parent().parent().find("input.updateButton").css(
				"display", "block");
		$(editButton).parent().parent().find("input.expireButton").css(
				"display", "block");
		$(editButton).parent().parent().find("input.cancelButton").css(
				"display", "block");
	}

	function cancel(cancelButton) {
		$(cancelButton).css("display", "none");
		$(cancelButton).parent().parent().find("input.customInput").attr(
				"readonly", "true");
		$(cancelButton).parent().find("input.customInput").val(
				$(cancelButton).parent().parent().find("input.hiddenInput")
						.val());
		$(cancelButton).parent().parent().find("input.updateButton").css(
				"display", "none");
		$(cancelButton).parent().parent().find("input.expireButton").css(
				"display", "none");
		$(cancelButton).parent().parent().find("input.editButton").css(
				"display", "block");
	}
</script>