<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%-- <fmt:setLocale value="${requestContext.response.locale}" /> --%>

<form:form class="editable-form" name="leaveCommentForm" method="POST"
	action="/news-admin/saveNews" commandName="newsForm">

	<div class="form-row">
		<div class="label">
			<spring:message code="label.title" />
			:
		</div>

		<form:input type="text" path="news.title"></form:input>
		<div class="error-message">
			<form:errors path="news.title" cssClass="error" />
		</div>
	</div>


	<form:input type="hidden" path="news.newsId"></form:input>

	<div class="form-row">
		<div class="label">
			<spring:message code="label.date" />
			:
		</div>


		<spring:message var="format" code="date.format" />


		<fmt:formatDate var="date" value="${newsForm.news.creationDate}"
			pattern="${format}"></fmt:formatDate>

		<c:choose>
			<c:when test="${empty newsForm.news.newsId}">
				<form:input path="news.creationDate" type="text" value="${date}"
					id="datepicker"></form:input>
			</c:when>
			<c:otherwise>
				<form:input path="news.creationDate" type="text" readonly="true"
					value="${date}"></form:input>
			</c:otherwise>
		</c:choose>
		<div class="error-message"></div>
	</div>

	<input type="hidden" id="locale" value="${pageContext.response.locale}" />
	<input type="hidden" id="format"
		value="<fmt:message key="date.format1"/>" />

	<div class="form-row">
		<div class="label">
			<spring:message code="label.brief" />
			:
		</div>

		<form:textarea path="news.shortText" ROWS="3" COLS="30"></form:textarea>
		<div class="error-message">
			<form:errors path="news.shortText" cssClass="error" />
		</div>
	</div>


	<div class="form-row">
		<div class="label">
			<spring:message code="label.content" />
			:
		</div>

		<form:textarea path="news.fullText" ROWS="3" COLS="30"></form:textarea>
		<div class="error-message">
			<form:errors path="news.fullText" cssClass="error" />
		</div>
	</div>



	<div class="form-row">
		<div class="select">
			<form:select path="authorId" class="selectAuthor">
				<option value="" selected><spring:message
						code="newsList.selectAuthor" /></option> 
				<c:forEach var="author" items="${authorsList}">
					<form:option value="${author.authorId}">${author.name}</form:option>
				</c:forEach>
			</form:select>
			<div class="error-message">${emptyAuthor}</div>
		</div>
		<div class="select">
			<spring:message var="placeholder" code="newsList.selectTags" />
			<form:select multiple="multiple" path="tagIdList" class="SlectBox"
				placeholder="${placeholder}">
				<c:forEach var="tag" items="${tagList}">
					<form:option value="${tag.tagId}">${tag.name}</form:option>
				</c:forEach>
			</form:select>
		</div>
	</div>

	<div class="form-row">
		<div class="form-cell"></div>
		<div class="select">
			<input class="submit-foorm-button" name="submit" type="submit"
				value="<spring:message code="button.save"/>" />
		</div>
	</div>

</form:form>

<script>
	$(document).ready(function() {
		$('.SlectBox').SumoSelect();
		$('.selectAuthor').SumoSelect();
	});
</script>

<script>
	$(document).ready($(function() {
		var locale = $('#locale').val();
		var format = $('#format').val();
		$("#datepicker").datepicker({
			dateFormat : format
		});
	}));
</script>
