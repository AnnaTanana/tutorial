<%@ page language="java" contentType="text/html; charset=UTF-8"
 pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles-extras"
	prefix="tilesx"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<ul>
<li class="${current eq 'newsList' ? 'active' : ' '}"><a class="menu-item" href="/news-admin/newsList"><spring:message code="menu.newsList" /></a></li>
<li class="${current eq 'addNews' ? 'active' : ' '}"><a class="menu-item" href="/news-admin/addNews"><spring:message code="menu.addNews" /></a></li>
<li class="${current eq 'editAuthors' ? 'active' : ' '}"><a class="menu-item" href="/news-admin/editAuthors"><spring:message code="menu.addAuthor" /></a></li>
<li class="${current eq 'editTags' ? 'active' : ' '}"><a class="menu-item" href="/news-admin/editTags"><spring:message code="menu.addTags" /></a></li>
</ul>

