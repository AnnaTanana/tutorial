<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<c:forEach var="tag" items="${tags}" varStatus="status">
	<form:form name="editTagForm" action="/news-admin/editTag"
		method="POST" commandName="tagForm">
		<div class="form-row">
			<div class="label">
				<spring:message code="label.tag" />
				:
			</div>
			<form:input class="customInput" path="name" value="${tag.name}"
				readonly="true"></form:input>
			<div class="form-cell">
				<input class="editButton" name="editButton" type="button"
					onclick="edit(this);" value="<spring:message code="button.edit"/>" />
			</div>
			<div class="form-cell">
				<input class="updateButton" name="command" type="submit"
					onclick="checkButton(this);" style="display: none;"
					value="<spring:message code="button.update"/>" />
			</div>
			<div class="form-cell">
				<input class="deleteButton" name="command" type="submit"
					onclick="checkButton(this);" style="display: none;"
					value="<spring:message code="button.delete"/>" />
			</div>
			<div class="form-cell">
				<input class="cancelButton" name="cancelButton" type="button"
					onclick="cancel(this);" style="display: none;"
					value="<spring:message code="button.cancel"/>" />
			</div>
			<input class="hiddenInput" type="hidden" name=tagName
				"
					value="${tag.name}">
			<form:input type="hidden" path="tagId" value="${tag.tagId}"></form:input>
		</div>
	</form:form>
</c:forEach>
<form:form name="addTagForm" action="/news-admin/addTag" method="POST"
	commandName="tagForm">
	<div class="form-row">
		<div class="label">
			<spring:message code="label.addTag" />
			:
		</div>
		<form:input path="name"></form:input>

		<input name="save" type="submit" class="saveButton"
			value="<spring:message code="button.save"/>" />

		<form:input type="hidden" path="tagId" value="${tag.tagId}"></form:input>

		<div class="error-message">
			<form:errors path="name" cssClass="error" />
		</div>

	</div>
</form:form>


<script>
	function edit(editButton) {
		$(editButton).css("display", "none");
		$(editButton).parent().parent().find("input.customInput").removeAttr(
				'readonly');
		$(editButton).parent().parent().find("input.updateButton").css(
				"display", "block");
		$(editButton).parent().parent().find("input.deleteButton").css(
				"display", "block");
		$(editButton).parent().parent().find("input.cancelButton").css(
				"display", "block");
	}

	function cancel(cancelButton) {
		$(cancelButton).css("display", "none");
		$(cancelButton).parent().parent().find("input.customInput").attr(
				"readonly", "true");
		$(cancelButton).parent().parent().find("input.customInput").val(
				$(cancelButton).parent().parent().find("input.hiddenInput")
						.val());
		$(cancelButton).parent().parent().find("input.updateButton").css(
				"display", "none");
		$(cancelButton).parent().parent().find("input.deleteButton").css(
				"display", "none");
		$(cancelButton).parent().parent().find("input.editButton").css(
				"display", "block");
	}
</script>