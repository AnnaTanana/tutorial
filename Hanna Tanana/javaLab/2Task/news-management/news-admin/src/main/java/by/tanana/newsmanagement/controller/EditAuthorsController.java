package by.tanana.newsmanagement.controller;

import java.sql.Timestamp;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import exception.ServiceException;
import by.tanana.newsManagement.entity.AuthorTO;
import by.tanana.newsManagement.service.AuthorService;
import by.tanana.newsmanagement.exception.ControllerException;

@Controller
public class EditAuthorsController extends BaseController {

    private static final Logger logger = LoggerFactory.getLogger(EditAuthorsController.class);

    @Autowired
    private AuthorService authorService;

    @RequestMapping({ "/editAuthors" })
    public String editAuthors(Model model) throws ControllerException {
	try {
	    List<AuthorTO> authors = authorService.getAuthors();
	    model.addAttribute("authors", authors);
	    if (!model.containsAttribute("authorForm")) {
		model.addAttribute("authorForm", new AuthorTO());
	    }
	} catch (ServiceException e) {
	    logger.error(e.getMessage());
	    throw new ControllerException(e.getMessage(), e);
	}
	return "editAuthors";
    }

    @RequestMapping({ "/editAuthor" })
    public String updateAuthor(@ModelAttribute("authorForm") @Valid AuthorTO authorForm, BindingResult result,
	    HttpServletRequest request, RedirectAttributes redirectAttributes,
	    @RequestParam(value = "command") String command) throws ControllerException {
	if (!result.hasErrors()) {
	    try {
		if ("expire".equals(command.toLowerCase())) {

		    authorService.delete(authorForm.getAuthorId(), new Timestamp(System.currentTimeMillis()));

		} else if ("update".equals(command.toLowerCase())) {
		    authorService.update(authorForm);
		}
	    } catch (ServiceException e) {
		logger.error(e.getMessage());
		throw new ControllerException(e.getMessage(), e);
	    }

	} else {
	    BaseController.handleValidationErrors(redirectAttributes, result);
	}
	return "redirect:/editAuthors";
    }

    @RequestMapping({ "/addAuthor" })
    public String addAuthor(@ModelAttribute("authorForm") @Valid AuthorTO authorForm, BindingResult result,
	    HttpServletRequest request, RedirectAttributes redirectAttributes) throws ControllerException {
	if (!result.hasErrors()) {
	    try {
		authorService.create(authorForm);
	    } catch (ServiceException e) {
		logger.error(e.getMessage());
		throw new ControllerException(e.getMessage(), e);
	    }

	} else {
	    BaseController.handleValidationErrors(redirectAttributes, result);
	}
	return "redirect:/editAuthors";
    }

}
