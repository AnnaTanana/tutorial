package by.tanana.newsmanagement.controller;

import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import exception.ServiceException;
import by.tanana.newsManagement.entity.AuthorTO;
import by.tanana.newsManagement.entity.NewsTO;
import by.tanana.newsManagement.entity.NewsVOForm;
import by.tanana.newsManagement.entity.TagTO;
import by.tanana.newsManagement.service.AuthorService;
import by.tanana.newsManagement.service.NewsManagementService;
import by.tanana.newsManagement.service.TagService;
import by.tanana.newsmanagement.exception.ControllerException;
import by.tanana.newsmanagement.utils.NewsFormValidator;

@Controller
public class EditNewsController extends BaseController {
    private static final Logger logger = LoggerFactory.getLogger(EditNewsController.class);

    @Autowired
    private NewsManagementService newsManagementService;

    @Autowired
    private AuthorService authorService;

    @Autowired
    private TagService tagService;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private NewsFormValidator newsFormValidator;

    @InitBinder
    public void initDateBinder(WebDataBinder dataBinder, Locale locale) {
	SimpleDateFormat sdf = new SimpleDateFormat(
		messageSource.getMessage("date.format", null,
			locale));
	dataBinder.registerCustomEditor(Date.class, new CustomDateEditor(sdf,  true));

	dataBinder.setValidator(newsFormValidator);
    }

    @RequestMapping(value = "/editNews/{newsId}")
    public String editNews(@PathVariable("newsId") Long newsId, Model model) throws ControllerException {
	try {
	    if (!model.containsAttribute("newsForm")) {
		NewsVOForm news = newsManagementService.getNewsVOForm(newsId);
		model.addAttribute("newsForm", news);
	    }
	    List<AuthorTO> authorsList = authorService.getAuthors();
	    model.addAttribute("authorsList", authorsList);

	    List<TagTO> tagList = tagService.getTags();
	    model.addAttribute("tagList", tagList);

	} catch (ServiceException e) {
	    logger.error(e.getMessage());
	    throw new ControllerException(e.getMessage(), e);
	}
	return "addNews";
    }

    @RequestMapping(value = "/addNews")
    public String addNews(Model model) throws ControllerException {
	try {
	    if (!model.containsAttribute("newsForm")) {
		NewsVOForm newsVoForm = new NewsVOForm();
		newsVoForm.setNews(new NewsTO());
		newsVoForm.getNews().setCreationDate(new Date(System.currentTimeMillis()));
		model.addAttribute("newsForm", newsVoForm);
	    }

	    List<AuthorTO> authorsList = authorService.getAuthors();
	    model.addAttribute("authorsList", authorsList);

	    List<TagTO> tagList = tagService.getTags();
	    model.addAttribute("tagList", tagList);
	} catch (ServiceException e) {
	    logger.error(e.getMessage());
	    throw new ControllerException(e.getMessage(), e);
	}
	return "addNews";
    }

    @RequestMapping(value = "/saveNews")
    public String saveNews(@ModelAttribute("newsForm") @Validated NewsVOForm newsForm, BindingResult result,
	    HttpServletRequest request, RedirectAttributes redirectAttributes, Locale locale) throws ControllerException {
	Long newsId = newsForm.getNews().getNewsId();;
	if (!result.hasErrors()) {
	    try {
		if (newsId != null) {
		    newsForm.getNews().setModificationDate(new Date(System.currentTimeMillis()));
		    newsManagementService.editNews(newsForm.getNews(), newsForm.getAuthorId(), newsForm.getTagIdList());
		} else {
		    newsForm.getNews().setModificationDate(newsForm.getNews().getCreationDate());
		    newsId = newsManagementService.addNews(newsForm.getNews(), newsForm.getAuthorId(),
			    newsForm.getTagIdList());
		}
	    } catch (ServiceException e) {
		logger.error(e.getMessage());
		throw new ControllerException(e.getMessage(), e);
	    }
	    return "redirect:/newsList/" + newsId;
	} else {
	    BaseController.handleValidationErrors(redirectAttributes, result);
	    if (newsForm.getAuthorId() == null) {
		redirectAttributes.addFlashAttribute("emptyAuthor", messageSource.getMessage("label.validate.authorId", null, locale));
	    }
	    if (newsId != null) {
		return "redirect:" + "editNews/" + newsId;
	    } else {
		return "redirect:" + "addNews";
	    }
	}
    }
}
