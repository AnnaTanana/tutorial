package by.tanana.newsmanagement.utils;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import by.tanana.newsManagement.entity.NewsVOForm;

public class NewsFormValidator implements Validator{

	public boolean supports(Class<?> clazz) {
		return NewsVOForm.class.isAssignableFrom(clazz);
	}

	public void validate(Object target, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "news.title", "label.validate.titleEmpty");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "news.shortText", "label.validate.shortTextEmpty");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "news.fullText", "label.validate.fullTextEmpty");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "authorId", "label.validate.authorId");

		NewsVOForm newsForm = (NewsVOForm) target;
		if ((newsForm.getNews().getTitle() !=  null) && (newsForm.getNews().getTitle().length() > 30)) {
			errors.rejectValue("news.title", "label.validate.titleSize");
		}
		if ((newsForm.getNews().getShortText() !=  null) && (newsForm.getNews().getShortText().length() > 100)) {
			errors.rejectValue("news.shortText", "label.validate.shortTextSize");
		}
		if ((newsForm.getNews().getFullText() !=  null) && (newsForm.getNews().getFullText().length() > 2000)) {
			errors.rejectValue("news.fullText", "label.validate.fullTextSize");
		}
		if (newsForm.getAuthorId() == null) {
		    errors.rejectValue("authorId", "label.validate.authorId");
		}

	}

}
