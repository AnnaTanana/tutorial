package by.tanana.newsmanagement.controller;

import java.sql.Date;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import exception.ServiceException;
import by.tanana.newsManagement.entity.CommentTO;
import by.tanana.newsManagement.entity.NewsVO;
import by.tanana.newsManagement.service.CommentService;
import by.tanana.newsManagement.service.NewsManagementService;
import by.tanana.newsmanagement.exception.ControllerException;

@Controller
public class ViewNewsController extends BaseController {

    private static final Logger logger = LoggerFactory.getLogger(ViewNewsController.class);

    @Autowired
    private NewsManagementService newsManagementService;

    @Autowired
    private CommentService commentService;

    @RequestMapping(value = "/newsList/{newsId}")
    public String watchNews(@PathVariable("newsId") Long newsId, Model model) throws ControllerException {
	if (!model.containsAttribute("commentForm")) {
	    model.addAttribute("commentForm", new CommentTO());
	}
	try {
	    NewsVO news = newsManagementService.getNews(newsId);
	    model.addAttribute("newsVO", news);
	} catch (ServiceException e) {
	    logger.error(e.getMessage());
	    throw new ControllerException(e.getMessage(), e);
	}
	return "news";
    }

    @RequestMapping(value = "/deleteComment")
    public String deleteComment(HttpServletRequest request) throws ControllerException {
	Long commentId = null;
	Long newsId = null;
	if (request.getParameter("commentId") != null) {
	    commentId = Long.parseLong(request.getParameter("commentId"));
	} 	 
	if (request.getParameter("commentId") != null) {
	    newsId = Long.parseLong(request.getParameter("newsId"));
	}
	try {
	    commentService.delete(commentId);
	} catch (ServiceException e) {
	    logger.error(e.getMessage());
	    throw new ControllerException(e.getMessage(), e);
	}
	return "redirect:" + "newsList/" + newsId;
    }

    @RequestMapping(value = "/leaveComment")
    public String leaveComment(@ModelAttribute("commentForm") @Valid CommentTO commentForm, BindingResult result,
	    HttpServletRequest request, RedirectAttributes redirectAttributes) throws ControllerException {
	Long newsId = null;
	if (request.getParameter("newsId") != null) {
	    newsId = Long.parseLong(request.getParameter("newsId"));
	}
	if (!result.hasErrors()) {
	    try {
		commentForm.setNewsId(newsId);
		commentForm.setCreationDate(new Date(System.currentTimeMillis()));
		commentService.create(commentForm);
	    } catch (ServiceException e) {
		logger.error(e.getMessage());
		throw new ControllerException(e.getMessage(), e);
	    }	    

	} else {
	    BaseController.handleValidationErrors(redirectAttributes, result);
	}
	return "redirect:" + "newsList/" + newsId;
    }

}
