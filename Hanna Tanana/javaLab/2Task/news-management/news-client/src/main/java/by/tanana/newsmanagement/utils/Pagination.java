package by.tanana.newsmanagement.utils;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;

public class Pagination {
    
    private int firstIndex;
    private int lastIndex;
    private int currentPage;
    
    public int getCurrentPage() {
	return currentPage;
    }

    public int getFirstIndex() {
	return firstIndex;
    }

    public int getLastIndex() {
	return lastIndex;
    }

    public List<Integer> definePages(int numberOfSearchedNews)  {	 
	long numberOfPages = (long) Math.ceil((double)numberOfSearchedNews/3);		

	List<Integer> pages = new ArrayList<Integer>();
	for (int i=1; i<=numberOfPages; i++) {
	    pages.add(i);
	}
	return pages;
    }

    public void defineCurrentPage(HttpServletRequest request, int numberOfSearchedNews) {
	if (request.getParameter("currentPage") != null) {
	    currentPage = Integer.parseInt(request.getParameter("currentPage"));
	}
	else if (numberOfSearchedNews != 0) {
	    currentPage = 1;
	}
	else {
	    currentPage = 0;
	}
    }

    public void defineIndex(int numberOfNews) {	
	
	firstIndex = currentPage*numberOfNews-numberOfNews;
	lastIndex = currentPage*numberOfNews;
    }
}
