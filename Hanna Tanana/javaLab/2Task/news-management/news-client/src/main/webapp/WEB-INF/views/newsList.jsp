<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<div>
	<form:form method="POST" action="filter" commandName="searchCriteria">
		<c:if test="${not empty authorsList}">
			<form:select path="authorId" class="selectAuthor">
				<option value="" selected><spring:message
						code="newsList.selectAuthor" /></option>
				<c:forEach var="author" items="${authorsList}">
					<form:option value="${author.authorId}">${author.name}</form:option>
				</c:forEach>
			</form:select>
		</c:if>
		<c:if test="${not empty tagList}">
			<spring:message var="placeholder" code="newsList.selectTags" />
			<form:select multiple="multiple" path="tagIdList" class="SlectBox"
				placeholder="${placeholder}">
				<c:forEach var="tag" items="${tagList}">
					<form:option value="${tag.tagId}">${tag.name}</form:option>
				</c:forEach>
			</form:select>

		</c:if>
		<input class="button" type="submit" value="<spring:message code="button.filter"/>" />
	</form:form>
	<a class="button button-2" href="reset"><spring:message code="button.reset" /></a>
</div>

<c:if test="${not empty emptyNewsListMess}">
	<div>${emptyNewsListMess}</div>
</c:if>
<c:if test="${not empty newsList}">
	<form method="post" action="deleteNews">
		<div class="news-list">
			<c:forEach var="newsVO" items="${newsList}" varStatus="status">
				<div class="form-row">
					<div class="form-cell title"><a href="newsList/${newsVO.news.newsId}">${newsVO.news.title}</a></div>
					<div class="form-cell bold">(by <c:out value="${newsVO.author.name}"></c:out>)
					</div>
					<fmt:message var="format" key="date.format" />
					<fmt:formatDate var="date" value="${newsVO.news.modificationDate}"
						pattern="${format}"></fmt:formatDate>
					<div class="form-cell bold"><c:out value="${date}"></c:out></div>
				</div>
				<div class="form-row">
					<div class="form-cell"><c:out value="${newsVO.news.shortText}"></c:out></div>
				</div>
				<div class="form-row last-row">
					<div class="form-cell tags-cell"><c:forEach var="tag" items="${newsVO.tags}"
							varStatus="status">
							<c:out value="${tag.name}"></c:out>
						</c:forEach></div>
					<div class="form-cell italic" ><spring:message code="newsList.comments" />(${newsVO.numberOfComments})</div>
					<div class="form-cell"><a href="editNews/${newsVO.news.newsId}"><spring:message
								code="button.edit" /></a></div>
					<div class="form-cell"><input type="checkbox" name="newsId"
						value="${newsVO.news.newsId}" onclick="checkItems();" /></div>
				</div>
			</c:forEach>
		</div>
		<input class="button" type="submit" id="deleteButton" disabled="disabled"
			value="<spring:message code="button.delete"/>" />
	</form>
</c:if>
<div class="page-links">
<c:if test="${not empty pages}">
	<c:forEach var="page" items="${pages}">
	<c:choose>
			<c:when test="${selectedPage eq page}">
				<a class="page-button-active" href="newsList?currentPage=${page}">${page}</a>
			</c:when>
			<c:otherwise>
				<a class="page-button " href="newsList?currentPage=${page}">${page}</a>
			</c:otherwise>
		</c:choose>
	</c:forEach>
</c:if>
</div>

<script>
	$(document).ready(function() {
		$('.SlectBox').SumoSelect();
		$('.selectAuthor').SumoSelect();
	});
</script>

<script>
	function checkItems() {
		var atLeastOneIsChecked = $('input[name="newsId"]:checked').length;
		if (atLeastOneIsChecked == 0) {
			$('#deleteButton').attr("disabled", "disabled");
		} else {
			$('#deleteButton').removeAttr("disabled");
		}
	}
</script>
