<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<div>
	<div id="header-title">
		<spring:message code="header.title" />
	</div>

	<div id="header-content">
		<div id="welcome">
			<!-- <sec:authorize access="hasRole('ROLE_ADMIN')"> -->
				<div id="welcome-message">
					<%-- <spring:message code="header.hello" />,
					<sec:authentication property="principal.username" /> --%>
				</div>
				<div id="logout">
					<%-- <form action="${pageContext.request.contextPath}/logout"
						method="POST">
						<input class="button" type="submit"
							value="<spring:message code="header.logout" />"> --%>
					<!-- </form> -->
				</div>
			<!-- </sec:authorize> -->
		</div>
		<div>
			<div id="language-bar"><a href="?lang=en">EN</a>
					| <a href="?lang=ru">RU</a>
			</div>
		</div>
	</div>
</div>
